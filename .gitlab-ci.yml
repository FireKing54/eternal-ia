# Define stages
stages:
  - build
  - test
  - deploy

# Global variables
variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: /certs
  TEST_DATABASE_URL: postgresql://testuser:testpassword@db:5432/testdb
  K8S_CONTEXT: default
  CONTAINER_IMAGE_NAME_FILE: .container_image_name
  CI_NETWORK_NAME: eternal-ia-network

# Cache for Yarn dependencies
cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .yarn/cache
    - node_modules/

# Build the Docker image
build:
  stage: build
  image: docker:24.0.5
  services:
    - name: docker:24.0.5-dind
      alias: docker
  before_script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - | # Required multi-line syntax to escape that colon
        export VERSION=$(grep '"version"' package.json |
                        sed 's/.*"version": "\(.*\)",/\1/')
    - export CONTAINER_RELEASE_IMAGE=$CI_REGISTRY_IMAGE:$VERSION
  script:
    - docker build -t $CONTAINER_RELEASE_IMAGE .
    - docker push $CONTAINER_RELEASE_IMAGE
    - echo "$CONTAINER_RELEASE_IMAGE" > $CONTAINER_IMAGE_NAME_FILE
  artifacts:
    paths:
      - $CONTAINER_IMAGE_NAME_FILE
  only:
    - master
    - merge_requests

# Run unit tests inside the Docker image
unit_test:
  stage: test
  image: docker:24.0.5
  services:
    - name: docker:24.0.5-dind
      alias: docker
  script:
    - export IMAGE_NAME=$(cat $CONTAINER_IMAGE_NAME_FILE)
    - docker run --rm -e ETERNAL_IA_DATABASE_PASSWORD="$ETERNAL_IA_DATABASE_PASSWORD"
      -e ETERNAL_IA_DATABASE_PORT="$ETERNAL_IA_DATABASE_PORT"
      -e ETERNAL_IA_ETERNALFEST_AUTH_TOKEN="$ETERNAL_IA_ETERNALFEST_AUTH_TOKEN"
      -e ETERNAL_IA_MAIN_GUILD="$ETERNAL_IA_MAIN_GUILD"
      -e ETERNAL_IA_QUIZ_CHANNEL="$ETERNAL_IA_QUIZ_CHANNEL"
      -e ETERNAL_IA_ROLE_CLAIM_CHANNEL="$ETERNAL_IA_ROLE_CLAIM_CHANNEL"
      -e ETERNAL_IA_TOKEN="$ETERNAL_IA_TOKEN"
      -e ETERNAL_IA_DATABASE_SERVER_USERNAME="$ETERNAL_IA_DATABASE_SERVER_USERNAME"
      -e ETERNAL_IA_DATABASE_NAME="$ETERNAL_IA_DATABASE_NAME"
      $IMAGE_NAME yarn test:unit
  only:
    - master
    - merge_requests

# Run integration tests inside the Docker image with PostgreSQL service
integration_test:
  stage: test
  image: docker:24.0.5
  services:
    - name: docker:24.0.5-dind
      alias: docker
    - name: postgres:16
      alias: db
  variables:
    POSTGRES_DB: testdb
    POSTGRES_USER: testuser
    POSTGRES_PASSWORD: testpassword
  before_script:
    - docker network create "$CI_NETWORK_NAME"
  script:
    - export IMAGE_NAME=$(cat $CONTAINER_IMAGE_NAME_FILE)
    - docker run --rm --network "$CI_NETWORK_NAME" -e ETERNAL_IA_DATABASE_PASSWORD="testpassword"
      -e ETERNAL_IA_DATABASE_PORT="5432"
      -e ETERNAL_IA_ETERNALFEST_AUTH_TOKEN="$ETERNAL_IA_ETERNALFEST_AUTH_TOKEN"
      -e ETERNAL_IA_MAIN_GUILD="$ETERNAL_IA_MAIN_GUILD"
      -e ETERNAL_IA_QUIZ_CHANNEL="$ETERNAL_IA_QUIZ_CHANNEL"
      -e ETERNAL_IA_ROLE_CLAIM_CHANNEL="$ETERNAL_IA_ROLE_CLAIM_CHANNEL"
      -e ETERNAL_IA_TOKEN="$ETERNAL_IA_TOKEN"
      -e ETERNAL_IA_DATABASE_SERVER_USERNAME="testuser"
      -e ETERNAL_IA_DATABASE_NAME="testdb"
      "$IMAGE_NAME" yarn migrate:latest
    - docker run --rm --network "$CI_NETWORK_NAME" -e ETERNAL_IA_DATABASE_PASSWORD="testpassword"
      -e ETERNAL_IA_DATABASE_PORT="5432"
      -e ETERNAL_IA_ETERNALFEST_AUTH_TOKEN="$ETERNAL_IA_ETERNALFEST_AUTH_TOKEN"
      -e ETERNAL_IA_MAIN_GUILD="$ETERNAL_IA_MAIN_GUILD"
      -e ETERNAL_IA_QUIZ_CHANNEL="$ETERNAL_IA_QUIZ_CHANNEL"
      -e ETERNAL_IA_ROLE_CLAIM_CHANNEL="$ETERNAL_IA_ROLE_CLAIM_CHANNEL"
      -e ETERNAL_IA_TOKEN="$ETERNAL_IA_TOKEN"
      -e ETERNAL_IA_DATABASE_SERVER_USERNAME="testuser"
      -e ETERNAL_IA_DATABASE_NAME="testdb"
      "$IMAGE_NAME" yarn test:integration
  after_script:
    - docker network rm $CI_NETWORK_NAME
  only:
    - master
    - merge_requests

# Deploy the image to Kubernetes
deploy:
  stage: deploy
  image: bitnami/kubectl:latest
  script:
    - export IMAGE_NAME=$(cat $CONTAINER_IMAGE_NAME_FILE)
    - kubectl config set-cluster $K8S_CLUSTER_NAME --server=$K8S_SERVER --insecure-skip-tls-verify=true
    - kubectl config set-credentials gitlab --token=$K8S_TOKEN
    - kubectl config set-context $K8S_CONTEXT --cluster=$K8S_CLUSTER_NAME --user=gitlab --namespace=$K8S_NAMESPACE
    - kubectl config use-context $K8S_CONTEXT
    - kubectl set image deployment/$K8S_DEPLOYMENT_NAME $K8S_CONTAINER_NAME=$IMAGE_NAME
    - kubectl rollout status deployment/$K8S_DEPLOYMENT_NAME
  only:
    - master
