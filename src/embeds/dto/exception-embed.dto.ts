export interface ExceptionEmbedDto {
    message: string;
    fix?: string;
}