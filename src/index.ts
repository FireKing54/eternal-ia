import "reflect-metadata";

import { Interaction, Message, GatewayIntentBits, CommandInteraction, ApplicationCommandOptionType, CommandInteractionOption } from "discord.js";
import { Client } from "discordx";
import { ENV } from "./Environment";

import { logger } from './Log';
import { NotBot } from './modules/guards/NotBot';
import { REGISTERED_COMMANDS } from './Managers';
import { NDJCommand } from './commands/NDJ';
import { ErrorHandler } from './guards/error-handler.guard';
import { initCrons } from './Cron';

const bot = new Client({
    // To only use global commands (use @Guild for specific guild command), comment this line
    // botGuilds: [(client) => client.guilds.cache.map((guild) => guild.id)],

    // Discord intents
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildMessageReactions,
        GatewayIntentBits.GuildVoiceStates,
    ],

    // Debug logs are disabled in silent mode
    silent: false,

    // Configuration for @SimpleCommand
    simpleCommand: {
        prefix: "!",
    },

    // Guards
    guards: [
        NotBot,
        ErrorHandler
    ],

});

bot.once("ready", async () => {
    // Make sure all guilds are cached
    await bot.guilds.fetch();
    logger.debug("Guilds cached");

    // Synchronize applications commands with Discord
    await bot.initApplicationCommands();
    logger.debug("Application commands synchronized");

    // Make sure all members in main guild are cached
    await bot.guilds.cache.get(ENV.MAIN_GUILD)?.members.fetch();
    logger.debug("Members cached");

    // Activate NDJ pings
    NDJCommand.activatePings(bot);
    logger.debug("NDJ pings activated");

    // Activate crons
    initCrons(bot);

    // To clear all guild commands, uncomment this line,
    // This is useful when moving from guild commands to global commands
    // It must only be executed once
    //
    //  await bot.clearApplicationCommands(
    //    ...bot.guilds.cache.map((g) => g.id)
    //  );

    logger.info("Bot started");
});

/**
 * Returns the full name of the command
 * It performs a recursive search in the options to find the full name
 * @param interaction 
 */
function getCommandFullName(interaction: CommandInteraction) {
    function aux(option: CommandInteractionOption): string {
        let fullName = ""
        if (option.type === ApplicationCommandOptionType.Subcommand || option.type === ApplicationCommandOptionType.SubcommandGroup) {
            fullName += ` ${option.name}`
            if (option.options) {
                fullName += option.options.map((opt) => aux(opt)).join("")
            }
        }
        return fullName
    }

    return `${interaction.commandName}${interaction.options.data.map((option) => aux(option)).join("")}`
}

bot.on("interactionCreate", async (interaction: Interaction) => {
    let incomingInteractionMessage = `Received interaction ${interaction.isCommand() ? interaction.commandName : interaction.id} from ${interaction.user.username}`

    if (interaction.isCommand()) {
        // Write fulle command name
        incomingInteractionMessage += `\nFull name: \`${getCommandFullName(interaction)}\``

        // Fetch the last subcommand, where the options are
        let lastOption: CommandInteractionOption | CommandInteraction = interaction.options.data[0]
        if ((! lastOption) || lastOption.type !== ApplicationCommandOptionType.Subcommand) {
            lastOption = interaction
        }
        else {
            while (lastOption && lastOption.type === ApplicationCommandOptionType.SubcommandGroup) {
                lastOption = lastOption.options?.[0]!
            }
        }

        // Write options
        let optionsToFormat = lastOption instanceof CommandInteraction ? lastOption.options?.data : lastOption.options
        const options = optionsToFormat?.map((option) => {
            return `${option.name}: ${option.value}`
        })

        if (options && options.length > 0) {
            incomingInteractionMessage += `\nDetails:\n\t${options?.join("\n\t")}`
        }
    }
    logger.info(incomingInteractionMessage)

    await bot.executeInteraction(interaction);
})

// For simple old commands
bot.on("messageCreate", (message: Message) => {
    bot.executeCommand(message);
});

async function run() {
  // The following syntax should be used in the commonjs environment
  // await importx(__dirname + "/classes/**/*.{ts,js}");

  logger.debug(REGISTERED_COMMANDS);

  // Log in with your bot token
  await bot.login(ENV.BOT_TOKEN);
}

run();