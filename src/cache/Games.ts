import { ApplicationCommandOptionChoiceData } from "discord.js";
import { normalMatch } from "../match/matchers";
import { eternalfestGameCache } from "./eternalfest.cache";

async function autocomplete(input: string) {

    let mapped: Array<{"id": string, "name": string}> = []

    const efGames = await eternalfestGameCache.getGames();
    mapped = efGames.map(game => ({"id": game.id, "name": game.channels.items[0].build.display_name}));

    const filtered: Array<ApplicationCommandOptionChoiceData> = mapped
        .filter(game => normalMatch(game.name, input))
        .map(game => ({
            "name": game.name,
            "value": game.id
        }))

    return filtered.slice(0, 24);
}

export default {
    autocomplete,
}