import BlessedAPI from "blessed-api";

export const eternaltwinUsersCache = new BlessedAPI.EternalTwin.v1.Users({
    "usersCacheOptions": { ttl: 60 * 60 * 24 }, // 1 day
});

export const eternaltwinHammerfestUserCache = new BlessedAPI.EternalTwin.v1.Hammerfest({
    "hammerfestUserCacheOptions": { ttl: 60 * 60 }, // 1 hour
});