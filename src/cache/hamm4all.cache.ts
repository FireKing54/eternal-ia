import { H4A_API, Item } from "@hamm4all/shared";
import axios from "axios"
import NodeCache from "node-cache";

const hamm4allAPI = axios.create({
    "baseURL": "https://hamm4all.net",
    "headers": {
        'Content-Type': 'application/json',
    }
})

export const getItems = async (gameID: number) => hamm4allAPI.get<Array<Item>>(
    H4A_API.v1.items.list.getParsedUri({}) + `?games=${gameID}`
).then(res => res.data)

const assetCache = new NodeCache({"stdTTL": 30 * 60})

export const getAsset = async (id: string) => {
    const inCache = assetCache.get<Buffer>(id)
    if (inCache) {
        return inCache
    }

    const asset = await hamm4allAPI.get(
        H4A_API.v1.files.getRaw.getParsedUri({"id": id}),
        {
            "responseType": "arraybuffer"
        }
    ).then(res => res.data)

    assetCache.set(id, asset)

    return asset
}

export const getItemInGroup = async (group: number, index: number) => {
    const id = `${group}/${index}`
    const inCache = assetCache.get<Buffer>(id)
    if (inCache) {
        return inCache
    }

    const asset = await hamm4allAPI.get(
        `https://hamm4all.net/assets/items/${group}/${index}.svg`,
        {
            "responseType": "arraybuffer"
        }
    ).then(res => res.data)

    assetCache.set(id, asset)

    return asset
}
