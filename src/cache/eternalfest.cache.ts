import BlessedAPI from "blessed-api";
import axios from "axios";
import { EternalfestGlobalUser } from "blessed-api/dist/eternalfest/v1/users/types/users.options";

export const eternalfestGameCache = new BlessedAPI.Eternalfest.v1.Game({
    "gamesCacheOptions": { ttl: 60 * 60 * 24 }, // 1 day
    "gameCacheOptions": { ttl: 60 * 60 * 24 }, // 1 hour
    "leaderboardCacheOptions": { ttl: 60 * 30 }, // 30 minutes
});

export const eternalfestUsersCache = new BlessedAPI.Eternalfest.v1.Users({
    "usersCacheOptions": { ttl: 60 * 60 * 24 }, // 1 day
});

export const eternalfestAssetsCache = BlessedAPI.Eternalfest.v1.Assets.defaultInstance

const tempUsers = axios.get<Array<EternalfestGlobalUser>>("https://nikost.dev/01918a11-dab6-7866-bc16-bf55630720fb/api/v1/users")
    .then(res => res.data)

export const tempEternalfestUsersCache = {"getUsers": () => tempUsers}