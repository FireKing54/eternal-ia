import { ApplicationCommandOptionChoiceData } from "discord.js";
import { normalMatch } from "../match/matchers";
import { tempEternalfestUsersCache } from "./eternalfest.cache";

async function autocomplete(input: string, get: "name" | "id" = "id"): Promise<ApplicationCommandOptionChoiceData<string>[]> {
    const users = await tempEternalfestUsersCache.getUsers();
    const mapped: Array<ApplicationCommandOptionChoiceData<string>> = users
        .map(user => ({"id": user.id, "name": user.display_name}))
        .filter(user => normalMatch(user.name, input))
        .map(user => ({
            "name": user.name,
            "value": get === "id" ? user.id : user.name
        }))
    return mapped.slice(0, 24);
}

async function autocompleteNameAndID(input: string): Promise<ApplicationCommandOptionChoiceData<string>[]> {
    const users = await tempEternalfestUsersCache.getUsers();

    const mapped: Array<ApplicationCommandOptionChoiceData<string>> = users
        .map(user => ({"id": user.id, "name": user.display_name}))
        .filter(user => normalMatch(user.name, input))
        .map(user => ({
            "name": `${user.name} - ${user.id}`,
            "value": user.id
        }))
    return mapped.slice(0, 24);
}

export default {
    autocomplete,
    autocompleteNameAndID
}