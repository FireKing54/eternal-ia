import { ApplicationCommandOptionType, CommandInteraction, EmbedBuilder } from "discord.js";

import sharp from "sharp";
import { CommunityBingo } from "../../database/models/CommunityBingo";
import Connection from "../../database/Connection";
import gaussian from "gaussian";
import { Discord, Guard, Slash, SlashGroup, SlashOption, VName } from "discordx";
import { NeedsPermission } from "../../permissions/Permissions";
import BlessedAPI from "blessed-api";
import BotError from "../../errors/BotError";
import { BingoElement, BingoElementFactory } from "./elements/BingoElement";
import { BingoItemFactory } from "./elements/BingoItem";
import { BingoGenericCoeffFactory } from "./elements/BingoGenericCoeff";
import ArrayUtil from "blessed-api/dist/utils/ArrayUtils";
import { BINGO_PATH } from "./Bingo";
import fs from "fs";
import { Item } from "@hamm4all/shared";
import { IsNull } from "typeorm";
import { getItems, getItemInGroup } from "../../cache/hamm4all.cache";

interface TotalWeightsType {
    "points": number;
    "effect": number;
}

interface SharpImage {
    "top": number,
    "left": number,
    "input": Buffer
}

interface RaritiesType {
    [key: string]: {
        "points": Array<Item>,
        "effect": Array<Item>
    }
}

type BingoGenerateInputOptions = {
    "c1-max-effects": number,
    "c2-max-effects": number,
    "c3-max-effects": number,
    "c4-max-effects": number,
    "c5-max-effects": number,
    "c6-max-effects": number,
    "c7-max-effects": number,
    "c1-max-points": number,
    "c2-max-points": number,
    "c3-max-points": number,
    "c4-max-points": number,
    "c5-max-points": number,
    "c6-max-points": number,
    "c7-max-points": number,
    "c4-max-generics": number,
    "c5-max-generics": number,
    "c6-max-generics": number,
    "size": number,
    "allow-duplicates": boolean,
    "community": boolean,
    "difficulty": number,
    
}

function getSlashOptionForCoeff(coeff: number, name: "points" | "effect" | "generics"): any {
    return {
        "name": `c${coeff}-max-${name}` as VName<string>,
        "description": `Définit le nombre maximal de coefficients ${coeff} ${name === "generics" ? "génériques" : `à ${name}`} pouvant apparaître en une seule grille`,
        "required": false,
        "type": ApplicationCommandOptionType.Integer,
        "minValue": 0,
    }
}

export const BINGO_BOX_SIZE = 50;

@Discord()
@SlashGroup({
    "name": "bingo",
    "description": "Tout ce qui concerne le bingo"
})
@SlashGroup("bingo")
@Guard(NeedsPermission("VIP"))
export class BingoGenerateCommand {

    @Slash({
        "name": "generate",
        "description": "Génère une grille de bingo",
    })
    async generate(
        @SlashOption({
            "name": "size",
            "description": "Définit la taille de la grille",
            "required": false,
            "type": ApplicationCommandOptionType.Integer,
            "minValue": 1,
            "maxValue": 24
        })
        size: number | undefined,
        @SlashOption({
            "name": "allow-duplicates",
            "description": "Permet de générer des doublons",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean,
        })
        allowDuplicates: boolean | undefined,
        @SlashOption({
            "name": "community",
            "description": "Permet de générer une grille de la communauté",
            "required": false,
            "type": ApplicationCommandOptionType.Boolean,
        })
        community: boolean | undefined,
        @SlashOption({
            "name": "difficulty",
            "description": "Définit la difficulté de la grille",
            "required": false,
            "type": ApplicationCommandOptionType.Integer,
            "minValue": 0,
            "maxValue": 1000
        })
        difficulty: number | undefined,
        @SlashOption(getSlashOptionForCoeff(1, "effect"))
        c1MaxEffects: number | undefined,
        @SlashOption(getSlashOptionForCoeff(2, "effect"))
        c2MaxEffects: number | undefined,
        @SlashOption(getSlashOptionForCoeff(3, "effect"))
        c3MaxEffects: number | undefined,
        @SlashOption(getSlashOptionForCoeff(4, "effect"))
        c4MaxEffects: number | undefined,
        @SlashOption(getSlashOptionForCoeff(5, "effect"))
        c5MaxEffects: number | undefined,
        @SlashOption(getSlashOptionForCoeff(6, "effect"))
        c6MaxEffects: number | undefined,
        @SlashOption(getSlashOptionForCoeff(7, "effect"))
        c7MaxEffects: number | undefined,
        @SlashOption(getSlashOptionForCoeff(1, "points"))
        c1MaxPoints: number | undefined,
        @SlashOption(getSlashOptionForCoeff(2, "points"))
        c2MaxPoints: number | undefined,
        @SlashOption(getSlashOptionForCoeff(3, "points"))
        c3MaxPoints: number | undefined,
        @SlashOption(getSlashOptionForCoeff(4, "points"))
        c4MaxPoints: number | undefined,
        @SlashOption(getSlashOptionForCoeff(5, "points"))
        c5MaxPoints: number | undefined,
        @SlashOption(getSlashOptionForCoeff(6, "points"))
        c6MaxPoints: number | undefined,
        @SlashOption(getSlashOptionForCoeff(7, "points"))
        c7MaxPoints: number | undefined,
        @SlashOption(getSlashOptionForCoeff(4, "generics"))
        c4MaxGenerics: number | undefined,
        @SlashOption(getSlashOptionForCoeff(5, "generics"))
        c5MaxGenerics: number | undefined,
        @SlashOption(getSlashOptionForCoeff(6, "generics"))
        c6MaxGenerics: number | undefined,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        const startedGenerationAt = Date.now()

        const options: BingoGenerateInputOptions = {
            "c1-max-effects": c1MaxEffects ?? Infinity,
            "c2-max-effects": c2MaxEffects ?? Infinity,
            "c3-max-effects": c3MaxEffects ?? Infinity,
            "c4-max-effects": c4MaxEffects ?? Infinity,
            "c5-max-effects": c5MaxEffects ?? Infinity,
            "c6-max-effects": c6MaxEffects ?? 0,
            "c7-max-effects": c7MaxEffects ?? 0,
            "c1-max-points": c1MaxPoints ?? Infinity,
            "c2-max-points": c2MaxPoints ?? Infinity,
            "c3-max-points": c3MaxPoints ?? Infinity,
            "c4-max-points": c4MaxPoints ?? Infinity,
            "c5-max-points": c5MaxPoints ?? Infinity,
            "c6-max-points": c6MaxPoints ?? 0,
            "c7-max-points": c7MaxPoints ?? 1,
            "c4-max-generics": c4MaxGenerics ?? 0,
            "c5-max-generics": c5MaxGenerics ?? 0,
            "c6-max-generics": c6MaxGenerics ?? 0,
            "size": size ?? 5,
            "allow-duplicates": allowDuplicates ?? false,
            "community": community ?? false,
            "difficulty": difficulty ?? 3,
        }

        const allItems = await getItems(31)

        const items_without_excluded = allItems.filter(item => ! EXCLUDED_IDS.includes(item.in_game_id));
        const allowed_items = items_without_excluded.filter(item => ![0].includes(item.coefficient)); // Remove the coeffs 0

        const totalWeights = await giveTotalWeights(allowed_items) // Get the total weights of all items
        
        if (community) {
            const notFinished = await CommunityBingo.findOne({
                "where": {
                    "ended_at": IsNull()
                }
            })

            if (notFinished) {
                throw new BotError(`Un autre Bingo communautaire est déjà en cours`)
            }
        }

        const rarities: RaritiesType = {}

        // Group by rarity and effect/points
        allowed_items.forEach(element => {
            if (! rarities[element.coefficient]) {
                rarities[element.coefficient] = {
                    "effect": [],
                    'points': []
                }
            }
            if (element.effect) {
                rarities[element.coefficient].effect.push(element)
            } else {
                rarities[element.coefficient].points.push(element)
            }
        });

        const elements: Array<BingoElementFactory> = await getAllowedFatories(options, allowed_items, totalWeights, rarities);

        let combination = findCombination(elements, options.difficulty, options.size);

        const composites: Array<SharpImage> = [];
        const gridElements: Array<BingoElement> = [];
        let actualDifficulties: number = 0;

        // Get the shift: it is applied to each row to ensure the diagonals are balanced
        const shift = ((max: number) => {
            for(let i = 2 ; i < max ; i++) {
                if (i % 2 === 0 && max % i === 0) {
                    return i;
                }
            }
            return 2;
        })(options.size)

        for(let y = 0 ; y < options.size ; y++) {
            for(let x = 0 ; x < options.size ; x++) {
                const combination_index = (x + (y * shift)) % options.size;
                let comb = combination[combination_index]

                const combinationElementMax = comb.decrementMax() // Decrement the max of the element (because it is used)
                if (combinationElementMax < 0) { // If the max is < 0, it means that the element is not allowed anymore
                    const eligibles = elements.filter(elem => elem !== comb &&
                        elem.difficulty > comb.difficulty / 1.75 &&
                        elem.difficulty < comb.difficulty * 1.75)
                    if (eligibles.length === 0) {
                        throw new BotError("Impossible de générer la grille. Essaye d'augmenter les maximums de certains types de cases")
                    }
                    const chosenReplacement = ArrayUtil.randomElement(eligibles)
                    combination[combination_index] = chosenReplacement
                    comb = chosenReplacement;
                }

                let item_generated = comb.generate()
                let try_counter = 0;

                while ((! options["allow-duplicates"]) && (! comb.allowDuplicates) && gridElements.some(it => it.equals(item_generated))) {
                    item_generated = comb.generate()
                    try_counter++;
                    if (try_counter > 2000) {
                        throw new BotError("Le grille n'a pas pu être générée. Essaye d'assigner le paramètre `allow-duplicates` à true.")
                    }
                }

                gridElements.push(item_generated)
                composites.push({
                    "top": y * BINGO_BOX_SIZE + 5 + BINGO_BOX_SIZE,
                    "left": x * BINGO_BOX_SIZE + 5 + BINGO_BOX_SIZE,
                    "input": await item_generated.display()
                })
                actualDifficulties += comb.difficulty;
            }

            // Add the numbers and letters
            composites.push({
                    "top": (y + 1) * BINGO_BOX_SIZE,
                    "left": 0,
                    "input": getBingoText(String.fromCharCode(65 + y))
                },
                {
                    "top": 0,
                    "left": (y + 1) * BINGO_BOX_SIZE,
                    "input": getBingoText((y + 1).toString())
                }
            )
        }

        // Add the top left corner
        const image = await getItemInGroup(0, Math.floor(Math.random() * 7))
        const padding = 5;
        const effectiveSize = BINGO_BOX_SIZE - 2 * padding;
        composites.push({
            "top": padding,
            "left": padding,
            "input": await BlessedAPI.Utils.Image.resize(image, {"width": effectiveSize, "height": effectiveSize})
        })

        let generated = sharp({
            "create": {
                "width": (options.size + 1) * BINGO_BOX_SIZE,
                "height": (options.size + 1) * BINGO_BOX_SIZE,
                "background": "#111111",
                "channels": 4,
            },
        })
        .composite(composites)

        function getBingoText(text: string): Buffer {
            return Buffer.from(`<svg width="${BINGO_BOX_SIZE}" height="${BINGO_BOX_SIZE}">
                <rect width="100%" height="100%" fill="none" stroke="#FFFFFF" />
                <style>
                    .title {
                        fill: #FFF;
                        font-size: 30px;
                        font-weight: bold;
                    }
                </style>
                <text x="50%" y="75%" color="white" text-anchor="middle" class="title">${text}</text>
            </svg>`);
        }

        const buff = await generated.png().toBuffer()

        const actualDifficulty = actualDifficulties / options.size ** 2 / DIFFICULTY_CONVERSION_COEFF;

        if (community) {
            const commu = new CommunityBingo();
            commu.difficulty = actualDifficulty;
            commu.size = options.size;
            commu.created_at = new Date();
            await CommunityBingo.save(commu)

            // Save the image
            fs.writeFileSync(BINGO_PATH, buff, {encoding: "binary"})

            const inserts = gridElements.map((_elem, index) => `(${commu.id}, ${(index % options.size) + 1}, ${Math.floor(index / options.size) + 1}, NULL, NULL)`)
            Connection.query(`INSERT INTO CommunityBingosBoxes VALUES ${inserts.join(",")}`)
        }

        const embed = new EmbedBuilder()
            .setTitle("Bingo")
            .addFields([
                {
                    "name": "Difficulté",
                    "value": BlessedAPI.Utils.Number.roundDecimal(actualDifficulty, 2).toString(),
                    "inline": true
                },
                {
                    "name": "Temps de génération",
                    "value": BlessedAPI.Utils.Number.roundDecimal((Date.now() - startedGenerationAt) / 1000, 1) + "s",
                    "inline": true
                }
            ])
            .setTimestamp()
            .setImage('attachment://image.png');

        interaction.editReply({
            "embeds": [embed],
            "files": [{name: "image.png", attachment: buff}]
        });
    }

}

type BingoGenerateOutputOptions = Required<BingoGenerateInputOptions>

const DIFFICULTY_CONVERSION_COEFF = 30;
const EXCLUDED_IDS: Array<number> = [
    111, 110, 26, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,
    52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 105, 109, 68,
    1223, 1224, 10, 103, 104, 1193, 1196, 1221, 1222
]
const WEIGHTS: Array<number> = [
    0, 2000, 1000, 300, 100, 10, 1, 60
]

async function giveTotalWeights(items: Array<Item>): Promise<TotalWeightsType> {
    const total: TotalWeightsType = {
        "effect": 0,
        "points": 0
    }
    items.forEach(obj => {
        const attribute = (obj.effect) ? "effect" : "points";
        total[attribute] += WEIGHTS[obj.coefficient]
    })
    return total;
}

async function getAllowedFatories(options: BingoGenerateOutputOptions, allowed_items: Array<Item>, totalWeights: TotalWeightsType, rarities: RaritiesType) {
    const elements: Array<BingoElementFactory> = [];

    if (options["c1-max-effects"] > 0) elements.push(await getMaxCoeff(options["c1-max-effects"], totalWeights, 1, rarities, "effect"))
    if (options["c2-max-effects"] > 0) elements.push(await getMaxCoeff(options["c2-max-effects"], totalWeights, 2, rarities, "effect"))
    if (options["c3-max-effects"] > 0) elements.push(await getMaxCoeff(options["c3-max-effects"], totalWeights, 3, rarities, "effect"))
    if (options["c4-max-effects"] > 0) elements.push(await getMaxCoeff(options["c4-max-effects"], totalWeights, 4, rarities, "effect"))
    if (options["c5-max-effects"] > 0) elements.push(await getMaxCoeff(options["c5-max-effects"], totalWeights, 5, rarities, "effect"))
    if (options["c6-max-effects"] > 0) elements.push(await getMaxCoeff(options["c6-max-effects"], totalWeights, 6, rarities, "effect"))
    if (options["c7-max-effects"] > 0) elements.push(await getMaxCoeff(options["c7-max-effects"], totalWeights, 7, rarities, "effect"))
    
    if (options["c1-max-points"] > 0) elements.push(await getMaxCoeff(options["c1-max-points"], totalWeights, 1, rarities, "points"))
    if (options["c2-max-points"] > 0) elements.push(await getMaxCoeff(options["c2-max-points"], totalWeights, 2, rarities, "points"))
    if (options["c3-max-points"] > 0) elements.push(await getMaxCoeff(options["c3-max-points"], totalWeights, 3, rarities, "points"))
    if (options["c4-max-points"] > 0) elements.push(await getMaxCoeff(options["c4-max-points"], totalWeights, 4, rarities, "points"))
    if (options["c5-max-points"] > 0) elements.push(await getMaxCoeff(options["c5-max-points"], totalWeights, 5, rarities, "points"))
    if (options["c6-max-points"] > 0) elements.push(await getMaxCoeff(options["c6-max-points"], totalWeights, 6, rarities, "points"))
    if (options["c7-max-points"] > 0) elements.push(await getMaxCoeff(options["c7-max-points"], totalWeights, 7, rarities, "points"))

    if (options["c4-max-generics"] > 0) elements.push(await getMaxGenerics(options["c4-max-generics"], allowed_items, totalWeights, 4));
    if (options["c5-max-generics"] > 0) elements.push(await getMaxGenerics(options["c5-max-generics"], allowed_items, totalWeights, 5));
    if (options["c6-max-generics"] > 0) elements.push(await getMaxGenerics(options["c6-max-generics"], allowed_items, totalWeights, 6));

    return elements.filter(elem => elem.max > 0);
}

async function getMaxCoeff(max: number, totalWeights: TotalWeightsType, coeff: number, rarities: RaritiesType, type: "effect" | "points"): Promise<BingoElementFactory> {
    const difficulty = WEIGHTS[coeff];
    const items = rarities[coeff][type];
    if (items.length === 0) {
        max = 0 // Disables the selection if there is no item in this category (like effect coeff 7)
    }
    return new BingoItemFactory(difficulty, totalWeights[type], items, max)
}

async function getMaxGenerics(max: number, allowed_items: Array<Item>, totalWeights: TotalWeightsType, coeff: number): Promise<BingoElementFactory> {
    const difficulty = (totalWeights.effect + totalWeights.points) / (WEIGHTS[coeff] * allowed_items.filter(item => item.coefficient === coeff).length)
    return new BingoGenericCoeffFactory(difficulty, coeff, max);
}

function findCombination(factories: Array<BingoElementFactory>, aimedDifficulty: number, size: number): Array<BingoElementFactory> {
    const reworkedDifficulty = aimedDifficulty * DIFFICULTY_CONVERSION_COEFF;
    factories.sort((a, b) => a.difficulty - b.difficulty); // Sort by difficulty

    // Find the factory closest to the aimed difficulty
    const closestToMean = factories.reduce((a, b) => {
        return Math.abs(b.difficulty - reworkedDifficulty) < Math.abs(a.difficulty - reworkedDifficulty) ? b : a;
    });

    // Find the index of the factory closest to the aimed difficulty
    const closestIndexToMeanDifficulty = factories.indexOf(closestToMean);
    
    // Create a gaussian distribution centered on the closest factory
    let gauss = gaussian(closestIndexToMeanDifficulty, 2);

    const gaussVarianceCoeff = 1.1;

    const alreadyTriedCombinations: Array<Array<number>> = [];

    const tries = 1_000;

    const combinations: Array<Array<BingoElementFactory>> = [];

    // Generate all combinations
    for (let i = 0; i < tries; i++) {
        let indexes = gauss.random(size)
            .map(x => {
                const rounded = Math.round(x);
                const positive = Math.max(rounded, 0);
                const narrowed = Math.min(positive, factories.length -1)
                return narrowed
            })

        indexes.sort((a, b) => a - b); // Sort the indexes

        // If the combination has already been tried, generate a new one
        if (alreadyTriedCombinations.some(x => x.every((y, i) => y === indexes[i]))) {
            // Reset variance
            gauss.variance /= gaussVarianceCoeff;
            continue;
        }
        alreadyTriedCombinations.push(indexes);
        
        combinations.push(indexes.map(x => factories[x]));

        gauss.variance *= gaussVarianceCoeff;
    }

    // Return the combination closest to the aimed difficulty
    const means: Array<number> = combinations.map(x => x.reduce((acc, curr) => acc + curr.difficulty, 0) / x.length / DIFFICULTY_CONVERSION_COEFF);

    const closestToAimedDifficulty = combinations.reduce((a, b) => {
        return Math.abs(means[combinations.indexOf(b)] - aimedDifficulty) < Math.abs(means[combinations.indexOf(a)] - aimedDifficulty) ? b : a;
    });

    return closestToAimedDifficulty;
}
