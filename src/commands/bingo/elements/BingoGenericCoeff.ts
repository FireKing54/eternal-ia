import { BingoElement, BingoElementFactory } from "./BingoElement";

class BingoGenericCoeffFactory extends BingoElementFactory {

    public coeff: number;

    constructor(difficulty: number, coeff: number, max: number) {
        super(difficulty, true, max)
        this.coeff = coeff;
    }

    generate(): BingoElement {
        return new BingoGenericCoeff(this.difficulty, this.coeff)
    }
    
}

function svgText(text: string): string {
    const height = 40;
    return `<svg width="${height}" height="${height}" viewBox="0 0 ${height} ${height + 2}">
        <rect x="0" y="0" width="100%" height="100%" fill="#111"/>
        <text x="50%" y="50%" text-anchor="middle" dy="0.25em" fill="#bbb">Coeff ${text}</text>
    </svg>`;

}

class BingoGenericCoeff extends BingoElement {

    public difficulty: number;
    public coeff: number;
    
    constructor(difficulty: number, coeff: number) {
        super()
        this.difficulty = difficulty
        this.coeff = coeff;
    }
    
    async display(): Promise<Buffer> {
        const text = svgText("" + this.coeff)
        return Buffer.from(text)
    }

    equals(other: BingoElement): boolean {
        if (other instanceof BingoGenericCoeff) {
            if (this.coeff === other.coeff) {
                return true
            }
        }
        return false
    }
}

export {
    BingoElement,
    BingoGenericCoeffFactory
}