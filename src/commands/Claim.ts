import { RoleModel } from "../database/models/Role";
import { ActionRowBuilder, ApplicationCommandOptionType, Attachment, ButtonBuilder, ButtonInteraction, ButtonStyle, channelMention, Colors, CommandInteraction, EmbedBuilder, GuildMember, ModalBuilder, ModalSubmitInteraction, Role, roleMention, TextChannel, TextInputBuilder, TextInputStyle, userMention } from "discord.js";
import { ButtonComponent, Discord, Slash, SlashOption, Guild, ModalComponent, Guard } from "discordx";
import { ENV } from "../Environment";
import BotError from "../errors/BotError";
import { getGameresultLink } from "./Gameresult";
import { buildAwardEmbed, handleAward } from "./Award";
import { hasPermission, NeedsPermission } from "../permissions/Permissions";
import { EmbedService } from "../embeds/embed.service";
import { BotException } from "../errors/bot.exception";

@Discord()
@Guild(ENV.MAIN_GUILD)
export class Claim {

    @Slash({
        "name": "claim",
        "description": "Permet de demander un titre",
        "dmPermission": false
    })
    async claim(
        @SlashOption({
            "name": "title",
            "description": "Titre à demander",
            "required": true,
            "type": ApplicationCommandOptionType.Role,
        })
        title: Role,
        @SlashOption({
            "name": "ef-run-id",
            "description": "ID de fin de partie Eternalfest",
            "required": false,
            "type": ApplicationCommandOptionType.String,
            "minLength": 36, // 36 is the length of a UUID,
            "maxLength": 100
        })
        efRunID: string,
        @SlashOption({
            "name": "screenshot",
            "description": "Capture d'écran servant de preuve supplémentaire",
            "required": false,
            "type": ApplicationCommandOptionType.Attachment,
        })
        attachment: Attachment,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        const registeredRoles = await RoleModel.find();
        if (! registeredRoles.find(r => r.id === title.id)) {
            throw new BotException(
                `Le titre ${roleMention(title.id)} ne compte pas pour les méta-titres, et ne peut donc pas être demandé`
            );
        }

        const member = interaction.member! as GuildMember;

        // Throw an error if the user already has the role
        if (member.roles.cache.find(r => r.id === title.id)) {
            throw new BotError(`Tu possèdes déjà le titre ${roleMention(title.id)}`);
        }

        const fields: Array<string> = [];

        if (efRunID) {
            fields.push(`[Gameresult](${getGameresultLink(efRunID)})`);
        }

        const embed = new EmbedBuilder()
            .setTitle("Demande de titre")
            .setDescription(`${userMention(member.id)} demande le titre ${roleMention(title.id)}`)
            .addFields([
                {
                    "name": "Preuves",
                    "value": fields.join("\n") || "Aucune"
                }
            ])
            .setThumbnail(interaction.user.avatarURL() ?? interaction.user.defaultAvatarURL)
            .setTimestamp()

        if (attachment) {
            embed.setImage(attachment.url);
        }

        // Get the Claim channel
        const claimChannel = interaction.guild?.channels.cache.find(c => c.id === ENV.ROLE_CLAIM_CHANNEL) as TextChannel;
        if (! claimChannel) {
            throw new BotError("Impossible de trouver le salon de demande de titre");
        }

        // Create the buttons
        const acceptButton = new ButtonBuilder()
            .setCustomId("accept-claim")
            .setLabel("Accepter")
            .setStyle(ButtonStyle.Success);

        const refuseButton = new ButtonBuilder()
            .setCustomId("refuse-claim")
            .setLabel("Refuser")
            .setStyle(ButtonStyle.Danger);

        // Create the action row
        const actionRow = new ActionRowBuilder<ButtonBuilder>()
            .addComponents([acceptButton, refuseButton]);

        // Send the message
        await claimChannel.send({
            "embeds": [embed],
            "components": [actionRow]
        });

        const res = EmbedService.buildSuccess({
            "title": "Demande de titre envoyée",
            "description": `Ta demande de titre a bien été envoyée ! Elle se trouve dans le salon ${channelMention(claimChannel.id)}`
        })

        await interaction.editReply({
            "embeds": [res],
        });
    }

    @ButtonComponent({"id": "accept-claim"})
    @Guard(NeedsPermission("MODERATOR"))
    async accept(interaction: ButtonInteraction) {
        if (! await hasPermission(interaction.user.id, "MODERATOR")) {
            throw new BotError("Tu n'as pas la permission d'accepter un titre");
        }

        await interaction.deferUpdate();

        const role = interaction.message?.embeds[0].description?.match(/<@&(\d+)>/)?.[1];
        if (! role) {
            throw new BotError("Impossible de trouver le rôle demandé");
        }
        const roleID = role.replace(/<@&(\d+)>/, "$1"); // Remove the mention
        const discordRole = interaction.guild?.roles.cache.find(r => r.id === roleID);

        const memberID = interaction.message?.embeds[0].description?.match(/<@(\d+)>/)?.[1];
        if (! memberID) {
            throw new BotError("Impossible de trouver le membre");
        }
        const member = interaction.guild?.members.cache.find(m => m.id === memberID)!;

        const newEmbed = new EmbedBuilder(interaction.message?.embeds[0].toJSON())
            .setFooter({
                "text": `Accepté par ${interaction.user.username}`,
                "iconURL": interaction.user.avatarURL() ?? "",
            })
            .setTimestamp()
            .setColor(Colors.Green);

        const awardInfo = await handleAward(interaction.guild!, member, discordRole!);
        const awardEmbed = buildAwardEmbed(member, awardInfo);

        // React with clapping hands
        await interaction.message.react("👏");

        await interaction.editReply({
            "embeds": [newEmbed, awardEmbed],
            "components": [],
        });
    }

    @ButtonComponent({"id": "refuse-claim"})
    @Guard(NeedsPermission("MODERATOR"))
    async refuse(interaction: ButtonInteraction) {

        if (! await hasPermission(interaction.user.id, "MODERATOR")) {
            throw new BotError("Tu n'as pas la permission d'utiliser cette commande");
        }

        const modal = new ModalBuilder()
            .setTitle("Raison du refus")
            .setCustomId("refuse_reason")

        const modalRow = new TextInputBuilder()
            .setCustomId("refuse_reason_text")
            .setPlaceholder("Raison du refus")
            .setLabel("Raison du refus")
            .setRequired(true)
            .setMaxLength(100)
            .setStyle(TextInputStyle.Short)

        const modalActionRow = new ActionRowBuilder<TextInputBuilder>()
            .addComponents([modalRow]);

        modal.addComponents([modalActionRow]);

        await interaction.showModal(modal);
    }

    @ModalComponent({"id": "refuse_reason"})
    async refuseReason(interaction: ModalSubmitInteraction) {
        await interaction.deferUpdate();

        const reason = interaction.fields.getTextInputValue("refuse_reason_text");

        // Get the embed builder
        const embed = new EmbedBuilder(interaction.message?.embeds[0].toJSON())
            .addFields([
                {
                    "name": "Raison du refus",
                    "value": reason
                }
            ])
            .setFooter({
                "text": `Refusé par ${interaction.user.username}`,
                "iconURL": interaction.member?.avatar ?? interaction.user.avatarURL() ?? ""
            })
            .setTimestamp()
            .setColor([255, 0, 0]);
        
        interaction.message?.edit({
            "embeds": [embed],
            "components": [],
        })
    }
}