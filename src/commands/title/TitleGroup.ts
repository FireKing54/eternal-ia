import { RoleModel } from "../../database/models/Role";
import { RoleGroup } from "../../database/models/RoleGroup";
import { RoleGroupItem } from "../../database/models/RoleGroupItems";
import { ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction, Role, roleMention } from "discord.js";
import { Discord, Guard, Guild, Slash, SlashGroup, SlashOption } from "discordx";
import { EmbedService } from "../../embeds/embed.service";
import { ENV } from "../../Environment";
import BotError from "../../errors/BotError";
import { buildTable } from "../../modules/Display";
import { PaginationService } from "../../modules/pagination/pagination.service";
import { NeedsPermission } from "../../permissions/Permissions";

@Discord()
@SlashGroup({
    "name": "group",
    "description": "Gestion des groupes de titres",
    "root": "title"
})
@SlashGroup("group", "title")
@Guild(ENV.MAIN_GUILD)
export class TitleGroup {

    @Slash({
        "name": "list",
        "description": "Liste tous les groupes de titres",
        "dmPermission": false
    })
    async list(interaction: CommandInteraction) {
        await interaction.deferReply();
        const items = (await RoleGroupItem.find()).sort((a, b) => a.idgroup - b.idgroup);

        const display = buildTable({
            "title": "Groupes de titres",
            "headers": ["Titre", "Niveau"],
            "data": items.map(item => [
                interaction.guild?.roles.cache.get(item.idrole)?.name ?? "Inconnu",
                item.rank.toString()
            ])
        });

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": display.map(disp => {
                    return {
                        "content": disp,
                    }
                })
            }
        })

        await interaction.editReply(pagination)
    }

    @Slash({
        "name": "register",
        "description": "Enregistre un groupe de titres",
        "dmPermission": false
    })
    @Guard(NeedsPermission("ADMINISTRATOR"))
    async register(
        @SlashOption({
            "name": "name",
            "description": "Nom du groupe",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "maxLength": 50,
        })
        name: string,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        let group = await RoleGroup.findOne({
            "where": {
                "name": name,
            },
        });
        const created = !group;
        if (created) {
            group = new RoleGroup();
            group.name = name;
            await RoleGroup.save(group);
        }

        if (created) {
            const embed = EmbedService.buildSuccess({
                "title": "Groupe créé",
                "description": `Le groupe \`${name}\` a été créé`
            });
            await interaction.editReply({
                "embeds": [embed],
            });
        }
        else {
            RoleGroup.remove(group!);
            const embed = EmbedService.buildSuccess({
                "title": "Groupe supprimé",
                "description": `Le groupe \`${name}\` a été supprimé, car il existait déjà`
            });
            await interaction.editReply({
                "embeds": [embed],
            });
        }
    }

    @Slash({
        "name": "assign",
        "description": "Ajoute un titre à un groupe",
        "dmPermission": false
    })
    @Guard(NeedsPermission("ADMINISTRATOR"))
    async assign(
        @SlashOption({
            "name": "group",
            "description": "Groupe de titres",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (interaction: AutocompleteInteraction) => {
                const groups = await RoleGroup.find({"select": ["name"]});
                await interaction.respond(groups.map(group => {
                    return {
                        "name": group.name,
                        "value": group.name,
                    }
                }))
            }
        })
        groupName: string,
        @SlashOption({
            "name": "role",
            "description": "Titre à ajouter",
            "required": true,
            "type": ApplicationCommandOptionType.Role,
        })
        role: Role,
        @SlashOption({
            "name": "rank",
            "description": "Niveau du titre",
            "required": true,
            "type": ApplicationCommandOptionType.Integer,
            "minValue": 1,
        })
        rank: number,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        const group = await RoleGroup.findOne({
            "where": {
                "name": groupName,
            },
        });

        if (! group) {
            throw new BotError(`Le groupe \`${groupName}\` n'existe pas`);
        }

        const roleModelCount = await RoleModel.count({
            "where": {
                "id": role.id,
            },
        });

        if (roleModelCount === 0) {
            throw new BotError(`Le titre ${roleMention(role.id)} n'est pas enregistré en tant que titre qui compte dans le classement`);
        }

        let item = await RoleGroupItem.findOne({
            "where": {
                "idgroup": group.id,
                "idrole": role.id,
            }
        });
        const beenCreated = ! item;
        if (! item) {
            item = new RoleGroupItem();
            item.idgroup = group.id;
            item.idrole = role.id;
            item.rank = rank;
            RoleGroupItem.save(item);
        }

        if (beenCreated) {
            const embed = EmbedService.buildSuccess({
                "title": "Titre ajouté",
                "description": `Le titre ${roleMention(role.id)} a été ajouté au groupe \`${groupName}\` au rang **${rank}**.`
            });
            await interaction.editReply({
                "embeds": [embed],
            });
        } else {
            item.rank = rank;
            RoleGroupItem.save(item);

            const embed = EmbedService.buildSuccess({
                "title": "Titre mis à jour",
                "description": `Le titre ${roleMention(role.id)} a été mis à jour au groupe \`${groupName}\` au rang **${rank}**.`
            });
            await interaction.editReply({
                "embeds": [embed],
            });
        }
    }

}