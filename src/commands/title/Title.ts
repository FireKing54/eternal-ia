import { buildAwardEmbed } from "../Award";
import { RoleModel } from "../../database/models/Role";
import { RoleGroup } from "../../database/models/RoleGroup";
import { RoleGroupItem } from "../../database/models/RoleGroupItems";
import { ApplicationCommandOptionType, CommandInteraction, EmbedBuilder, GuildMember, Role, roleMention } from "discord.js";
import { Discord, Guard, Guild, Slash, SlashGroup, SlashOption } from "discordx";
import { EmbedService } from "../../embeds/embed.service";
import { ENV } from "../../Environment";
import { buildTable } from "../../modules/Display";
import { PaginationService } from "../../modules/pagination/pagination.service";
import { NeedsPermission } from "../../permissions/Permissions";

@Discord()
@SlashGroup({
    "name": "title",
    "description": "Gestion des titres",
})
@SlashGroup("title")
@Guild(ENV.MAIN_GUILD)
export class Title {

    @Guard(NeedsPermission("ADMINISTRATOR"))
    @Slash({
        "dmPermission": false,
        "name": "register",
        "description": "Enregistre un titre dans la liste de ceux qui comptent pour le classement",
    })
    async register(
        @SlashOption({
            "name": "role",
            "description": "Rôle à enregistrer",
            "required": true,
            "type": ApplicationCommandOptionType.Role
        })
        title: Role,
        @SlashOption({
            "name": "coeff",
            "description": "Coefficient du titre",
            "required": false,
            "type": ApplicationCommandOptionType.Integer,
        })
        coeff: number | undefined,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        coeff ??= 1;

        const role = await RoleModel.findOne({
            "where": {
                "id": title.id,
            },
        });

        if (role) {
            // If the role exists and the coeff is different, update it
            if (role.coeff !== coeff) {

                role.coeff = coeff;
                await RoleModel.save(role)

                const embed = EmbedService.buildSuccess({
                    "title": "Titre modifié",
                    "description": `Le titre \`${title.name}\` a été **modifié** avec un coefficient de ${coeff}.`
                });

                return interaction.editReply({
                    "embeds": [embed]
                });
            }
            // If the role exists and the coeff is the same, delete it
            else {
                await RoleModel.remove(role);

                const embed = EmbedService.buildSuccess({
                    "title": "Titre supprimé",
                    "description": `Le titre \`${title.name}\` a été **supprimé** de la liste des titres qui comptent pour le classement.`
                });

                return interaction.editReply({
                    "embeds": [embed]
                });
            }
        }

        // If the role doesn't exist, create it

        await RoleModel.insert({
            "id": title.id,
            "coeff": coeff
        });

        const embed = EmbedService.buildSuccess({
            "title": "Titre enregistré",
            "description": `Le titre \`${title.name}\` a été **enregistré** avec un coefficient de ${coeff}.`
        });

        return interaction.editReply({
            "embeds": [embed]
        });
    }

    @Slash({
        "dmPermission": false,
        "name": "list",
        "description": "Liste les titres enregistrés",
    })
    async list(
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        const allTitles = await RoleModel.find();

        const displays = buildTable({
            "title": "Titres enregistrés",
            "headers": ["Titre", "Coefficient"],
            "data": allTitles.map(title => {
                const role = interaction.guild?.roles.cache.get(title.id);
                return [
                    role?.name ?? title.id,
                    "" + title.coeff
                ]
            })
        })

        const pages = displays.map(display => {
            return {
                "content": display
            }
        })
        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": pages
            }  
        })

        await interaction.editReply(pagination)
    }

    @Slash({
        "dmPermission": false,
        "name": "rank",
        "description": "Affiche le classement des titres",
    })
    async rank(
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        const serverRanking = await RoleModel.getDiscordServerRankings(interaction.guild!);

        const displays = buildTable({
            "title": "Classement des titres",
            "headers": ["Position", "Joueur", "Nombre de titres", "Méta titre"],
            "data": serverRanking.map((player, index) => {
                return [
                    "" + (index + 1),
                    player.username,
                    "" + player.nbRoles,
                    player.metaTitleName
                ]
            })
        })

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": displays.map(display => {
                    return {
                        "content": display
                    }
                })
            }
        })

        await interaction.editReply(pagination);

    }

    @Slash({
        "dmPermission": false,
        "name": "missing",
        "description": "Montre les titres manquants du joueur",
    })
    async missing(
        @SlashOption({
            "name": "joueur",
            "description": "Joueur dont on veut voir les titres manquants",
            "required": false,
            "type": ApplicationCommandOptionType.User
        })
        player: GuildMember,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        player ??= interaction.member as GuildMember;

        const allTitles = await RoleModel.find();
        const missingTitles = allTitles.filter(title => !player.roles.cache.has(title.id));
        let realMissingTitles: Array<RoleModel> = [];

        const upgradableTitles = await RoleGroup.getUpgradableRoles(player);

        for(const title of missingTitles) {
            const allTitlesInGroup = await RoleGroup.getAllRolesInSameGroup(title.id);
            if (allTitlesInGroup.length === 0) { // Add in the array the titles that are not grouped
                realMissingTitles.push(title);
            }
            else {
                // If the user has none of the titles in the group, add the lowest ranked title
                if (allTitlesInGroup.every(title => !player.roles.cache.has(title.idrole))) {
                    const lowestRankedTitle: RoleGroupItem = allTitlesInGroup.reduce((previous, current) => {
                        return previous.rank < current.rank ? previous : current;
                    });
                    const convertedTitle = await RoleModel.findOne({
                        "where": {
                            "id": lowestRankedTitle?.idrole
                        }
                    })!;
                    realMissingTitles.push(convertedTitle!);
                }
            }
        }

        // Remove duplicates
        realMissingTitles = realMissingTitles.filter((title, index) => {
            return realMissingTitles.findIndex(t => t.id === title.id) === index;
        })

        const embeds: Array<EmbedBuilder> = [];

        const upgradleField = {
            "name": "Titres améliorables",
            "value": upgradableTitles.length > 0 ? upgradableTitles.map(title => {
                return `${roleMention(title.newRole.id)} (amélioration de ${roleMention(title.originalRole.id)})`;
            }).join("\n") : "Tu as amélioré au maximum tes titres actuels"
        }

        if (realMissingTitles.length === 0) {
            const embed = this.getEmbed(player);
            embed.setFields([
                {
                    "name": "Titres manquants",
                    "value": "Wow, tu as réussi à collectionner tous les titres ! Bravo !"
                },
                upgradleField
            ])
            return interaction.editReply({
                "embeds": [embed]
            }).then(message => {
                message.react("🎉");
            })
        }

        const TITLES_PER_PAGE = 10;
        for(let index = 0; index < realMissingTitles.length; index += TITLES_PER_PAGE) {
            embeds.push(
                this.getEmbed(player)
            );

            const value = realMissingTitles.slice(index, index + TITLES_PER_PAGE).map(title => {
                return roleMention(title.id);
            }).join("\n");

            embeds[embeds.length - 1].addFields([
                {
                    "name": "Titres manquants",
                    "value": value,
                },
                upgradleField
            ])

            embeds[embeds.length - 1].setFooter({
                "text": `Page ${embeds.length} / ${Math.ceil(realMissingTitles.length / TITLES_PER_PAGE)}`
            });
        }

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": embeds.map(embed => {
                    return {
                        "embeds": [embed]
                    }
                })
            }
        })

        await interaction.editReply(pagination);
    }

    @Slash({
        "dmPermission": false,
        "name": "recap",
        "description": "Affiche un récapitulatif des titres du joueur",
    })
    async recap(
        @SlashOption({
            "name": "joueur",
            "description": "Joueur dont on veut voir les titres manquants",
            "required": false,
            "type": ApplicationCommandOptionType.User
        })
        player: GuildMember,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        player ??= interaction.member as GuildMember;

        const rankings = await RoleModel.getDiscordServerRankings(interaction.guild!);
        const { rank, metaTitleName, nbRoles } = rankings.find(ranking => ranking.username === player.displayName)!;

        const embed = buildAwardEmbed(player, {
            rank,
            metaTitle: player.guild.roles.cache.find(role => role.name === metaTitleName)!,
            "titles": nbRoles,
            removedRoles: []
        })

        await interaction.editReply({
            "embeds": [embed]
        });
    }

    getEmbed(player: GuildMember) {
        return new EmbedBuilder()
            .setAuthor({
                "name": player.displayName
            })
            .setThumbnail(player.user.displayAvatarURL())
            .setTimestamp()
    }
}