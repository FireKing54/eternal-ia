import { Question } from "../../questions/Question";
import { getQuizQuestionEmbed } from "../../QuizStart";

interface GeneralCultureQuestion {
    question: string;
    answers: Array<string>
}

const Bya = ["Bya", "max8312083"]
const maxdefolsch = ["maxdefolsch"]
const Jj = ["Jj", "jjdu51", "EternalSadist"]
const lokapi = ["Lokapi", "lekapi", "EternalCool"]
const globox = ["Globox", "Globox62", "EternalBot"]
const BlessedRacc = ["BlessedRacc"]
const yuuty = ["yuuty", "Dashell"]
const Erneraude = ["Erneraude"];
const kahakatashi = ["kahakatashi"];
const globZOsiris = ["GlobZOsiris"];
const Destructeur = ["Destructeur"];
const Docthib = ["Docthib"];
const mrloxa = ["mrloxa"]
const Gary = ["Gary", "GardienTO"]
const Xirty = ["Xirty"]
const Deepnight = ["Deepnight"]
const Thepoule = ["Thepoule"]
const Rollerfou = ["Rollerfou"]

let generalCulture: Array<GeneralCultureQuestion> = [
    {
        "question": "Pour l'anniversaire de quel **joueur** le **Cénotaphe de Byakuren** a-t-il été conçu ?",
        "answers": Bya
    },
    {
        "question": "Quel **joueur** est connu et souvent charié pour \"jouer avec la Pause\" ?",
        "answers": maxdefolsch
    },
    {
        "question": "Quel **créateur de contrée** est connu pour dissimuler de nombreux **pics cachés** ?",
        "answers": Jj
    },
    {
        "question": "Quel **membre d'Eternalfest** est **professeur de mathématiques** ?",
        "answers": lokapi
    },
    {
        "question": "Qui est la **première personne** à avoir **débloqué un coeff 6** autre que le Piouz ?",
        "answers": ["Joekickass"]
    },
    {
        "question": "Qui est la **première personne** à avoir **débloqué 346 objets** ?",
        "answers": ["fabianh"]
    },
    {
        "question": "Quel joueur a exploité le bug du niveau 33.? d'Officialfest pour débloquer le **laxatif aux amandes** ?",
        "answers": globox
    },
    {
        "question": "Quelle est la **première personne** à avoir **fini la vraie fin** du Supplice des Téméraires ?",
        "answers": Bya
    },
    {
        "question": "Qui m'a codé ?",
        "answers": BlessedRacc
    },
    {
        "question": "Qui est le **créateur** du site **Hamm4all** ?",
        "answers": BlessedRacc
    },
    {
        "question": "Qui est le **co-fondateur** du site **Hamm4all** ?",
        "answers": yuuty
    },
    {
        "question": "Qui a créé le site **gameresult**, permettant de visionner ses résulats de partie ?",
        "answers": Erneraude
    },
    {
        "question": "Quel **joueur** a découvert la technique du **tiers-étage** ?",
        "answers": kahakatashi
    },
    {
        "question": "Quel **joueur** a découvert la technique de \"**l'élévation des monstres**\" ?",
        "answers": globZOsiris
    },
    {
        "question": "Quel **joueur** a découvert le fonctionnement du mécanisme de gros cristaux ?",
        "answers": ["Ally"]
    },
    {
        "question": "Qui a **découvert** la **Technique du Cache-Ennemi** ?",
        "answers": kahakatashi
    },
    {
        "question": "Qui a **découvert** le fonctionnement des lettres CRISTAL (big bang maximal du niveau précédent) ?",
        "answers": globZOsiris
    },
    {
        "question": "Quel **joueur** fut le premier à **compléter toutes les familles** ?",
        "answers": Destructeur
    },
    {
        "question": "Qui est le **premier joueur** a avoir vaincu **Tuberculoz** ?",
        "answers": Docthib
    },
    {
        "question": "Qui fut le **premier panthéonien** ?",
        "answers": mrloxa
    },
    {
        "question": "Qui fut le **premier joueur** à s'emparer du **Pass Pyramide** ?",
        "answers": globZOsiris
    },
    {
        "question": "Qui fut le **premier joueur** à s'emparer du **Pad Frusion 64** ?",
        "answers": globZOsiris
    },
    {
        "question": "Qui fut le **premier joueur** à s'emparer de la mythique **Neige-ô-Glycérine** ?",
        "answers": globZOsiris
    },
    {
        "question": "Dans **quel pays** se situe la ville d'**Hammerfest** ?",
        "answers": ["Norvège"]
    },
    {
        "question": "Comment s'appelle le **boss final** du jeu ?",
        "answers": ["Tuber", "Tuberculoz"]
    },
    {
        "question": "Quel était le **niveau maximum** atteignable à la sortie du jeu ?",
        "answers": ["114"]
    },
    {
        "question": "En **quelle année** le jeu est-il sorti (en France) ?",
        "answers": ["2006"]
    },
];

const EF: Array<{eternal: string, player: Array<string>}> = [
    {
        "eternal": "EternalPGM",
        "player": ["maxdefolsch"]
    },
    {
        "eternal": "Eternalfest",
        "player": ["Demurgos", "Elseabora"]
    },
    {
        "eternal": "EternalSky",
        "player": ["Elagea"]
    },
    {
        "eternal": "EternalGorithm",
        "player": ["moulins"]
    },
    {
        "eternal": "EternalLèle",
        "player": ["Stef87"]
    },
    {
        "eternal": "EternalSadist",
        "player": ["Jj", "jjdu51"]
    },
    {
        "eternal": "EternalArming",
        "player": Bya
    },
    {
        "eternal": "EternalLiteration",
        "player": ["arounet"]
    },
    {
        "eternal": "EternalEd",
        "player": ["Aksamyt"]
    },
    {
        "eternal": "EternalCool",
        "player": ["Lokapi", "lekapi"]
    },
    {
        "eternal": "EternalLegro",
        "player": ["Crapaud21"]
    },
    {
        "eternal": "EternalBot",
        "player": ["Globox", "Globox62", "Kyosset"]
    },
    {
        "eternal": "EternalGebra",
        "player": ["Jules93"]
    },
    {
        "eternal": "EternalCruel",
        "player": ["Maxadona"]
    },
    {
        "eternal": "EternalAbourre",
        "player": ["Lougarelou", "Lougarelou11"]
    }
]

generalCulture.push(...EF.map(member => {
    return {
        "question": `A quel **joueur** est associé le pseudo Eternal **${member.eternal}** ?`,
        "answers": member.player
    }
}));

generalCulture.push({
        "question": "Quel **objet** provoque actuellement un bug qui **fige les quêtes** ?",
        "answers": ["Neige-ô-Glycérine", "Snow Bomb"]
    },
    {
        "question": "Depuis **quelle année** la montée au panthéon est-elle **impossible** ?",
        "answers": ["2017"]
    },
    {
        "question": "Quel **joueur** détient le record du plus haut score sur **Hammerfest** ?",
        "answers": ["Vaxos"]
    },
    {
        "question": "Quel **joueur** détient le record du plus haut score en **Rétro Igor** ?",
        "answers": ["Vaxos"]
    },
    {
        "question": "Quel **joueur** détient le record du plus haut score réalisé sur **Hammerfest** en **Ninjutsu** ?",
        "answers": ["philippe1"]
    },
    {
        "question": "Quel est le **nom** donné à la **Boule de Feu** ?",
        "answers": ["Georges"]
    },
    {
        "question": "Combien de **bondissantes** y a-t-il dans le **niveau 99** ?",
        "answers": ["9"]
    },
    {
        "question": "Quel **niveau** du jeu de base contient des plateformes **glissantes qui ne sont pas recouvertes de glace** ?",
        "answers": ["77.3"]
    },
    {
        "question": "De quel **valeur** est le bonus que l'on obtient en faisant **CRISTAL dans l'ordre** ?",
        "answers": ["150000", "150 000"]
    },
    {
        "question": "Quel fruit se fait **couper en deux** quand les portes de **l'ascenseur** se referment, au **niveau 103** ?",
        "answers": ["Citron", "Lemon"]
    },
    {
        "question": "A quel **niveau** se situe l'entrée d'une des **rares dimensions** qui n'ont **pas de nom** ?",
        "answers": ["72.1", "15.11", "74"]
    },
    {
        "question": "Combien de **coups** sont nécessaires pour tuer la **chauve-souris au niveau 100** ?",
        'answers': ["3"]
    },
    {
        "question": "Comment s'appelle le **niveau** qui permet de **prendre le même objet** un nombre illimité de fois, en suivant une certaine technique ?",
        "answers": ["33.?"]
    },
    {
        "question": "Quel niveau peut permettre de récupérer **2 cannes de Bobble** avec la bonne technique ?",
        "answers": ["33.0.2"]
    },
    {
        "question": "Quel joueur fut **le premier** à s'emparer des **2 Cannes de Booble** du niveau 33.0.2 sans perdre de vie avec seulement **deux bombes** ?",
        "answers": maxdefolsch
    },
    {
        "question": "Quel membre **d'Eternalfest** est connu pour **ne pas aimer le mode Tornade** ?",
        "answers": lokapi
    },
    {
        "question": "Qui est le **vainqueur** du **Tounoi Soccerfest 2019** ?",
        "answers": Gary
    },
    {
        "question": "Qui est le **vainqueur** du **Tounoi Soccerfest 2020** ?",
        "answers": Gary
    },
    {
        "question": "Qui est le **vainqueur** du **Tounoi Soccerfest Hivernal 2021** ?",
        "answers": yuuty
    },
    {
        "question": "Au bout de combien de fruits tués l'**ig'or** apparait-il, au **niveau 101** ?",
        "answers": ["30"]
    },
    {
        "question": "Quel est le **nom** de **l'option de jeu** permettant de voir des **bots** jouer au jeu à notre place dans Eternalfest ?",
        "answers": ["Randomfest"]
    },
    {
        "question": "Quel est le **nom** du **mode de jeu abandonné** se servant de la barre de chargement comme d'un **compteur de souffle** ?",
        "answers": ["Aqua"]
    },
    {
        "question": "Quel est le **nom** du **mode de jeu multicoopératif abandonné** où Sandy contrôle les bombes d'Igor ?",
        "answers": ["Bombotopia"]
    },
    {
        "question": "Quelle **contrée du premier avril** est devenue une option de jeu d'**Hammerfest Deluxe Edition** ?",
        "answers": ["Hammerfest II", "Hammerfest 2"]
    },
    {
        "question": "Quel est le **niveau maximal** actuellement atteignable dans le jeu de base ?",
        "answers": ["106"]
    },
    {
        "question": "Quel est le **seul joueur** ayant le **niveau 115** comme niveau maximum sur sa **fiche publique** ?",
        "answers": ["Vacerne"]
    },
    {
        "question": "Quel est le **nom** de la dimension permettant de se rendre dans la **dimension 82** grâce au **saut de dimension** ?",
        "answers": ["Petite salle bonus"]
    },
    {
        "question": "Quel est le **seul niveau** du jeu de base où la **Boule de feu** arrive dès l'entrée dans le niveau ?",
        "answers": ["16.0"]
    },
    {
        "question": "Comment s'appelle la **clé** supposée ouvrir la **dimension 86.1** ?",
        "answers": ["Clé d'inversion"]
    },
    {
        "question": "Quel mode de jeu est nécessaire pour pouvoir accéder à la **dimension 86** ?",
        "answers": ["Miroir", "Mirror"]
    },
    {
        "question": "Comment s'appelle la **quête** permettant de débloquer l'option de jeu **Tornade** ?",
        "answers": ["Meilleur Joueur"]
    },
    {
        "question": "Comment s'appelle la **quête** permettant de débloquer l'option de jeu **Explosifs Instables** ?",
        "answers": ["Maître des Bombes"]
    },
    {
        "question": "Quel **mode de jeu** permet d'accéder au célèbre niveau **ololzd** dans **Eternel Recommencement** ?",
        "answers": ["Explosifs Instables", "Bomb expert"]
    },
    {
        "question": "Quel **joueur** a découvert une technique pour faire un temps **inférieur à 600ms** au **Time Attack** ?",
        "answers": maxdefolsch
    },
    {
        "question": "Combien de **terrains Soccerfest** existe-t-il dans le **jeu de base** ?",
        "answers": ["4"]
    }
)

const WELL_FRUITS = [
    {
        "fruit": "Cerise",
        "firstLevel": 2
    },
    {
        "fruit": "Orange",
        "firstLevel": 4
    },
    {
        "fruit": "Pomme",
        "firstLevel": 7
    },
    {
        "fruit": "Banane",
        "firstLevel": 15
    },
    {
        "fruit": "Citron Sorbex",
        "firstLevel": 30
    },
    {
        "fruit": "Bombinos",
        "firstLevel": 40
    },
    {
        "fruit": "Poire Melbombe",
        "firstLevel": 50
    },
    {
        "fruit": "Abricot",
        "firstLevel": 56
    },
    {
        "fruit": "Litchi",
        "firstLevel": 58
    },
    {
        "fruit": "Fraise Tagada",
        "firstLevel": 60
    },
    {
        "fruit": "Sapeur Kiwi",
        "firstLevel": 70
    },
    {
        "fruit": "Bondissante",
        "firstLevel": 80
    },
    {
        "fruit": "Ananargeddon",
        "firstLevel": 90
    }
];

generalCulture.push(
    ...WELL_FRUITS.map(fruit => {
        return {
            "question": `A quel **niveau du puits** le fruit \`${fruit.fruit}\` fait son apparition pour la **première fois** ?`,
            "answers": ["" + fruit.firstLevel]
        }
    })
)

generalCulture.push(
    {
        "question": "A quel **niveau** se trouve la **Porte des Enfers** ?",
        "answers": ["54"]
    },
    {
        "question": "Quel **niveau** du jeu de base contient **8 oranges** ?",
        "answers": ["64"]
    },
    {
        "question": "Dans quel **niveau** se trouve l'entrée de la première **dimension parallèle** accessible dans le jeu de base pour un **nouveau joueur** ?",
        "answers": ["3"]
    },
    {
        "question": "Quel **mot** peut-on **lire** avec les plateformes du **niveau 55** ?",
        "answers": ["ice"]
    },
    {
        "question": "Que peut-on **lire** avec les plateformes du **niveau 60** ?",
        "answers": ["ball ball", "ballball"]
    },
    {
        "question": "Dans quel **niveau** l'entrée du **niveau HK** a-t-elle été cachée dans **Hammerfest Deluxe Edition** ?",
        "answers": ["39"]
    },
    {
        "question": "Quel est le **seul niveau** du puits où les objets **apparaissent** bien qu'ils n'y ait **pas d'ennemis** ?",
        "answers": ["59"]
    },
    {
        "question": "Quel **niveau** du puits contient une **sortie alternative** permettant de **passer le niveau suivant** ?",
        "answers": ["81"]
    },
    {
        "question": "Quel est le **niveau maximum** atteignable dans la **première partie de l'apprentissage** ?",
        "answers": ["22"]
    },
    {
        "question": "Quel **lettre** peut-on **lire** sur les plateformes du niveau **26.3** ?",
        "answers": ["k"]
    },
    {
        "question": "Que peut-on **lire** sur les plateformes du niveau **43.3** ?",
        "answers": ["pop!!"]
    },
    {
        "question": "Que peut-on **lire** sur les plateformes du niveau **43.5** ?",
        "answers": ["up"]
    },
    {
        "question": "Que peut-on **lire** sur les plateformes du niveau **43.7** ?",
        "answers": ["bub' bob", "bub bob", "bub'bob", "bubbob"]
    },
    {
        "question": "Que peut-on **lire** sur les plateformes du niveau **43.8** ?",
        "answers": ["extra"]
    },
    {
        "question": "A quel **niveau** se situe la **tanière des Framboijules** ?",
        "answers": ["54.12.0"]
    },
    {
        "question": "A quel **niveau** se trouve le **Pass Pyramide** ?",
        "answers": ["54.12.1D.9.3.16"]
    },
    {
        "question": "A quel **niveau** se trouve la **Neige-ô-glycérine** ?",
        "answers": ["54.12.1G.18.4"]
    }
);

generalCulture.push(
    {
        "question": "Avec quel **fruit** la pomme est-elle souvent **confondue** ?",
        "answers": ["tomate", "tomato"]
    },
    {
        "question": "A quel niveau du **puits** peut-on faire un **double-ordre** ?",
        "answers": ["47"]
    },
    {
        "question": "Quel niveau du **puits** contient un bloc 1x1 **invisible** ?",
        "answers": ["1"]
    },
    {
        "question": "A **quel niveau** est-on propulsé en **cassant le seau** du niveau 0 ?",
        "answers": ["10"]
    },
    {
        "question": "Quel est le nom de la **pastèque** dans le **code du jeu** ?",
        "answers": ["baleine", "whale"]
    },
    {
        "question": "A partir de quel niveau du **puits** l'**obscurité** commence à apparaître ?",
        "answers": ["16"]
    }
);

const raw: Array<{name: string, value: {small?: number, big?: number}}> = [
    {
        "name": "cristal bleu",
        "value": {
            "small": 500
        }
    },
    {
        "name": "cristal vert",
        "value": {
            "small": 2000,
            "big": 10000
        }
    },
    {
        "name": "cristal rouge",
        "value": {
            "small": 4500,
            "big": 22500
        }
    },
    {
        "name": "cristal jaune",
        "value": {
            "small": 8000,
            "big": 40000
        }
    },
    {
        "name": "cristal violet",
        "value": {
            "small": 12500,
            "big": 62500
        }
    },
    {
        "name": "cristal orange",
        "value": {
            "small": 18000,
            "big": 90000
        }
    },
    {
        "name": "cristal noir",
        "value": {
            "small": 24500,
            "big": 122500
        }
    },
    {
        "name": "cristal noir (2ème)",
        "value": {
            "small": 32000
        }
    },
    {
        "name": "cristal noir (3ème)",
        "value": {
            "small": 40500
        }
    },
    {
        "name": "cristal noir (4ème)",
        "value": {
            "small": 50000
        }
    },
    {
        "name": "oeil de tigre",
        "value": {
            "big": 160000
        }
    },
    {
        "name": "jade de 12kg",
        "value": {
            "big": 202500
        }
    },
    {
        "name": "reflet-de-lune",
        "value": {
            "big": 250000
        }
    },
]

raw.forEach(elem => {
    if (elem.value.small) {
        let qu = `Quelle est la **valeur** d'un ${elem.name} ?`;
        generalCulture.push({
            "question": qu,
            "answers": [elem.value.small.toString()]
        })
    }

    if (elem.value.big) {
        let qu = `Quelle est la **valeur** d'un ${elem.name} géant ?`;
        generalCulture.push({
            "question": qu,
            "answers": [elem.value.big.toString()]
        })
    }
})

const uhcQuestions: GeneralCultureQuestion[] = [
    {
        "question": "Qui a été le **premier joueur** à **terminer le jeu** avec **0 point** ?",
        "answers": Xirty
    },
    {
        "question": "Quel **site d'aide** le joueur **steph0808** a-t-il créé en **2006** ?",
        "answers": ["Tout sur Hammerfest"]
    },
    {
        "question": "**Qui** est à l'origine du **topic \"Exploration des Enfers\"** sur le **forum** ?",
        "answers": Deepnight
    },
    {
        "question": "Quel **joueur** a chanté la chanson **Coeff 6** ?",
        "answers": Thepoule
    },
    {
        "question": "**Combien** de **cristaux** le **coffre d'Anarchipel** fait-il apparaître ?",
        "answers": ["5"]
    },
    {
        "question": "Quel **joueur** a créé le **topic \"Réactions du jour\"** sur le **forum** ?",
        "answers": Rollerfou
    }
]
generalCulture.push(...uhcQuestions)



async function ask(): Promise<Question> {
    
    const index = Math.floor(Math.random() * generalCulture.length)
    const chosen = generalCulture[index]

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": `*Culture Hammerfestiane #${index + 1}*\n\n${chosen.question}\n`
            }
        ])

    return {
        "question": embed,
        "answers": {
            "anwsers": chosen.answers,
            "multiplier": 1
        }
    }
}

export default {
    ask
}