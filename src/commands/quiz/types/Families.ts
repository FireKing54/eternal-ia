import ArrayUtil from "blessed-api/dist/utils/ArrayUtils";
import { Question } from "../questions/Question";
import { getQuizQuestionEmbed } from "../QuizStart";
import { FamiliesService } from "../../../commands/families/families.service";
import H4AFamilies from "../../../core/api/hamm4all/families-api";
import H4AFiles from "../../../core/api/hamm4all/files-api";
import H4AItems from "../../../core/api/hamm4all/items-api";

const objectSize = 50;

function calculateMultiplier(items: Array<any>) {
    return (items.length - 1) * 0.1
}

async function askMissingItemName(): Promise<Question> {
    const families = await H4AFamilies.getFamiliesByCondition(fam => fam.items.length > 1)
    const family = ArrayUtil.randomElement(families);

    const randomItem = ArrayUtil.randomElement(family.items);
    randomItem.img = undefined!;
    const image = await FamiliesService.buildFamilyImage(family)

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **nom** de l'objet manquant pour terminer la **famille** ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image}],
        "answers": {
            "anwsers": Object.values(randomItem.name),
            "multiplier": 1 + calculateMultiplier(family.items)
        }
    }
}

async function askFromObjects(): Promise<Question> {
    const families = await H4AFamilies.getFamiliesByCondition(fam => fam.items.length > 1)
    let family = ArrayUtil.randomElement(families);

    // Get the image of all the items
    const image = await FamiliesService.buildFamilyImage(family)

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **nom** de cette **famille** ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image}],
        "answers": {
            "anwsers": Object.values(family.name),
            "multiplier": 1
        }
    }
}

async function askFamilyByObjectName() {
    const chosenFamily = await FamiliesService.getRandomFamily();
    const randomOldItem = ArrayUtil.randomElement(chosenFamily.items, (item) => item.img !== undefined);
    const questItem = await H4AItems.getItemByGameIDAndInGameID(31, randomOldItem.id);
    if (! questItem) {
        throw new Error("Item not found");
    }

    const image = await H4AFiles.getResizedFile(questItem.picture_id!, objectSize, objectSize)

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "A quelle **famille** cet objet appartient-il ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image || Buffer.from("")}],
        "answers": {
            "anwsers": Object.values(chosenFamily.name),
            "multiplier": 1
        }
    }
}

export default {
    askFamilyByObjectName,
    askMissingItemName,
    askFromObjects
}