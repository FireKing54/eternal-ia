import ArrayUtil from "blessed-api/dist/utils/ArrayUtils";
import { getQuizQuestionEmbed } from "../QuizStart";
import { Question } from "../questions/Question";
import { QuestService } from "../../quests/quests.service";
import H4AQuests from "../../../core/api/hamm4all/quests-api";

async function askNameFromItems(): Promise<Question> {
    const quest = await QuestService.getRandomQuest();
    const image = await QuestService.buildQuestImage(quest);

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **nom** de cette **quête** ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image}],
        "answers": {
            "anwsers": Object.values(quest.name),
            "multiplier": 1
        }
    }
}

function calculateMultiplier(items: Array<any>) {
    return (items.length - 1) * 0.1
}

async function askAmountFromObject(): Promise<Question> {
    const quests = await H4AQuests.getQuestsByCondition(qu => qu.items.length > 1);
    const quest = ArrayUtil.randomElement(quests)

    const randomItem = ArrayUtil.randomElement(quest.items);
    const quantity = randomItem.quantity;
    randomItem.quantity = NaN; // May cause some issues

    const image = await QuestService.buildQuestImage(quest)

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quelle est la **quantité** manquante pour compléter la **quête** ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image}],
        "answers": {
            "anwsers": [quantity.toString()],
            "multiplier": 1
        }
    }
}


async function askItemName(): Promise<Question> {
    const quests = await H4AQuests.getQuestsByCondition(qu => qu.items.length > 1);
    const quest = ArrayUtil.randomElement(quests)

    const randomItem = ArrayUtil.randomElement(quest.items);
    randomItem.img = undefined!;

    const image = await QuestService.buildQuestImage(quest);

    const embed = getQuizQuestionEmbed()
        .addFields([
            {
                "name": "Question",
                "value": "Quel est le **nom** de l'objet manquant pour terminer la **quête** ?"
            }
        ])
        .setImage('attachment://image.png');

    return {
        "question": embed,
        "attachedFiles": [{name: "image.png", attachment: image}],
        "answers": {
            "anwsers": Object.values(randomItem.name),
            "multiplier": 1 + calculateMultiplier(quest.items)
        }
    }
}

export default {
    askNameFromItems,
    askItemName,
    askAmountFromObject
}