import { EmbedBuilder } from "discord.js";
import Objects from "../types/Objects";
import Families from "../types/Families";
import GeneralCulture from "../types/general-culture/GeneralCulture";
import * as Levels from "../types/Levels";
import Quests from "../types/Quests";

interface QuestionType {
    perform: () => Promise<Question>,
    weight: number,
    pointsMultiplier: number
}

interface Question {
    question: EmbedBuilder;
    attachedFiles?: Array<{
        "name": string,
        "attachment": Buffer
    }>;
    answers: QuestionAnswer;
}

interface QuestionAnswer {
    anwsers: Array<string>,
    multiplier: number
}

type Rarities = "common" | "rare" | "epic" | "legendary" | "mythic";
type RarityNames = "commune" | "rare" | "épique" | "légendaire" | "mythique";

const QUESTIONS: Array<QuestionType> = [
    {
        perform: Objects.askName,
        weight: 1200, // Tested
        pointsMultiplier: 1.2
    },
    {
        perform: Objects.askCoeff,
        weight: 500, // Tested
        pointsMultiplier: 0.6
    },
    {
        perform: Families.askFamilyByObjectName,
        weight: 200, // Tested
        pointsMultiplier: 3
    },
    {
        perform: Objects.askPoints,
        weight: 700, // Tested
        pointsMultiplier: 1.2
    },
    {
        perform: GeneralCulture.ask,
        weight: 1000, // Tested
        pointsMultiplier: 1.6
    },
    {
        perform: Quests.askNameFromItems,
        weight: 200, // Tested
        pointsMultiplier: 1.8
    },
    {
        perform: Quests.askItemName,
        weight: 200, // Tested
        pointsMultiplier: 2
    },
    {
        perform: Quests.askAmountFromObject,
        weight: 200, // Tested
        pointsMultiplier: 1
    },
    {
        perform: Families.askMissingItemName,
        weight: 200, // Tested
        pointsMultiplier: 1.8
    },
    {
        perform: Families.askFromObjects,
        weight: 200, // Tested
        pointsMultiplier: 2.4
    },
    {
        perform: Levels.askGameName,
        weight: 500, // Tested
        pointsMultiplier: 0.8
    },
    {
        perform: Levels.askLevelName,
        weight: 800, // Tested
        pointsMultiplier: 2
    },
    {
        perform: Levels.askGameNameFromLevel,
        weight: 1200,
        pointsMultiplier: 1.3
    }
]

type QuestionRarityType = {
    color: [number, number, number],
    rarity: Rarities,
    name: RarityNames,
    pointsCoeff: number,
    weight: number
}

const QUESTIONS_RARITY: Array<QuestionRarityType> = [
    {
        color: [30, 255, 0],
        rarity: "common",
        name: "commune",
        pointsCoeff: 1,
        weight: 2000
    },
    {
        color: [0, 112, 221],
        rarity: "rare",
        name: "rare",
        pointsCoeff: 2,
        weight: 300
    },
    {
        color: [153, 63, 238],
        name: "épique",
        rarity: "epic",
        pointsCoeff: 4,
        weight: 50
    },
    {
        color: [255, 128, 0],
        name: "légendaire",
        rarity: "legendary",
        pointsCoeff: 8,
        weight: 8
    },
    {
        color: [220, 30, 30],
        name: "mythique",
        rarity: "mythic",
        pointsCoeff: 16,
        weight: 1
    },
];

export {
    QUESTIONS,
    QUESTIONS_RARITY
}
export type {
    Question,
    QuestionAnswer,
    QuestionType,
    Rarities,
    RarityNames,
    QuestionRarityType
}
