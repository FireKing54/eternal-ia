import { ApplicationCommandOptionType, CommandInteraction, EmbedBuilder, GuildMember } from "discord.js";

import ChartJS, { BarController, BarElement, CategoryScale, Chart, Filler, LinearScale, LineController, LineElement, PointElement } from "chart.js"
import connection from "../../database/Connection";
import { Discord, Slash, SlashGroup, SlashOption } from "discordx";
import { QUESTIONS_RARITY } from "./questions/Question";
import BlessedAPI from "blessed-api";
import { Canvas } from "canvas";


type Rarity = "common" | "rare" | "epic" | "legendary" | "mythic"

interface Rarities {
    "common": UnderRarity
    "rare": UnderRarity,
    "epic": UnderRarity,
    "legendary": UnderRarity,
    "mythic": UnderRarity
}

interface UnderRarity {
    name: string,
    color: string,
    points: Array<number>
}

// Register category scales
Chart.register(CategoryScale);
Chart.register(LinearScale);
Chart.register(BarController);
Chart.register(BarElement);
Chart.register(LineController);
Chart.register(PointElement);
Chart.register(LineElement);
Chart.register(Filler)

@Discord()
@SlashGroup("quiz")
export class QuizStats {

    @Slash({
        "name": "stats",
        "description": "Donne des statistiques sur l'EternalQuiz pour le joueur concerné",
    })
    async getStats(
        @SlashOption({
            "name": "days",
            "description": "Jours affichés sur le graphique",
            "required": false,
            "type": ApplicationCommandOptionType.Integer,
            "minValue": 1,
            "maxValue": 1_000
        })
        days: number | undefined,
        @SlashOption({
            "name": "user",
            "description": "Utilisateur sur lequel appliquer la commande",
            "type": ApplicationCommandOptionType.User,
            "required": false
        })
        user: GuildMember | undefined,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        days ??= 14;
        user ??= interaction.member as GuildMember;

        const image = await getHistoryDrawing(user.id, days)

        const res = new EmbedBuilder()
            .setTitle("Statistiques")
            .setAuthor({
                "name": user.displayName,
                "iconURL": user.user.avatarURL() ?? undefined
            })
            .setTimestamp()
            .setThumbnail(user.avatarURL() as string)
            .setImage('attachment://image.png');

        interaction.editReply({
            "embeds": [res],
            "files": [{ name: "image.png", attachment: image }]
        })
    }

}


async function getHistoryDrawing(id: string, days: number) {
    const history = await getHistory(id, days);
    
    const quizStatsCanvas = new Canvas(400, 300);
    new Chart(quizStatsCanvas as any, {
        "type": "bar",
        data: history,
        options: {
            plugins: {
                legend: {
                    labels: {
                        color: "#DDDDDD"
                    }
                }
            },
            animation: false,
            responsive: true,
            scales: {
                x: {
                    stacked: true,
                    "ticks": {
                        "color": "#DDDDDD"
                    }
                },
                y: {
                    stacked: true,
                    min: 0,
                    "ticks": {
                        "color": "#DDDDDD"
                    }
                }
            }
        }
    })

    return quizStatsCanvas.toBuffer()
}

async function getHistory(id: string, nbDays: number = 30): Promise<ChartJS.ChartData<keyof ChartJS.ChartTypeRegistry, any[], any>> {
    const date = new Date();
    date.setMonth(date.getMonth() - 1);
    const data = await connection.query<
        Array<{day: Date, rarity: Rarity, sum: number}>
    >(`SELECT *
    FROM  (
       SELECT DISTINCT date_trunc('day', (current_date - offs)) as day 
       FROM generate_series(0,365,1) as offs
       where date_trunc('day', (current_date - offs)) > date '2021-05-20'
       ) d
    LEFT   JOIN (
       SELECT created_at::date as day, rarity, sum(reward)
       FROM   quiz
       WHERE    rarity != 'none'
       AND winner = $1::text
       GROUP  BY day, rarity
       ) t USING (day)
    ORDER  BY day`, [id]
    );

    let labels = [...(new Set(data.map(d => d.day.toLocaleDateString())))];

    let rarities: Rarities = Object.fromEntries(QUESTIONS_RARITY.map(qu => {
        const rar: UnderRarity = {
            name: BlessedAPI.Utils.String.capitalize(qu.name),
            color: `rgb(${qu.color.join(",")})`,
            points: []
        }
        return [
            qu.rarity as Rarity,
            rar
        ]
    })) as any;

    data.forEach(d => {
        const index = labels.indexOf(d.day.toLocaleDateString());
        Object.values(rarities).forEach((elem) => {
            if (! elem.points[index]) elem.points.push(0);
        })
        if (! d.rarity) return;
        const rar: Rarity = d.rarity
        rarities[rar].points[index] = d.sum;
    })

    let average: Array<number | null> = [];

    const days = 7;
    const res = labels.reduce((acc: Array<number>, elem, index) => {
        acc.push(Object.values(rarities).reduce((accu, element) => accu + element.points[index], 0))
        if (index >= days - 1) {
            if (index > days - 1) acc.shift();
            average.push(acc.reduce((accu, element) => accu + element, 0) / days)
        }
        else {
            average.push(null)
        }

        return acc;
    }, []);
    (() => res)()

    const originalLength = average.length;
    average = average.slice(average.length - nbDays);
    labels = labels.slice(labels.length - nbDays);

    const questionsColor = "rgb(40, 120, 128)";
    const questionsColorBackground = "rgba(40, 120, 128, 0.3)"
    const rarityDataset = Object.values(rarities).map(rar => {
        return {
            label: rar.name,
            backgroundColor: rar.color,
            borderColor: rar.color,
            data: rar.points.slice(originalLength - nbDays),
            order: 1
        }
    })

    return {
        labels,
        datasets: [
            ...rarityDataset,
            {
                label: "Moyenne sur 7 jours",
                backgroundColor: questionsColorBackground,
                borderColor: questionsColor,
                data: average,
                type: "line",
                fill: true,
                order: 0
            }
        ]
    };
}
