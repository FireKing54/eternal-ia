import { ApplicationCommandOptionType, CommandInteraction, EmbedBuilder, GuildMember } from "discord.js";
import puppeteer from "puppeteer";

import pug from "pug"
import connection from "../../database/Connection";
import { Discord, Guild, Slash, SlashGroup, SlashOption } from "discordx";
import { ENV } from "../../Environment";
import { QUESTIONS_RARITY } from "./questions/Question";
import BlessedAPI from "blessed-api";

interface QuizTitle {
    "name": string,
    "minimum": number
}

type QuestionsAnswered = Array<{
    rarity: string,
    sum: number
}>

type ByDate = Array<{
    sum: number,
    amount: number,
    date_trunc: Date
}>

const capitalize = BlessedAPI.Utils.String.capitalize;

@Discord()
@SlashGroup("quiz")
@Guild(ENV.MAIN_GUILD)
export class QuizRecapCommand {

    @Slash({
        "name": "recap",
        "description": "Donne un récapitulatif sur l'EternalQuiz pour le joueur concerné",
    })
    async recap(
        @SlashOption({
            "name": "user",
            "description": "Utilisateur sur lequel appliquer la commande",
            "type": ApplicationCommandOptionType.User,
            "required": false
        })
        user: GuildMember | undefined,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply()

        const realUser = user ?? interaction.member as GuildMember;

        const users_info = await connection.query("SELECT winner, SUM(reward) as points, rank () over (order by SUM(reward) desc) FROM Quiz WHERE winner IS NOT NULL group by winner");

        const user_info = users_info.find((info: any) => info.winner === realUser.id)

        let {points, rank} = user_info || {points: 0, rank: "?"};
        points = BlessedAPI.Utils.Number.roundDecimal(points, 2);

        const {title, nextTitle} = getTitle(points)

        const questionsAnswered: QuestionsAnswered = await connection.query("SELECT rarity, COUNT(reward) as sum FROM Quiz WHERE winner = $1::text GROUP BY rarity ORDER BY sum DESC", [realUser.id])

        const fields = QUESTIONS_RARITY.map(qu => {
            return {

                "name": capitalize(qu.name),
                "color": `rgb(${qu.color.join(",")})`,
                "value": (questionsAnswered.find(resp => resp.rarity === qu.rarity) || {sum: 0}).sum,
                "inline": true
            }
        })

        let byDate: ByDate = await connection.query(`SELECT
                SUM(reward)::numeric(10,2) as sum,
                COUNT(reward) as amount,
                date_trunc('day', created_at)
            FROM Quiz
            WHERE winner = $1::text
                AND rarity != 'none'
                AND created_at > now() - interval '2 days'
            GROUP BY date_trunc('day', created_at)
            ORDER BY date_trunc('day', created_at) DESC`,
            [realUser.id]
        )

        const today = new Date();
        const yesterday = new Date();
        yesterday.setDate(today.getDate() - 1)
        const dates = {
            "today": {
                ...byDate.find(elem => elem.date_trunc.getDate() === today.getDate()),
                "name": capitalize("Aujourd'hui")
            },
            "yesterday": {
                ...byDate.find(elem => elem.date_trunc.getDate() === yesterday.getDate()),
                "name": capitalize("Hier")
            }
        }

        const percentageToNextStep = BlessedAPI.Utils.Number.floorDecimal(((points - title.minimum) / (nextTitle.minimum - title.minimum)) * 100, 2)

        const browser = await puppeteer.launch({
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        });
        const page = await browser.newPage();

        const htmlContent = `
            <html>
                <head>
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@1.0.2/css/bulma.min.css">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.6.0/css/all.min.css">
                </head>
                <body>
                    ${PAGE_RENDERER({
                        rarities: fields,
                        title,
                        nextTitle,
                        currentPoints: points.toLocaleString(),
                        progress: {
                            "value": points - title.minimum,
                            "max": nextTitle.minimum - title.minimum,
                            "percentage": percentageToNextStep
                        },
                        rank,
                        dates,
                        translations: {
                            "points": "points",
                            "questions": "questions"
                        }
                    })}
                </body>
            </html>
        `

        await page.setContent(htmlContent, { waitUntil: "networkidle0" });
        await page.setViewport({ width: 500, height: 500 });
        const image = await page.screenshot({
            type: "png",
            fullPage: true
        });
        await browser.close();

        const res = new EmbedBuilder()
            .setTitle(`Eternal Quiz`)
            .setAuthor({
                "name": realUser.nickname ?? realUser.displayName,
                "iconURL": realUser.avatarURL() ?? realUser.user.avatarURL() ?? undefined
            })
            .setTimestamp()
            .setImage('attachment://image.png');

        await interaction.editReply({
            embeds: [res],
            "files": [{name: "image.png", attachment: Buffer.from(image)}]
        })
    }
}

const PAGE_RENDERER = pug.compileFile("./resources/html/quiz.pug")

function getTitle(points: number): {title: QuizTitle, nextTitle: QuizTitle} {
    let [title, nextTitle] = titles;
    for(let i = 1 ; i < titles.length ; i++) {
        if (titles[i].minimum > points) {
            title = titles[i - 1];
            nextTitle = titles[i];
            break;
        }
    }
    return {title, nextTitle};
}

const titles = [
    {
        "name": "Débutant",
        "minimum": 0
    },
    {
        "name": "Amateur",
        "minimum": 100
    },
    {
        "name": "Culturé",
        "minimum": 250
    },
    {
        "name": "Connaisseur I",
        "minimum": 500
    },
    {
        "name": "Connaisseur II",
        "minimum": 750
    },
    {
        "name": "Connaisseur III",
        "minimum": 1_200
    },
    {
        "name": "Livre Ouvert I",
        "minimum": 1_800
    },
    {
        "name": "Livre Ouvert II",
        "minimum": 2_500
    },
    {
        "name": "Livre Ouvert III",
        "minimum": 3_500
    },
    {
        "name": "Fou du Quiz I",
        "minimum": 5_000
    },
    {
        "name": "Fou du Quiz II",
        "minimum": 7_000
    },
    {
        "name": "Fou du Quiz III",
        "minimum": 10_000
    },
    {
        "name": "Omniscient I",
        "minimum": 15_000
    },
    {
        "name": "Omniscient II",
        "minimum": 22_000
    },
    {
        "name": "Omniscient III",
        "minimum": 35_000
    },
    {
        "name": "Savant Fou I",
        "minimum": 50_000
    },
    {
        "name": "Savant Fou II",
        "minimum": 75_000
    },
    {
        "name": "Instable Insatiable I",
        "minimum": 100_000
    },
    {
        "name": "Instable Insatiable II",
        "minimum": 130_000
    },
    {
        "name": "Instable Insatiable III",
        "minimum": 160_000
    },
    {
        "name": "Malade Mental I",
        "minimum": 200_000
    },
    {
        "name": "Malade Mental II",
        "minimum": 250_000
    },
    {
        "name": "Détraqué Psychologique I",
        "minimum": 320_000
    },
    {
        "name": "Détraqué Psychologique II",
        "minimum": 400_000
    },
    {
        "name": "Dieu du Savoir I",
        "minimum": 500_000
    },
    {
        "name": "Dieu du Savoir II",
        "minimum": 600_000
    },
    {
        "name": "Dieu du Savoir III",
        "minimum": 750_000
    },
    {
        "name": "Ultime Savant",
        "minimum": 1_000_000
    },
    {
        "name": "Ultime Savant (Prestige I)",
        "minimum": 1_050_000
    },
    {
        "name": "Ultime Savant (Prestige II)",
        "minimum": 1_100_000
    },
    {
        "name": "Ultime Savant (Prestige III)",
        "minimum": 1_150_000
    },
    {
        "name": "Ultime Savant (Prestige IV)",
        "minimum": 1_200_000
    },
    {
        "name": "Ultime Savant (Prestige V)",
        "minimum": 1_250_000
    },
    {
        "name": "Ultime Savant (Prestige VI)",
        "minimum": 1_300_000
    },
    {
        "name": "Ultime Savant (Prestige VII)",
        "minimum": 1_350_000
    },
    {
        "name": "Ultime Savant (Prestige VIII)",
        "minimum": 1_400_000
    },
    {
        "name": "Ultime Savant (Prestige IX)",
        "minimum": 1_450_000
    },
    {
        "name": "Ultime Savant (Prestige X)",
        "minimum": 1_500_000
    },
]

export {
    getTitle
}
