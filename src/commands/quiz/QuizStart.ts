import BlessedAPI from "blessed-api";
import { Quiz } from "../../database/models/Quiz";
import { ActionRowBuilder, ButtonBuilder, ButtonInteraction, ButtonStyle, channelMention, ChannelType, Client, CommandInteraction, EmbedBuilder, Message } from "discord.js";
import { ArgsOf, ButtonComponent, Discord, Guild, On, Slash, SlashGroup } from "discordx";
import { EmbedService } from "../../embeds/embed.service";
import { ENV } from "../../Environment";
import BotError from "../../errors/BotError";
import { logger } from "../../Log";
import { exactMatch } from "../../match/matchers";
import { Question, QuestionRarityType, QUESTIONS, QUESTIONS_RARITY, QuestionType } from "./questions/Question";
import { getTitle } from "./QuizRecap";

const BASE_POINTS = 5;
let questionPoints = BASE_POINTS;
const MINIMUM_POINTS = BASE_POINTS * 0.20 // 20%

const DECREASE_POINTS_RATE = 0.970; // 3%

const WAIT_BEFORE_ASK = 5_000 // 5s
const TIME_BEFORE_DECREASING_POINTS = 5_000; // 5s
const RANDOM_COEFF = 0.1 // + or - 10 percents

const TIME_BEFORE_RESET = 1000 * 60 * 10 // 10 minutes

const HAPPY_HOUR_COEFF = 1.5;

const RESTART_ICON = "🔄";
const RESTART_ACTION_ROW = new ActionRowBuilder<ButtonBuilder>()
    .addComponents(
        new ButtonBuilder()
            .setCustomId("quiz-restart")
            .setEmoji(RESTART_ICON)
            .setStyle(ButtonStyle.Success)
            )

const SUCCESS_REPONSE = EmbedService.buildSuccess({
    "title": "Bonne réponse !",
})

interface QuizQuestion {
    question: Question;
    message?: Message;
    askedAt?: Date
    askTimer?: NodeJS.Timeout;
    rarity: QuestionRarityType;
}

@Discord()
@SlashGroup({
    "name": "quiz",
    "description": "Permet d'interagir avec l'Eternal Quiz",
})
@SlashGroup("quiz")
@Guild(ENV.MAIN_GUILD)
export class QuizStart {

    private currentQuiz: QuizQuestion | null = null;
    private static happyHour = false;

    @Slash({
        "name": "start",
        "description": "Démarre une partie d'Eternal Quiz",
        "dmPermission": false,
    })
    async start(
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        if (interaction.channelId !== ENV.QUIZ_CHANNEL) {
            throw new BotError(`Tu ne peux pas lancer un quiz ici. tu dois être dans le salon ${channelMention(ENV.QUIZ_CHANNEL)}.`)
        }

        const quizQuestion = await this.startQuiz();

        await interaction.editReply({
            "embeds": [quizQuestion],
        }).then(message => this.scheduleQuestionAsk(interaction, message));
    }

    static startHappyHour(client: Client) {
        QuizStart.happyHour = true;

        const channel = client.guilds.cache.get(ENV.MAIN_GUILD)?.channels.cache.get(ENV.QUIZ_CHANNEL)!
        if (channel.type !== ChannelType.GuildText) {
            throw new BotError("Le salon quiz n'est pas un salon texte")
        }

        channel.send({
            "embeds": [EmbedService.buildSuccess({
                "title": "Happy hour !",
                "description": `L'Eternal Quiz est en happy hour ! Les points gagnés seront multipliés par ${HAPPY_HOUR_COEFF} !`
            })]
        })

        setTimeout(() => {
            QuizStart.happyHour = false;

            channel.send({
                "embeds": [EmbedService.buildSuccess({
                    "title": "Happy hour terminée",
                    "description": `L'Eternal Quiz n'est plus en happy hour. Reviens demain pour une nouvelle chance !`
                })]
            })
        }, 1000 * 60 * 60)
    }

    async startQuiz(): Promise<EmbedBuilder> {
        // CHOOSE QUESTION AND DRAW

        if (this.currentQuiz !== null) {
            throw new BotError("Un quiz a déjà été lancé")
        }

        const chosenType = this.choose();
        logger.info(`Chosen type: ${chosenType.pointsMultiplier} - ${chosenType.weight}`)

        this.currentQuiz = {
            "question": await chosenType.perform(),
            "rarity": BlessedAPI.Utils.Array.pickFromWeights(QUESTIONS_RARITY, (elem) => elem.weight)
        };

        questionPoints = BASE_POINTS * chosenType.pointsMultiplier * this.currentQuiz.question.answers.multiplier;
        questionPoints *= this.currentQuiz.rarity.pointsCoeff;
        questionPoints += (Math.random() * 2 - 1) * RANDOM_COEFF * questionPoints
        if (QuizStart.happyHour) {
            questionPoints *= HAPPY_HOUR_COEFF;
        }

        const ortho = (WAIT_BEFORE_ASK / 1000 > 1) ? "secondes" : "seconde";

        // BUILD WAITING EMBED
        const embed = new EmbedBuilder()
            .setTitle("Eternal Quiz")
            .setDescription(`**Eternal Quiz** est lancé ! Dans **${WAIT_BEFORE_ASK / 1000} ${ortho}**, une question sera posée. Tu devras y répondre en écrivant simplement la réponse. Bonne chance !`)

        return embed;
    }

    scheduleQuestionAsk(interaction: CommandInteraction | ButtonInteraction, message: Message) {
        if (this.currentQuiz === null) {
            throw new BotError("Il n'y a pas de Quiz lancé actuellement")
        }

        this.currentQuiz.askTimer = setTimeout(() => {

            if (this.currentQuiz === null) {
                throw new BotError("Il n'y a pas de Quiz lancé actuellement")
            }

            this.currentQuiz.question.question
                .setColor(this.currentQuiz.rarity.color)
                .setDescription(`${raritiesToDesc[this.currentQuiz.rarity.rarity]} Mutliplicateur ${this.currentQuiz.rarity.pointsCoeff}x`)
                .setFooter({
                    "text": `Points de départ : ${BlessedAPI.Utils.Number.roundDecimal(questionPoints, 2)} points`
                })
                .setTimestamp(new Date())

            this.askQuestion(interaction, message);

            // Give up button
            const giveUpButton = new ButtonBuilder()
                .setCustomId("quiz-reset")
                .setStyle(ButtonStyle.Danger)
                .setLabel("Abandonner")

            this.currentQuiz.askTimer = undefined;
            message.edit({
                "embeds": [this.currentQuiz.question.question],
                "files": this.currentQuiz.question.attachedFiles,
                "components": [new ActionRowBuilder<ButtonBuilder>().addComponents(giveUpButton)],
            }).then(() => {
                this.currentQuiz!.askedAt = new Date();
            });
        }, WAIT_BEFORE_ASK)
    }

    isCorrect(response: string): boolean {
        if (! this.currentQuiz) {
            logger.info(this.currentQuiz)
            throw new BotError("Il n'y a pas de Quiz lancé actuellement")
        }

        for(const answer of this.currentQuiz.question.answers.anwsers) {
            if (exactMatch(answer, response)) {
                return true
            }
        }

        return false;
    }

    reset() {
        if (! this.currentQuiz) {
            return;
        }

        if (this.currentQuiz.askTimer) {
            clearTimeout(this.currentQuiz.askTimer);
        }

        this.currentQuiz = null;
    }
    
    async saveAndReset() {
        await this.saveQuestion();
        this.reset();
    }
    
    async reward(message: Message, memberID: string, millisecondsToAnswer: number): Promise<void> {
        const power = Math.max(0, millisecondsToAnswer - TIME_BEFORE_DECREASING_POINTS) / 1000;
        const coefficient = DECREASE_POINTS_RATE ** power
        const points = BlessedAPI.Utils.Number.roundDecimal(questionPoints * coefficient, 2);
        const capped = Math.max(points, BlessedAPI.Utils.Number.roundDecimal(MINIMUM_POINTS, 2));

        if (! this.currentQuiz) {
            throw new BotError("Il n'y a pas de Quiz lancé actuellement. Cela ne devrait pas arriver")
        }

        const s = (BlessedAPI.Utils.Number.roundDecimal(capped, 2) > 1) ? "s" : ""
        const goodAnswerMessage = `Tu as répondu au bout de \`${BlessedAPI.Utils.Number.roundDecimal(millisecondsToAnswer / 1000, 1)}s\` et tu as remporté **${BlessedAPI.Utils.Number.roundDecimal(capped, 2)} point${s}** ! Tu peux relancer en appuyant sur le bouton.`
    
        const embed = SUCCESS_REPONSE
            .setDescription(goodAnswerMessage);

        message.reply({"embeds": [embed], "components": [RESTART_ACTION_ROW]})
    
        const {rarity} = this.currentQuiz;
        this.reset();
        const quiz = new Quiz()
        quiz.winner = memberID;
        quiz.rarity = rarity.rarity;
        quiz.reward = capped;
        await Quiz.save(quiz)
        .then(() => {
            logger.info(`User ${memberID} won ${capped} points with Eternal Quiz`);

            Quiz.sum("reward", {
                "winner": memberID
            })
            .then(newPoints => {
                newPoints ??= 0;
                const {title} = getTitle(newPoints);
    
                if (newPoints - capped < title.minimum) {
                    const res = `Bravo, tu as reçu un nouveau titre ! Tu es désormais \`${title.name}\``;
                    message.reply(res)
                }
            })
            .catch(err => {
                logger.error("Error while getting user points", err);
                this.reset();
            })
        })

    }

    choose(): QuestionType {
        const sumWeights = QUESTIONS.reduce((acc, elem) => acc + elem.weight, 0);
        const rand = Math.random() * sumWeights;
        let total = 0;
        for(const question of QUESTIONS) {
            total += question.weight;
            if (total >= rand) {
                return question
            }
        }
        return QUESTIONS[QUESTIONS.length - 1]
    }
    
    
    async saveQuestion() {
        const quiz = new Quiz();
        quiz.rarity = this.currentQuiz?.rarity.rarity!;
        quiz.reward = BlessedAPI.Utils.Number.roundDecimal(questionPoints, 2);
        await Quiz.save(quiz);
    }

    @ButtonComponent({ "id": "quiz-reset" })
    @Slash({
        "name": "reset",
        "description": "Réinitialise le quiz",
    })
    async resetQuestion(interaction: CommandInteraction | ButtonInteraction) {

        await interaction.deferReply()
        if (! this.currentQuiz) {
            throw new BotError("Aucun quiz n'est en cours")
        }

        if (! this.currentQuiz.askedAt) {
            throw new BotError("La question n'a pas encore été posée")
        }

        if (interaction instanceof ButtonInteraction) {
            await interaction.message.edit({
                "components": [],
            })
        }

        const responses = (this.currentQuiz.question.answers.anwsers.length > 1) ? "Les bonnes réponses étaient" : "La bonne réponse était"

        const embed = new EmbedBuilder()
            .setTitle("Le quiz a été réinitialisé")
            .setDescription(`Le quiz a été réinitialisé. ${responses} \`${this.currentQuiz.question.answers.anwsers.join("`, `")}\`.`)

        await this.saveAndReset();

        await interaction.editReply({
            embeds: [embed],
            components: [RESTART_ACTION_ROW]
        })
    }

    @ButtonComponent({ "id": "quiz-restart" })
    async restartQuestion(interaction: ButtonInteraction) {
        await interaction.deferReply()

        // Remove the restart button
        interaction.message.edit({
            "components": [],
        })

        if (this.currentQuiz) {
            throw new BotError("Un quiz est déjà lancé ailleurs")
        }

        const quizQuestion = await this.startQuiz();
        const message = await interaction.editReply({
            "embeds": [quizQuestion]
        }).then(reply => reply.fetch())
        
        this.scheduleQuestionAsk(interaction, message)
    }

    @On({ "event": "messageCreate" })
    async onMessage([message]: ArgsOf<"messageCreate">) {

        if (! message.author) { // If the message is a webhook
            return;
        }

        if (message.author.bot) {
            return;
        }

        if (message.channelId !== ENV.QUIZ_CHANNEL) {
            return;
        }

        if (! this.currentQuiz) {
            return;
        }

        if (! this.currentQuiz.message) {
            return;
        }

        if (! this.currentQuiz.askedAt) {
            return;
        }

        const content = (await message.fetch()).content;

        const response = content.replace(/[´’]/g , "'")

        if (this.isCorrect(response)) {
            const time = Date.now() - this.currentQuiz.askedAt.getTime()
            this.reward(message, message.author.id, time)
        }
    }

    askQuestion(interaction: CommandInteraction | ButtonInteraction, message: Message) {

        if (! this.currentQuiz) {
            throw new BotError("Aucun quiz n'est en cours")
        }

        this.currentQuiz.message = message;

        setTimeout(async () => {

            if (! this.currentQuiz) {
                return
            }

            if (this.currentQuiz.message === message) {
                let res = "Personne n'a répondu au quiz, il a été réinitialisé. "

                const reps = (this.currentQuiz.question.answers.anwsers.length > 1) ? "Les bonnes réponses étaient" : "La bonne réponse était";
                res += `${reps} \`${this.currentQuiz.question.answers.anwsers.join("`, `")}\`.`;
                await this.saveAndReset();
                interaction.followUp(res);
            }
        }, TIME_BEFORE_RESET)
    }
    
}

export function getQuizQuestionEmbed(): EmbedBuilder {
    return new EmbedBuilder()
        .setColor([30, 255, 0])
        .setTitle("Eternal Quiz")
}

const raritiesToDesc = {
    "common": "Question commune.",
    "rare": "__Question rare__.",
    "epic": "**__Question épique__** !",
    "legendary": "**__Question légendaire__** !!",
    "mythic": "**__Question mythique__** !!!"
}

