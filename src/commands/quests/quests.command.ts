import { ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction } from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import { QuestService } from "./quests.service";
import H4AQuests from "../../core/api/hamm4all/quests-api";


@Discord()
export class QuestCommand {

    @Slash({
        "name": "quest",
        "description": "Donne des informations sur la quête recherchée"
    })
    async questCommand(
        @SlashOption({
            "name": "name",
            "description": "Quête recherchée",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (inter: AutocompleteInteraction) => {
                const input = inter.options.getFocused();
                await inter.respond(await H4AQuests.autoCompleteQuests(input));  
            }
        })
        questName: string,
        interaction: CommandInteraction
    ) {
        // Remove the language prefix
        questName = questName.slice(5);

        const quest = await QuestService.getQuest(questName);

        const questImage = await QuestService.buildQuestImage(quest);
        const embed = await QuestService.buildDisplay(questName);

        interaction.reply({
            embeds: [embed],
            files: [{name: "image.png", attachment: questImage}]
        })
    }

}