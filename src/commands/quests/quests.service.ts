import { OldQuestItem, Quest } from "@hamm4all/shared";
import { ItemsService } from "../../commands/items/items.service";
import H4AFiles from "../../core/api/hamm4all/files-api";
import H4AItems from "../../core/api/hamm4all/items-api";
import H4AQuests from "../../core/api/hamm4all/quests-api";
import { EmbedBuilder } from "discord.js";

export class QuestService {

    public static async getQuest(name: string) {
        const quest = await H4AQuests.getQuestByName(name);
        if (! quest) {
            throw new Error("Quest not found");
        }

        return quest;
    }

    public static async buildDisplay(askedName: string) {
        return new EmbedBuilder()
            .setTitle(askedName)
            .setColor([250, 0, 250])
            .setImage('attachment://image.png')
    }

    public static async getRandomQuest() {
        const quests = await H4AQuests.getQuests();
        const randomQuest = quests[Math.floor(Math.random() * quests.length)];
        return randomQuest;
    }

    public static async buildQuestImage(quest: Quest) {
        const enhancedItems: Array<OldQuestItem & {image?: Buffer}> = []

        for(const item of quest.items) {
            const questItem = await H4AItems.getItemByGameIDAndInGameID(31, item.id);
            if (! questItem) {
                throw new Error("Item not found");
            }

            if (!item.img || !questItem.picture_id) {
                enhancedItems.push(item);
                continue;
            }

            const itemImage = await H4AFiles.getResizedFile(questItem.picture_id, 50, 50);

            enhancedItems.push({
                ...item,
                "image": itemImage
            })
        }

        const itemsAndAmount = enhancedItems.map((item) => {
            return {
                "img": item.image,
                "amount": item.quantity
            }
        })

        // Build the image
        return ItemsService.getGroupItemsImage(itemsAndAmount, true);
    }
}