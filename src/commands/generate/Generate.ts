import BlessedAPI from "blessed-api";
import { Discord, Slash, SlashGroup } from "discordx";
import { adjectives, nouns } from "./game-names/game-names.bank";
import { CommandInteraction } from "discord.js";
import { Noun, articles, prepositions } from "./game-names/game-names.types";

// TODO
// WIP

@Discord()
@SlashGroup({
    name: "generate",
    description: "Génère aléatoirement quelque chose"
})
@SlashGroup("generate")
export class Generate {


    @Slash({
        name: "game-name",
        description: "Génère un nom de jeu",
        dmPermission: true
    })
    async generateGameName(interaction: CommandInteraction) {

        const baseNoun = this.getRandomBaseNoun();

        const gameName = new GameNameBuilder(baseNoun)
            .checkPluralPresence()
            .checkAdjectivePresence()
            .checkArticlePresence()
            .checknominalGroupPresence()
            .build();

        return interaction.reply({
            content: `**__${gameName}__**`
        })
    }

    getRandomBaseNoun() {
        return BlessedAPI.Utils.Array.randomElement(nouns)
    }

}

class GameNameBuilder {
    
    private adjectivePresent: boolean = false;
    private adjectiveChance: number = 0.85;

    private articleChance: number = 0.75;
    private articlePresent = false;

    private baseNounPluralChance: number = 0.4;
    private baseNounPlural: boolean = false;

    private nominalGroupChance: number = 0.5;
    private nominalGroupPresent: boolean = false;

    constructor(private baseNoun: Noun) {}

    private checkPresence(chance: number) {
        return Math.random() < chance;
    }

    checkPluralPresence() {
        this.baseNounPlural = this.checkPresence(this.baseNounPluralChance);
        return this;
    }

    checkAdjectivePresence() {
        this.adjectivePresent = this.checkPresence(this.adjectiveChance);
        return this;
    }

    checkArticlePresence() {
        this.articlePresent = this.checkPresence(this.articleChance);
        return this;
    }

    checknominalGroupPresence() {
        this.nominalGroupPresent = this.checkPresence(this.nominalGroupChance);
        return this;
    }

    private getRandomNoun() {
        return BlessedAPI.Utils.Array.randomElement(nouns);
    }

    build() {
        let gameName = "";

        if (this.articlePresent) {
            const goodGenredArticle = this.baseNounPlural ? articles.plural : articles[this.baseNoun.genre];
            gameName += goodGenredArticle + " ";
        }

        gameName += this.baseNoun[this.baseNounPlural ? "plural" : "singular"];

        if (this.adjectivePresent) {
            const randomAdjective = BlessedAPI.Utils.Array.randomElement(adjectives);
            gameName += " " + randomAdjective[this.baseNounPlural ? "plural" : "singular"][this.baseNoun.genre];
        }

        if (this.nominalGroupPresent) {
            const nominalGroupNoun = this.getRandomNoun();
            const isPlural = this.checkPresence(0.5);

            const accordingPreposition = prepositions[isPlural ? "plural" : nominalGroupNoun.genre];

            gameName += ` ${accordingPreposition} ${nominalGroupNoun[isPlural ? "plural" : "singular"]}`;

            if (this.checkPresence(0.6)) {
                const nominalGroupAdjective = BlessedAPI.Utils.Array.randomElement(adjectives);
                gameName += ` ${nominalGroupAdjective[isPlural ? "plural" : "singular"][nominalGroupNoun.genre]}`;
            }
        }

        return BlessedAPI.Utils.String.capitalize(gameName);
    }
}