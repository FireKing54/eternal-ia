import { Adjective, Noun } from "./game-names.types";


export const nouns: Noun[] = [
    {"singular": "mausolée", "genre": "m", "plural": "mausolées"},
    {"singular": "château", "genre": "m", "plural": "châteaux"},
    {"singular": "palais", "genre": "m", "plural": "palais"},
    {"singular": "temple", "genre": "m", "plural": "temples"},
    {"singular": "sanctuaire", "genre": "m", "plural": "sanctuaires"},
    {"singular": "cathédrale", "genre": "f", "plural": "cathédrales"},
    {"singular": "basilique", "genre": "f", "plural": "basiliques"},
    {"singular": "ruine", "genre": "f", "plural": "ruines"},
    {"singular": "nécropole", "genre": "f", "plural": "nécropoles"},
    {"singular": "supplice", "genre": "m", "plural": "supplices"},
    {"singular": "volcan", "genre": "f", "plural": "volcans"},
    {"singular": "montagne", "genre": "f", "plural": "montagnes"},
    {"singular": "forêt", "genre": "f", "plural": "forêts"},
    {"singular": "caverne", "genre": "f", "plural": "cavernes"},
    {"singular": "grotte", "genre": "f", "plural": "grottes"},
    {"singular": "canyon", "genre": "m", "plural": "canyons"},
    {"singular": "falaise", "genre": "f", "plural": "falaises"},
    {"singular": "désert", "genre": "m", "plural": "déserts"},
    {"singular": "plaine", "genre": "f", "plural": "plaines"},
    {"singular": "marais", "genre": "m", "plural": "marais"},
    {"singular": "lac", "genre": "m", "plural": "lacs"},
    {"singular": "rivière", "genre": "f", "plural": "rivières"},
    {"singular": "crypte", "genre": "f", "plural": "cryptes"},
    {"singular": "cimetière", "genre": "m", "plural": "cimetières"},
    {"singular": "tombeau", "genre": "m", "plural": "tombeaux"},
    {"singular": "jugement", "genre": "m", "plural": "jugement"},
]

export const adjectives: Adjective[] = [
    {"singular": {"m": "sombre", "f": "sombre"}, "plural": {"m": "sombres", "f": "sombres"}},
    {"singular": {"m": "mystérieux", "f": "mystérieuse"}, "plural": {"m": "mystérieux", "f": "mystérieuses"}},
    {"singular": {"m": "ancien", "f": "ancienne"}, "plural": {"m": "anciens", "f": "anciennes"}},
    {"singular": {"m": "sacré", "f": "sacrée"}, "plural": {"m": "sacrés", "f": "sacrées"}},
    {"singular": {"m": "maudit", "f": "maudite"}, "plural": {"m": "maudits", "f": "maudites"}},
    {"singular": {"m": "désolé", "f": "désolée"}, "plural": {"m": "désolés", "f": "désolées"}},
    {"singular": {"m": "désert", "f": "déserte"}, "plural": {"m": "déserts", "f": "désertes"}},
    {"singular": {"m": "virtuose", "f": "virtuose"}, "plural": {"m": "virtuoses", "f": "virtuoses"}},
    {"singular": {"m": "sauvage", "f": "sauvage"}, "plural": {"m": "sauvages", "f": "sauvages"}},
    {"singular": {"m": "téméraire", "f": "téméraire"}, "plural": {"m": "téméraires", "f": "téméraires"}},
    {"singular": {"m": "oublié", "f": "oubliée"}, "plural": {"m": "oubliés", "f": "oubliées"}},
    {"singular": {"m": "perdu", "f": "perdue"}, "plural": {"m": "perdus", "f": "perdues"}},
    {"singular": {"m": "téméraire", "f": "téméraire"}, "plural": {"m": "téméraires", "f": "téméraires"}},
    {"singular": {"m": "sauvage", "f": "sauvage"}, "plural": {"m": "sauvages", "f": "sauvages"}}
]