import changes from "./changes/changelog.changes";
import { ChangelogService } from "./changelog.service";
import { NoChangelogFoundException } from "./exceptions/no-changelog-found.exception";
import { Discord, Slash } from "discordx";
import { CommandInteraction } from "discord.js";
import { PaginationService } from "../../modules/pagination/pagination.service";

@Discord()
export class ChangelogCommand {
    private readonly changelogService: ChangelogService;

    constructor() {
        this.changelogService = new ChangelogService();
    }

    @Slash({
        "name": "changelog",
        "description": "Affiche un descriptif de la dernière version du bot"
    })
    async changelogHandler(interaction: CommandInteraction) {
        await interaction.deferReply();

        if (changes.length === 0) {
            throw new NoChangelogFoundException();
        }

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            pagination: {
                "currentPageIndex": 0,
                "pages": changes.map((_, index) => {
                    return {
                        "embeds": [this.changelogService.getChangelogByIndex(index)]
                    }
                })
            },
        });

        return interaction.editReply(pagination);
    }
}