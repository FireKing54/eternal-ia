import { BotException } from "../../../errors/bot.exception";

export class NoChangelogFoundException extends BotException {
    constructor() {
        super(
            `Aucun Changelog n'a été trouvé`,
            "Cette erreur ne devrait jamais arriver. Cela signifie qu'aucun changelog n'a été trouvé, alors qu'il devrait toujours y en avoir au moins un."
        )
    }
}