function getCommandName(command: string) {
    return "`/" + command + "`";
}

const changes: Array<Change> = [
    {
        "version": "9.5.6",
        "changes": {
            "upgrade": [
                `Les ex aequo sont maintenant placés derrière les classements "normaux dans la commande ${getCommandName("info leaderboard")}.`
            ]
        },
        "updatedAt": new Date("2024-09-29T17:25:00Z")
    },
    {
        "version": "9.5.5",
        "changes": {
            "upgrade": [
                `Ajout d'un symbole \`=\` pour signifier qu'il y a une ou plusieurs autre personne à égalité sur son propre score, dans la commande ${getCommandName("info leaderboard")}.`
            ]
        },
        "updatedAt": new Date("2024-09-29T17:10:00Z")
    },
    {
        "version": "9.5.4",
        "changes": {
            "upgrade": [
                `Ajout d'une blacklist pour les jeux qui apparaissent dans ${getCommandName("info leaderboard")}. Les jeux Soccerfest ne sont plus affichés dans cette commande.`
            ]
        },
        "updatedAt": new Date("2024-09-29T16:50:00Z")
    },
    {
        "version": "9.5.3",
        "changes": {
            "fix": [
                `Correction de la commande ${getCommandName("quiz recap")} qui n'avait pas exactement le bon classement des joueurs`
            ]
        },
        "updatedAt": new Date("2024-09-29T16:25:00Z")
    },
    {
        "version": "9.5.2",
        "changes": {
            "fix": [
                `Correction et mise à jour de la commande ${getCommandName("quiz recap")}, qui été cassée`
            ]
        },
        "updatedAt": new Date("2024-09-29T16:05:00Z")
    },
    {
        "version": "9.5.1",
        "changes": {
            "fix": [
                `Certains problème de cache sont désormais réglés. C'est notamment le cas de la commande ${getCommandName("info leaderboard")}, qui se met à jour toutes les demi-heures`,
                `Suppression de certaines option inutiles dans certaines commandes`
            ]
        },
        "updatedAt":new Date("2024-09-04T12:10:00Z")
    },
    {
        "version": "9.5.0",
        "changes": {
            "feature": [
                `Une nouvelle commande ${getCommandName("full-greed")} permet de donner une tower of greed sur l'entièreté des jeux Eternalfest combinés.`,
                `Une nouvelle option \`displayAmount\` pour les commandes de tower of greed permet d'afficher le nombre d'objets au bout de leur ligne.`
            ]
        },
        "updatedAt": new Date("2024-09-01T12:30:00Z")
    },
    {
        "version": "9.4.2",
        "changes": {
            "fix": [
                `Correction du traitement des ex-aequo dans ${getCommandName("info leaderboard")} qui avait des ratés dans certains cas.`
            ]
        },
        "updatedAt": new Date("2024-09-01T10:10:00Z")
    },
    {
        "version": "9.4.1",
        "changes": {
            "fix": [
                "Fix temporaire des commandes à autocomplétion d'utilisateurs (merci @Jules93)",
                `Les descriptions items affichés par les commandes ${getCommandName("item")} et ${getCommandName("random item")} n'affichent plus les liens cassés et résiduels en Markdown`,
                `L'affichage des médalles pour ${getCommandName("info leaderboard")} a été retiré pour des raisons de dépendance dépréciée et de sécurité. Il est remplacé par un chiffre de la couleur de la médaille.`
            ]
        },
        "updatedAt": new Date("2024-09-01T10:00:00Z")
    },
    {
        "version": "9.4.0",
        "changes": {
            "feature": [
                `La commande ${getCommandName("greed")} change : elle prend désormais le nom du jeu à la place du serveur, et a une option pour afficher les coefficients 7.`
            ],
            "upgrade": [
                `La commande ${getCommandName("screen")} affiche le screen, ainsi que d'autres informations`
            ],
            "fix": [
                `Les questions du quiz sur le nom d'une famille à partir d'un objet est réparée`,
            ]
        },
        "updatedAt": new Date("2024-07-04T13:35:00Z")
    },
    {
        "version": "9.3.2",
        "changes": {
            "fix": [
                `Les questions du quiz sur les quêtes et les familles sont désormais à nouveau fonctionnelles`
            ]
        },
        "updatedAt": new Date("2024-04-09T21:25:00Z")
    },
    {
        "version": "9.3.1",
        "changes": {
            "fix": [
                `La question du quiz sur l'objet manquant d'une famille est réparée`,
                `La question du quiz sur l'objet manquant d'une quête est réparée`,
                `Les problèmes concernant "picture_id" sur certaines questions du quiz sont réglés`
            ]
        },
        "updatedAt": new Date("2024-04-09T07:25:00Z")
    },
    {
        "version": "9.3.0",
        "changes": {
            "upgrade": [
                `Ajustement du bot pour prendre en compte la mise à jour 1.0 d'Hamm4all`
            ]
        },
        "updatedAt": new Date("2024-03-30T17:00:00Z")
    },
    {
        "version": "9.3.0",
        "changes": {
            "upgrade": [
                `Ajustement du bot pour prendre en compte la mise à jour 1.0 d'Hamm4all`
            ]
        },
        "updatedAt": new Date("2023-11-10T21:45:00Z")
    },
    {
        "version": "9.2.0",
        "changes": {
            "feature": [
                `Le quiz a désormais une "happy hour", une fois par jour, à une heure aléatoire. Pendant cette heure, les questions rapportent 1.5 fois plus de points !`
            ]
        },
        "updatedAt": new Date("2023-11-10T21:45:00Z")
    },
    {
        "version": "9.1.1",
        "changes": {
            "upgrade": [
                `La commande ${getCommandName("greed")} affiche désormais la date de dernière mise à jour du frigo`
            ],
            "fix": [
                `La commande ${getCommandName("bingo tick")} fonctionne désormais avec des lettres minuscules`
            ]
        },
        "updatedAt": new Date("2023-10-31T19:00:00Z")
    },
    {
        "version": "9.1.0",
        "changes": {
            "feature": [
                `Ajout d'une commande ${getCommandName("generate game-name")}, qui permet de générer un nom de contrée aléatoire. Cette commande est encore en test, et n'est pas encore aboutie.`
            ],
            "upgrade": [
                "Ajout de quelques questions de culture générale pour le quiz (inspirée de l'UHC)"
            ]
        },
        "updatedAt": new Date('2023-10-11T14:40:00.000Z')
    },
    {
        "version": "9.0.2",
        "changes": {
            "fix": [
                `La commande ${getCommandName("info leaderboard")} gère désormais les ex aequo correctement`,
                `Le message si on n'a pas la permission d'accepter un titre est désormais caché à la vision de tous`
            ]
        },
        "updatedAt": new Date('2023-10-11T09:00:00.000Z')
    },
    {
        "version": "9.0.1",
        "changes": {
            "fix": [
                `La création de bingo communautaire fonctionne à nouveau`,
                `L'erreur occasionnelle (Array is empty) du quiz est désormais réglée`
            ]
        },
        "updatedAt": new Date('2023-09-25T11:45:00.000Z')
    },
    {
        "version": "9.0.0",
        "changes": {
            "feature": [
                "Certains messages d'erreurs contiennent désormais une explication détaillée, ainsi que des indications sur comment régler le problème",
                `La commande ${getCommandName("info recap")} est de retour, plus efficace qu'avant, sous le nom de ${getCommandName("info leaderboard")}`,
                `Les commandes ${getCommandName("quest")} et ${getCommandName("family")} ont été ajoutées, ainsi que leur homologues random ${getCommandName("random quest")} et ${getCommandName("random family")}`,
            ],
            "upgrade": [
                "L'affichage des tableaux est désormais plus complet",
                "Ajout d'émojis dans les boutons de la pagination",
                "Un bouton indiquant le numéro de page est désormais affiché dans la pagination",
                "Un meilleur système a été mis en place pour pouvoir débugger plus facilement pour les développeurs",
                "Un bouton pour abandonner a été placé sur les questions du quiz"
            ],
            "fix": [
                "Certaines commandes s'exécutent désormais plus rapidement",
                `Correction de ${getCommandName("random level")} qui plantait parfois sur un message d'erreur non explicite`,
                `La commande ${getCommandName("ef")} fonctionne à nouveau`,
                `Meilleurs messages d'erreur pour la commande ${getCommandName("fridge")}`,
                "Tous les bugs du quiz connus ont été réparés"
            ]
        },
        "updatedAt": new Date('2023-07-31T16:30:00.000Z')
    },
    {
        "version": "8.3.1",
        "changes": {
            "fix": [
                `Fix de la commande ${getCommandName("claim")}, qui ne fonctionnait plus`,
                "Mise à jour de la plupart des dépendances"
            ]
        },
        "updatedAt": new Date('2023-04-02T13:10:00.000Z')
    },
    {
        "version": "8.3.0",
        "changes": {
            "feature": [
                "La case à cocher lors d'une demande de coche de bingo communautaire est désormais en surbrillance",
                "Les numéros des cases d'un bingo sont affichés sur le côté"
            ],
            "upgrade": [
                "La grille du bingo communautaire est rappelée à chaque demande de coche",
                "Les grille de bingos sont plus jolies, avec une lettre random affichée en haut à gauche :D"
            ],
            "fix": [
                `Fix des acceptations et refus de cochage de cases du bingo communautaire, qui pouvait être faites par n'importe qui`,
                `Potentiel fix du bug qui faisait qu'on ne pouvait plus accepter de demande de coche de bingo communautaire`,
            ]
        },
        "updatedAt": new Date('2021-01-24T12:20:00.000Z')
    },
    {
        "version": "8.2.4",
        "changes": {
            "fix": [
                `Fix du quiz qui ne tournait que sur une seule question`,
                `Fix du mauvais avatar d'accepteur dans la commande ${getCommandName("bingo tick")}`,
                `L'heure de la case cochée pour le bingo communautaire est désormais bien enregistrée en base de données`,
            ]
        },
        'updatedAt': new Date('2021-01-23T23:00:00.000Z')
    },
    {
        "version": "8.2.3",
        "changes": {
            "fix": [
                "Nettoyage de morceaux de code",
            ]
        },
        "updatedAt": new Date('2021-01-23T17:30:00.000Z')
    },
    {
        "version": "8.2.2",
        "changes": {
            "fix": [
                "La question du quiz sur l'objet manquant d'une famille ou d'une quête s'affiche à nouveau correctement",
                "Ménage dans les dépendances du bot"
            ]
        },
        "updatedAt": new Date('2021-01-23T10:30:00.000Z')
    },
    
    {
        "version": "8.2.1",
        "changes": {
            "fix": [
                "Les personnes qui n'ont pas de permissions sur le bot reçoivent désormais un message d'erreur approprié lorsqu'elles tentent d'utiliser une commande qui nécessite des permissions",
            ]
        },
        "updatedAt": new Date('2021-01-08T15:40:00.000Z')
    },
    {
        "version": "8.2.0",
        "changes": {
            "feature": [
                `La commande ${getCommandName("claim")} accepte maintenant comme preuve supplémentaire la fiche publique Hammerfest d'un joueur`
            ],
            "upgrade": [
                `La commande ${getCommandName("claim")} n'a plus besoin d'une preuve pour accepter un titre`,
            ],
            "fix": [
                "Fix temporaire : la commande `/quiz reset` fonctionne plus quoi qu'il arrive, en attendant une solution plus propre",
                `Le paramètre \`run-id\` de commande ${getCommandName("claim")} accepte désormais les liens complets Gameresult`,
            ]
        },
        "updatedAt": new Date('2023-01-08T15:00:00.000Z')
    },
    {
        "version": "8.1.1",
        "changes": {
            "upgrade": [
                `Remise en place de la commande \`/quiz reset\` pour le quiz, et suppression du bouton \`Abandonner\` sur les questions`,
                `Suppression du temps d'attente pour la réinitialisation du quiz`,
                `Les commandes ${getCommandName("gameresult")} et ${getCommandName("claim")} n'acceptent plus que des ID de parties, ou des liens de la bonne forme`,
            ],
            "fix": [
                `La commande ${getCommandName("title rank")} contient une pagination avec boutons plutôt qu'avec un menu déroulant`,
            ]
        },
        "updatedAt": new Date('2023-01-04T21:40:00.000Z')
    },
    {
        "version": "8.1.0",
        "changes": {
            "feature": [
                "Remise en place des commandes `/greed` et `/fridge`",
            ],
            "upgrade": [
                "Amélioration de l'interface de la commande `/fridge`",
            ],
            "fix": [
                "Possiblement fixé le bug qui freezait le quiz au bout d'un moment",
                "Fix une bonne fois pour toutes des titres qui peuvent être acceptés ou refusés par tout le monde",
            ]
        },
        "updatedAt": new Date('2023-01-02T16:45:00.000Z')
    },
    {
        "version": "8.0.3",
        "changes": {
            "upgrade": [
                "Ajout du numéro de page dans la commande `/title missing`",
            ],
            "fix": [
                "Les permissions pour accepter ou refuser un titre sont désormais correctes",
            ]
        },
        "updatedAt": new Date('2022-12-29T19:00:00.000Z')
    },
    {
        "version": "8.0.2",
        "changes": {
            "upgrade": [
                "Le grade `VIP` et supérieur peut désormais utiliser la commande `/bingo generate`",
                "La commande `/admin` affiche désormais le grade `VIP` et supérieur",
            ],
        },
        "updatedAt": new Date('2022-12-27T23:00:00.000Z')
    },
    {
        "version": "8.0.1",
        "changes": {
            "upgrade": [
                "Ajout de logs plus poussés côté serveur pour pouvoir débugger",
            ],
            "fix": [
                "Fix une typo"
            ]
        },
        "updatedAt": new Date('2022-12-27T22:35:00.000Z')
    },
    {
        "version": "8.0.0",
        "changes": {
            "feature": [
                "Révision complète du code du bot pour adapter aux nouveaux standards de Discord.js",
                "Il est désormais possible de fournir un screenshot comme deuxième preuve pour `/claim`",
                "Lors d'un refus de titre, il est possible de fournir une raison, qui apparaîtra sur le message de claim",
                "Beaucoup de paramètres sont disponibles désormais pour générer un bingo plus personnalisé",
                "Il est possible de générer un bingo communautaire, et de cocher les cases avec la commande `/bingo tick`",
                "Le changelog affiche désormais la date de publication de la version",
            ],
            "upgrade": [
                "La commande `object` s'appelle désormais `item`",
                "Beaucoup de commandes ont de nouvelles interfaces plus sympathiques",
                "La commande `rank` permet désormais d'afficher l'ID de la partie et les options en plus du classement",
                "La génération de Bingo est désormais beaucoup plus rapide"
            ]
        },
        "updatedAt": new Date('2022-12-26T17:40:00.000Z')
    },
    {
        "version": "7.12.1",
        "changes": {
            "upgrade": [
                "La commande `/info recap` gère désormais les ex-aequo"
            ],
            "fix": [
                "L'arguments `game-id` de la commande `/claim` devient `run-id`"
            ]
        }
    },
    {
        "version": "7.12.0",
        "changes": {
            "upgrade": [
                "La commande `/ef` affiche désormais la liste des contrées en suggestion de remplissage",
                "La commande `/rank` affiche désormais la liste des contrées en suggestion de remplissage",
                "La commande `/object` affiche désormais la liste des noms des objets en suggestion de remplissage",
                "La commande `/screen` affiche désormais la liste des contrées et le nom du niveau en suggestion de remplissage",
                "La commande `/info recap` affiche désormais la liste des joueurs en suggestion de remplissage"
            ],
            "fix": [
                "La commande `/title group assign` a un plus beau rendu",
                "Les objets à point affichent bien `0` en valeur pour `/object`, et non plus `undefined`",
                "Les Changelogs sont triés correctement",
                "Les messages renvoyés par `/title group assign` et `/title group register` sont désormais privés"
            ]
        }
    },
    {
        "version": "7.11.0",
        "changes": {
            "upgrade": [
                "La commande `/award` a une nouvelle interface",
                "La commande `/claim` a, par extension, une nouvelle interface également",
                "Le message de demande de titre s'affiche en rouge quand il est refusé, et en vert quand il est accepté",
                "Il est désormais impossible de demander un rôle que l'on a déjà",
                "On ne peut pas demander un rôle qui ne compte pas pour les méta titres"
            ],
            "fix": [
                "Refuser le titre permet VRAIMENT de refuser le titre"
            ]
        }
    },
    {
        "version": "7.10.0",
        "changes": {
            "upgrade": [
                "Ajout d'une classification des anecdotes de `/hammerfact` par niveau",
                "`/hammerfact` affiche désormais le nombre total de questions",
                "Ajout d'un paramètre optionnel `level` permettant de sélectionner le niveau de l'anecdote voulue",
                "Ajout d'un paramètre optionnel `number` permettant de sélectionner un numéro de fait en particulier",
                "Ajout de 22 anecdotes pour `/hammerfact`"
            ],
            "fix": [
                "Corrections de fautes d'orthographes dans les Hammerfacts"
            ]
        }
    },
    {
        "version": "7.9.0",
        "changes": {
            "upgrade": [
                "Ajout d'une classification des anecdotes de `/hammerfact` par niveau",
                "`/hammerfact` affiche désormais le nombre total de questions",
                "Ajout d'un paramètre optionnel `level` permettant de sélectionner le niveau de l'anecdote voulue",
                "Ajout d'un paramètre optionnel `number` permettant de sélectionner un numéro de fait en particulier",
                "Ajout de 22 anecdotes pour `/hammerfact`"
            ]
        }
    },
    {
        "version": "7.8.0",
        "changes": {
            "feature": [
                "Ajout de la commande `/hammerfact`, qui donne un fait, une histoire ou une anecdote sur Hammerfest"
            ],
            "upgrade": [
                "Le bot réagira par 👏 au message apparaissant si un rôle est accepté par la commande `/claim`"
            ]
        }
    },
    {
        "version": "7.7.0",
        "changes": {
            "feature": [
                "Ajout de la commande `/claim`, permettant de demander un titre en fournissant un gameresult comme preuve"
            ],
            "fix": [
                "Les messages d'erreur concernant le NDJ sont désormais cachés au vu de tous"
            ]
        }
    },
    {
        "version": "7.6.4",
        "changes": {
            "fix": [
                "Correction de la désinscription au NDJ Ping qui était buggée",
                "Fix des messages d'inscription et désinscription des pings du NDJ qui étaient inversés"
            ]
        }
    },
    {
        "version": "7.6.3",
        "changes": {
            "feature": [
                "Ajout de la commande `/donation` pour faire un don et soutenir le bot"
            ]
        }
    },
    {
        "version": "7.6.2",
        "changes": {
            "upgrade": [
                "Les erreurs dû aux mauvaises utilisations de commandes sont entourées de l'émoji ❌, au lieu de [BOT_ERROR]",
                "Les erreurs de commandes n'apparaissent que pour l'utilisateur de la commande"
            ],
            "fix": [
                "Corrections de typo dans la commande `/award`",
                "Corrections de typo dans la commande `/ef`",
                "Corrections mineurs en cas de frigo non trouvé pour `/greed`",
                "Petites corrections de typo diverses dans `/title missing`"
            ]
        }
    },
    {
        "version": "7.6.1",
        "changes": {
            "upgrade": [
                "La commande `/title recap` peut être appelée sur sur un autre joueur que soi-même"
            ],
            "fix": [
                "Les classements globaux se mettent désormais à jour **toutes les heures**",
                "Optimisations diverses du code pour la pagination"
            ]
        }
    },
    {
        "version": "7.6.0",
        "changes": {
            "feature": [
                "La commande `/info recap` contient désormais une option pour afficher le mode solo uniquement, ou tous les modes"
            ],
            "upgrade": [
                "La commande `/info recap` n'affiche plus que les modes solo par défaut"
            ],
            "fix": [
                "Les noms de modes de la commande `/info recap` sont en noir au lieu de rouge"
            ]
        }
    },
    {
        "version": "7.5.0",
        "changes": {
            "feature": [
                "La commande `/info recap` affiche désormais le détail des classements dans toutes les contrées et tous les modes"
            ]
        }
    },
    {
        "version": "7.4.1",
        "changes": {
            "fix": [
                "Le classement des contrées n'ayant pas de sets de niveaux par défaut fonctionnent correctement avec la commande `/leaderboard`"
            ]
        }
    },
    {
        "version": "7.4.0",
        "changes": {
            "feature": [
                "Ajout de la commande `/info update`, qui permet d'actualiser tous les classements en cache",
                "Ajout de la commande `/info recap`, qui montre les classements globaux du joueur passé en paramètre",
                "Par défaut, l'actualisation des classements se fait chaque jour à munuit automatiquement"
            ]
        }
    },
    {
        "version": "7.3.1",
        "changes": {
            "fix": [
                "Le quiz ne se réinitialise plus pour rien",
                "Corrections de typo"
            ]
        }
    },
    {
        "version": "7.3.0",
        "changes": {
            "feature": [
                "Ajout de la commande `/random screen`",
                "Ajout de la commande `/random object`"
            ]
        }
    },
    {
        "version": "7.2.0",
        "changes": {
            "feature": [
                "Support de la traduction anglaise retiré, car cela devenait très handicapant dans le développement"
            ],
            "upgrade": [
                "La commande `/quiz recap` peut désormais être appelée sur un autre joueur",
                "La commande `/quiz stats` peut désormais être appelée sur un autre joueur"
            ],
            "fix": [
                "Le cooldown de la commande `/quiz reset` passe de 20 secondes à 15 secondes",
                "Le cooldown de la réinitialisation du quiz si personne ne répond passe de 1h à 10 minutes"
            ]
        }
    },
    {
        "version": "7.1.1",
        "changes": {
            "fix": [
                "La commande `/rank` affiche désormais tous les gamemodes d'une contrée, même si ceux-ci ne sont pas accessibles par défaut (comme le prélude d'Hackfest)"
            ]
        }
    },
    {
        "version": "7.1.0",
        "changes": {
            "feature": [
                "La commande `/rank` a désormais un menu, permettant de choisir le mode de jeu (encore expérimental)"
            ],
            "fix": [
                "Fix de quelques erreurs de typo"
            ]
        }
    },
    {
        "version": "7.0.0",
        "changes": {
            "feature": [
                "Toutes les commandes ont été remplacées par des commandes `slash`",
                "Simplification drastique de la gestion des commandes côté utilisateur",
                "/title missing peut être appelé pour un autre utilisateur",
                "Les commandes avec de la pagination utilisent désormais des boutons, au lieu des réactions",
                "Les commandes gérant le ping du NDJ utlisent désormais les boutons au lieu des réactions"
            ],
            "upgrade": [
                "La souscription au ping du NDJ se fait désormais toutes les 5 minutes au minimum"
            ],
            "fix": [
                "La commande `/quiz rank` affiche désormais aussi les joueurs qui ont quitté le serveur",
                "Les commandes comme `/title`, ou `/quiz` qui ont des sous-commandes ont été renommées en `/title recap` et `/quiz recap`"
            ]
        },
    },
    {
        "version": "6.5.0",
        "changes": {
            "feature": [
                "Ajout du flag `--days` pour la commande `!quiz stats` qui permet de spécifier le nombre de jours à afficher sur le graphique"
            ],
            "upgrade": [
                "La commande `!quiz stats` s'arrête désormais à 1 mois par défaut"
            ]
        }
    },
    {
        "version": "6.4.5",
        "changes": {
            "upgrade": [
                "Le timer est plus rapide à se lancer quand on relance un quiz avec la réaction",
                "Ajout de 25 nouvelles questions de culture Hammerfestiane pour l'Eternal Quiz"
            ],
            "fix": [
                "Légères optimisation de certains types de questions pour le quiz"
            ]
        }
    },
    {
        "version": "6.4.4",
        "changes": {
            "fix": [
                "Meilleur contraste du texte pour la commande `!quiz stats`"
            ]
        }
    },
    {
        "version": "6.4.3",
        "changes": {
            "upgrade": [
                "Nette amélioration de la commande `!quiz stats`"
            ]
        }
    },
    {
        "version": "6.4.2",
        "changes": {
            "upgrade": [
                "Suppression du nombre de questions répondues pour `!quiz stats` et ajout à la place de la moyenne sur 7 jours glissants"
            ]
        }
    },
    {
        "version": "6.4.1",
        "changes": {
            "upgrade": [
                "Les `?` apparaissant pour les questions du quiz sont d'une autre couleur, pour pouvoir être localisés facilement"
            ],
            "fix": [
                "Equlibrages mineurs de certaines questions du quiz"
            ]
        }
    },
    {
        "version": "6.4.0",
        "changes": {
            "feature": [
                "Ajout de la commande `!quiz stats`, qui montre sous forme de graphique les réponses données au quiz pour la personne qui lance la commande"
            ],
            "fix": [
                "Quand personne ne répond pas au quiz, les réponses à la question sont désormais affichées également",
                "Il n'y a plus de chronomètre quand une question est posée pour l'Eternal Quiz : cela fixe le bug faisant que parfois, le bot ne prend pas en compte les réponses rapides, même si elles sont justes",
                "Les questions concernant les familles et les quêtes sont plus rares",
                "Les questions demandant le coefficient d'un objet sont un peu moins rares",
                "Le noms des titres post-100k ont été changé pour le Quiz",
                "Des titres intermédiaires ont été rajoutés post-100k"
            ]
        }
    },
    {
        "version": "6.3.2",
        "changes": {
            "fix": [
                "Ajustement de certaines récompenses de points pour l'Eternal Quiz",
                "Les bots ne sont plus autorisés à répondre aux questions de l'Eternal Quiz",
                "La commande `!quiz rank` fonctionne même si quelqu'un qui a quitté le serveur est dans le classement",
                "Optimisations diverses, corrections de bugs rares et mineurs",
                "Possible fix du vidage du cache, qui faisait crasher le bot parfois"
            ]
        }
    },
    {
        "version": "6.3.1",
        "changes": {
            "upgrade": [
                "Un cooldown a été ajouté pour le reset du Quiz. Ce cooldown a cenpendant un pas de 2 secondes au lieu d'une seule."
            ],
            "fix": [
                "Le cooldown des questions de l'Eternal Quiz devrait se figer moins souvent",
            ]
        }
    },
    {
        "version": "6.3.0",
        "changes": {
            "feature": [
                "Ajout de deux nouveaux types de questions pour le quiz, concernant les **familles**",
                "Ajout d'un cache sur les objets, pour permettre un chargement plus rapide de certaines commandes"
            ],
            "upgrade": [
                "Les objets du quiz étant affectés par le cache, ils se chargeront beaucoup plus vite une fois les objets déjà chargés",
                "La commande `!obj` sera plus rapide si l'objet a déjà été chargée ailleurs avant"
            ]
        }
    },
    {
        "version": "6.2.2",
        "changes": {
            "fix": [
                "Fix du ? qui pour les questions des quêtes, était plus petit en première position"
            ]
        }
    },
    {
        "version": "6.2.1",
        "changes": {
            "fix": [
                "Equilibrages de points mineurs pour l'Eternal Quiz"
            ]
        }
    },
    {
        "version": "6.2.0",
        "changes": {
            "feature": [
                "Ajout de 3 nouveaux types de questions pour l'Eternal Quiz, concernant les **quêtes**"
            ],
            "fix": [
                "Certains nombres n'avaient pas d'espaces en 3 chiffres pour la commande `!quiz`",
                "La question demandant le nom de la famille est légèrement moins rare"
            ]
        }
    },
    {
        "version": "6.1.0",
        "changes": {
            "feature": [
                "La commande `!quiz` affiche désormais les nombre de questions et les points obtenus sur les deux jours précédents",
                "La commande `!quiz` affiche désormais une barre de progression au titre suivant, ainsi qu'un poucentage de complétion",
                "La commande `!quiz` affiche désormais les questions avec la couleur de leur rareté, pour une question d'esthétisme"
            ],
            "upgrade": [
                "La commande `!quiz` est désormais plus lente, mais affiche un résultat nettement plus joli"
            ]
        }
    },
    {
        "version": "6.0.2",
        "changes": {
            "upgrade": [
                "Ajout d'un poucentage de complétion pour passer au palier suivant avec l'Eternal Quiz"
            ]
        }
    },
    {
        "version": "6.0.1",
        "changes": {
            "fix": [
                "Fix du message de montée en grade du quiz qui affichait [object Object]",
                "Fix d'une typo dans la commande Eternalfest",
                "Fix de `quiz rank` qui ne se lançait plus"
            ]
        }
    },
    {
        "version": "6.0.0",
        "changes": {
            "feature": [
                "Nouvelle commande `!ef`, permettant d'avoir des informations sur la contrée demandée",
                "La commande `!ef` donne les musiques utilisées dans la contrée ! Cependant attention : il faut être connecté à Eternalfest sur le navigateur pour pouvoir les écouter"
            ],
            "upgrade": [
                "Le quiz stocke désormais la rareté de chaque question, la personne qui y a répondu, sa date et sa rareté",
                "La commande `!quiz` affiche désormais des statistiques sur la rareté des questions répondues"
            ]
        }
    },
    {
        "version": "5.7.2",
        "changes": {
            "upgrade": [
                "Ajout d'une erreur plus propre si la commande est exécutée en DM quand elle n'est pas autorisée"
            ],
            "fix": [
                "Le temps d'attente pour une question de Quiz passe de 10 à 5s",
                "Le temps d'attente pour le reset du quiz passe de 30 à 20s",
                "Le points décroissent de 3% chaque seconde, au lieu de 2.5"
            ]
        }
    },
    {
        "version": "5.7.1",
        "changes": {
            "fix": [
                "Fix d'un bug qui donnait parfois les points à au joueur qui lançait le quiz plutôt qu'à celui qui qui répondait correctement"
            ]
        }
    },
    {
        "version": "5.7.0",
        "changes": {
            "feature": [
                "Ajout de titres pour l'Eternal Quiz, en fonction des points que l'on a",
            ],
            "upgrade": [
                "La commande `!quiz` possède désormais une bien meilleure interface",
                "Le classement du quiz affiche désormais le titre du joueur"
            ],
            "fix": [
                "Fix d'un bug terriblement bien caché qui empêchait les questions mythiques d'apparaître"
            ]
        }
    },
    {
        "version": "5.6.6",
        "changes": {
            "upgrade": [
                "Très légère amélioration des performances globales",
                "Correction de quelques fautes d'orthographe",
                "Correction de la question sur la première apparition du litchi"
            ]
        }
    },
    {
        "version": "5.6.5",
        "changes": {
            "upgrade": [
                "Ajout d'un coefficient aléatoire de plus ou moins 10% sur les points de base d'une question, pour ne pas qu'on puisse apprendre les points d'une question pour trouver la réponse"
            ]
        }
    },
    {
        "version": "5.6.4",
        "changes": {
            "fix": [
                "Les réponses aux questions juste après un `!quiz end` ne rapportent plus le double de points"
            ]
        }
    },
    {
        "version": "5.6.3",
        "changes": {
            "fix": [
                "Correction d'un mauvais affichage pour deux commandes du NDJ",
                "Les points affichés par la commande `!quiz` sont également affichés avec un espace"
            ]
        }
    },
    {
        "version": "5.6.2",
        "changes": {
            "fix": [
                "Les commandes avec des pages ne peuvent plus aller dans les pages négatives",
                "Les pages du changelog ne vont plus au-delà des limites des versions disponibles",
                "Les points du quiz ont maintenant un espace pour chaque paquet de 3 chiffres"
            ]
        }
    },
    {
        "version": "5.6.1",
        "changes": {
            "upgrade": [
                "Le calcul de points se fait au millième de seconde près au lieu d'une seconde près. De ce fait, il n'y a plus de \"paliers\" de points à chaque seconde"
            ],
            "fix": [
                "Correction du bug du quiz qui parfois faisait descendre les points trop vite, et baisser le maximum de points possibles (Merci eipi :heart:)"
            ]
        }
    },
    {
        "version": "5.6.0",
        "changes": {
            "feature": [
                "Le Niveau du Jour est maintenant rappelé en message privé",
                "Cocher le NDJ aura effet immédiatement, et non pas au prochain ping"
            ],
            "upgrade": [
                "La miniature du bot n'a plus la barre de chargement"
            ],
            "fix": [
                "Les commandes donnant un classement ou le lancement du quiz (et quelques autres) ne sont plus autorisées en message privé du bot",
                "La commande de cooldown d'une question n'affiche plus qu'il faut utiliser la commande `!r`",
                "Légère correction de la question de culture sur la Chauve Souris du niveau 100",
                "Correction de la question sur les cannes de la 33 avec 2 bombes"
            ]
        }
    },
    {
        "version": "5.5.1",
        "changes": {
            "fix": [
                "Les points du quiz sont bien attribués même en cliquant sur la réaction",
                "Correction d'une faute de français dans la question #50"
            ]
        }
    },
    {
        "version": "5.5.0",
        "changes": {
            "feature": [
                "La commande `!r` a été supprimée. Pour répondre au quiz, il suffit d'écrire simplement la réponse",
                "Possibilité de recommencer un quiz en réagissant au message de bonne réponse"
            ],
            "fix": [
                "Les points du quiz commencent à décroître au bout de 5s, au lieu de 3."
            ]
        }
    },
    {
        "version": "5.4.7",
        "changes": {
            "upgrade": [
                "Le message de bonne réponse pour l'Eternal Quiz donne désormais aussi le temps de réponse"
            ]
        }
    },
    {
        "version": "5.4.6",
        "changes": {
            "upgrade": [
                "Ajout de 33 nouvelles questions de culture Hammerfestiane pour l'Eternal Quiz",
                "Les questions demandant la famille d'un objet sont légèrement moins rares"
            ]
        }
    },
    {
        "version": "5.4.5",
        "changes": {
            "fix": [
                "Les questions ne demandent la valeur d'un objet si elle n'est pas définie",
                "La question du quiz demandant les noms d'objet supprime les éventuels espaces avant ou après leur nom"
            ]
        }
    },
    {
        "version": "5.4.4",
        "changes": {
            "upgrade": [
                "Les réponses au quiz acceptent aussi les `´` et les `’` faites par un clavier de téléphone",
            ]
        }
    },
    {
        "version": "5.4.3",
        "changes": {
            "upgrade": [
                "Amélioration drastique de la page de changelog",
            ],
            "fix": [
                "Les changelogs sont réparties en trois catégories : les nouveautés, les amélioration et les bigfix"
            ]
        }
    },
    {
        "version": "5.4.2",
        "changes": {
            "upgrade": [
                "Ajout de la pagination pour la comamnde `!log`"
            ]
        }
    },
    {
        "version": "5.4.1",
        "changes": {
            "upgrade": [
                "Nette amélioration graphique de la commande `!help`"
            ]
        }
    },
    {
        "version": "5.4.0",
        "changes": {
            "feature": [
                "Les commandes donnant un classement sont désormais navigables au moyen de pages et de réactions"
            ],
            "upgrade": [
                "Les classements n'ont plus de paramètre `size` ou `page`",
                "Les noms anglais fonctionnent sur les questions ou une réponse équivalente en anglais existe"
            ],
            "fix": [
                "Nettoyage de certaines traductions devenues inutilisées",
                "Les questions du quiz demandant le nom de la famille passent de 20 points à 15 points",
                "Les questions de culture Hammerfestiane passent de 10 points à 8 points",
                "Correction de quelques questions ambiguës"
            ]
        }
    },
    {
        "version": "5.3.3",
        "changes": {
            "upgrade": [
                "Ajout de 32 nouvelles questions de culture hammerfestiane pour l'Eternal Quiz"
            ],
            "fix": [
                "Les questions de culture sont un peu moins rares"
            ]
        }
    },
    {
        "version": "5.3.2",
        "changes": {
            "upgrade": [
                "Adaptation de la commande `!result` pour qu'il traite les URL complets et autres cas d'URL semi-complets"
            ]
        }
    },
    {
        "version": "5.3.1",
        "changes": {
            "upgrade": [
                "La commande `!quiz reset` affiche désormais la bonne réponse à la question à sa réinitialisation"
            ]
        }
    },
    {
        "version": "5.3.0",
        "changes": {
            "feature": [
                "Ajout d'un nouveau type de question pour l'Eternal Quiz"
            ]
        }
    },
    {
        "version": "5.2.2",
        "changes": {
            "upgrade": [
                "Ajout d'un cap maximum à 8 pour la génération de grille de Bingo"
            ]
        }
    },
    {
        "version": "5.2.1",
        "changes": {
            "fix": [
                "La grille de Bingo générée prend en compte les diagonales",
                "Le champ 'Tentatives' a été retiré",
                "Le nombre de coeff génériques maximum par défaut est maintenant à 1 au lieu de 2"
            ]
        }
    },
    {
        "version": "5.2.0",
        "changes": {
            "feature": [
                "Ajout d'un nouveau type de question pour l'Eternal Quiz"
            ],
            "fix": [
                "La question qui demande le coefficient d'un objet est légèrement moins rare"
            ]
        }
    },
    {
        "version": "5.1.0",
        "changes": {
            "upgrade": [
                "Grosse amélioration graphique de la commande `!obj`",
                "Les objets maintenant sont grossis et en très haute qualité"
            ]
        }
    },
    {
        "version": "5.0.0",
        "changes": {
            "feature": [
                "Ajout de la commande `!bingo start`, qui permet de générer une grille de Bingo paramétrable"
            ]
        }
    },
    {
        "version": "4.4.0",
        "changes": {
            "feature": [
                "Ajout d'un système de rareté de questions : parfois, les questions seront de rareté supérieures et rapporteront plus de points"
            ],
            "upgrade": [
                "Le message de démarrage d'un quiz actualise le temps qu'il reste avant que la question ne soit posée",
                "Le message de démarrage d'un quiz disparait quand la question est posée",
                "Affichage de la rareté de la question, avec le coefficient multiplicateur",
                "Affichage des points de base d'une question, quand elle est posée",
                "Les couleurs des questions changent en fonction de la rareté dans l'ordre vert, bleu, violet, orange, rouge",
                "Affinage des points en prenant en compte la difficulté de la réponse. Ainsi, les noms de niveaux longs et compliqués rapporteront plus de points que les niveaux 0 ou 1",
                "Affinage des points sur la longueur de la réponse, pour les noms de contrées et de familles"
            ],
            "fix": [
                "La commande `!r` n'envoie plus de message d'erreur si elle est utilisée hors d'un quiz",
                "La question demandant le nom de la famille d'un objet est désormais beaucoup plus rare (environ 1 sur 35)"
            ]
        }
    },
    {
        "version": "4.3.3",
        "changes": {
            "upgrade": [
                "Ajout du paramètre `--noFloconEnorme` pour la Tower of Greed, qui permet de ne pas afficher le(s) flocon(s) énorme(s)"
            ],
            "fix": [
                "Ajout d'une traduction manquantes sur le Tower of Greed"
            ]
        }
    },
    {
        "version": "4.3.2",
        "changes": {
            "fix": [
                "Correction du bug qui déclenchait parfois plusieurs commandes d'un seul coup"
            ]
        }
    },
    {
        "version": "4.3.1",
        "changes": {
            "fix": [
                "Fix du bug rare du quiz qui ne posait jamais la question",
                "Fix du bug pour lequel il était impossible d'utiliser deux fois la commande `!quiz reset",
                "La question demandant la famille de l'objet est plus rare",
                "La question demandant la famille de l'objet donne 20 points maximum au lieu de 15"
            ]
        }
    },
    {
        "version": "4.3.0",
        "changes": {
            "feature": [
                "Ajout d'un nouveau type de questions pour le quiz : les familles"
            ]
        }
    },
    {
        "version": "4.2.0",
        "changes": {
            "feature": [
                "Ajout de la commande `!towerofgreed`, qui génère sa Tower of Greed"
            ],
            "upgrade": [
                "Les questions du quiz sont désormais capées à leur valeur maximum de points pendant 3 secondes avant de décroître",
                "Le reset du quiz envoie désormais un message d'erreur si un autre reset a déjà été lancé",
                "Ajout d'un cap minimum de 20% des points de base pour le quiz (soit 1 point)"
            ],
            "fix": [
                "La question du quiz demandant le nom du niveau demande désormais le `numéro` du niveau",
                "La question demandant le nom du niveau est passée de 18 points maximum à 12 points maximum",
                "Les questions demandant le nom d'une contrée à partir d'un niveau, et celle demandant le nom d'un objet sont plus communes",
                "Possible fix des questions rapportant 0 point (mais pas sûr)",
            ]
        }
    },
    {
        "version": "4.1.0",
        "changes": {
            "feature": [
                "Mise en place de la commande `!quiz reset`",
                "Ajout d'un cooldown à la commande, pour ne pas pouvoir spammer dès qu'on ne sait pas"
            ],
            "upgrade": [
                "Répondre correctement à un quiz arrête le cooldown de réinitialisation"
            ],
            "fix": [
                "Ajout nouvelles traductions et de certaines traductions manquantes"
            ]
        }
    },
    {
        "version": "4.0.2",
        "changes": {
            "fix": [
                "La commande `!quiz rank` s'affiche désormais à tous les coups",
                "Potentiel fix de certains quiz qui buguaient",
                "Le classement `!quiz rank` affiche 10 personnes par défaut désormais",
                "Equilibrage des points reçus en fonction du temps",
                "Fix de la commande `!quiz` qui n'arrondissait pas le nombre de points"
            ]
        }
    },
    {
        "version": "4.0.1",
        "changes": {
            "fix": [
                "Correction de certains classements qui n'étaient pas complets au départ"
            ]
        }
    },
    {
        "version": "4.0.0",
        "changes": {
            "feature": [
                "Ajouts de traductions en anglais",
                "Ajout des commandes `!lang` et `!lang change`",
                "Possibilité de choisir sa langue",
                "Eternal Quiz ! Accessible avec `!quiz start`",
                "Ajout d'un système de points au quiz",
                "Classement pour l'Eternal Quiz"
            ],
            "upgrade": [
                "Si une traduction n'est pas disponible dans la langue choisie, elle est rabattue sur d'autres langues automatiquement",
                "les pluriels sont mieux gérés (il n'y aura plus de `1 titres`)",
            ],
            "fix": [
                "fix probable du classement Discord, qui est complet dès le départ",
            ]
        }
    },
    {
        "version": "3.1.0",
        "changes": {
            "feature": [
                "Ajout de la commande `!title group list`",
                "Ajout de la commande `!title group assign`",
                "Ajout de la commande `!title group register`",
            ],
            "upgrade": [
                "Modification de la commande `!title missing` pour prendre en compte les titres améliorables",
            ],
            "fix": [
                "Rectification du problème de la commande `!ndjping HHhMM`",
                "La commande `!title list` trie désormais par nom de rôle, plutôt que par coefficient",
                "La réinitialisation du NDJ arrive à 2h du matin, pour s'adapter à l'heure d'été"
            ]
        }
    },
    {
        "version": "3.0.3",
        "changes": {
            "fix": [
                "La commande `!title rank` n'affiche bien que 10 personnes maximum par défaut",
                "La commande `!title rank` affiche toujours le bon classement en fin de tableau, même si on lui passe le paramètre `--size`"
            ]
        }
    },
    {
        "version": "3.0.2",
        "changes": {
            "fix": [
                "La commande `!title rank` affiche désormais les bonnes positions en face des bonnes personnes"
            ]
        }
    },
    {
        "version": "3.0.1",
        "changes": {
            "fix": [
                "Correction de la pagination pour la commande `!title list`"
            ]
        }
    },
    {
        "version": "3.0.0",
        "changes": {
            "feature": [
                "Refonte de la gestion des commandes : ajout de flags",
                "Ajouts de commandes :`!title`, `!title rank`, `!title register`, `!title list`, `!title missing`",
                "Les paramètres (comme `rank15 faille`) sont devenus des flags (en l'occurence, `rank faille --size=15`). Ca complexifie les choses mais c'était nécessaire compte tenu de la complexité grandissante de commandes que le bot doit pouvoir traiter"
            ],
            "upgrade": [
                "Messages plus explicites en cas d'erreurs",
                "La commande !help n'affiche désormais plus que les noms de commande : le détail de chaque commande est accessible avec !manual <nom de la commande>",
                "Certaines commandes ont changé leur design d'affichage",
            ],
            fix: [
                "Les commandes `!registerrole` et `!registerrole list` sont remplacées respectivement par `!title register` et `title list`"
            ]
        }
    },
    {
        "version": "2.0.0",
        "changes": {
            feature: [
                "Ajout d'un système de grade",
                "Ajout de la commande !promote, accessible que par les administrateurs",
                "Ajout de la commande !registerrole, permettant d'enregistrer un rôle avec son coefficient, comme comptant dans le calcul des méta-titres",
                "Ajout de la commande !award, permettant de d'assigner un rôle comptant pour les méta-titres",
                "Les rôles ajoutés par !award calculent et assignent automatiquement le meta-titre correspondant"
            ]
        }
    },
    {
        "version": "1.8.0",
        "changes": {
            feature: [
                "Possibilité de choisir individuellement l'heure de ping",
                "Ajout de la commande `ndjping list` pour lister les moments de ping de l'utilisateur",
            ],
            upgrade: [
                "Les messages où les réactions sont analysées sont tous les message du jour, et non pas le dernier",
            ],
            fix: [
                "L'affichage de l'heure de ping est rectifiée (ex : 15h00 au lieu de 15h0)",
                "Désormais, les pings arrivent légèrement après l'actualisation, pour éviter que certains message cochés comptent pour le jour avant l'actualisation"
            ]
        }
    },
    {
        "version": "1.7.0",
        "changes": {
            "feature": [
                "Ajout de la commande `!object`",
                "Ajout des alias de commande ! En voici quelques uns : `frigo`, `classement`, `level`, `niveau`, `obj`, `objet`, `pingndj`",
                "Possibilité de choisir individuellement l'heure de ping"
            ]
        }
    },
    {
        "version": "1.5.0",
        "changes": {
            "feature": [
                "Passage sous Postgres",
            ],
            "upgrade": [
                "Allégement de certains traitements"
            ]
        }
    },
    {
        "version": "1.3.1",
        "changes": {
            "upgrade": [
                "Le bot ajoute désormais une réaction automatique à chaque message de rappel du NDJ"
            ],
            "fix": [
                "Réparation de la commande `!screen`",
            ]  
        }
    },
    {
        "version": "1.2.0",
        "changes": {
            "feature": [
                "Possibilité d'être ping deux fois par jour avec la commande `!ndjping`",
                "Refaire cette commande permet de ne plus se faire ping",
                "Réagir d'une certaine manière aux message de ping permet de ne plus se faire ping aux prochains de la journée"
            ]
        }
    },
    {
        "version": "1.1.0",
        "changes": {
            "feature": [
                "Entourer par une paire de \" une contrée dont on demande le classement, permet une recherche exacte sans mot-clé.",
                "Ajout de la commande \"!changelog\".",
                "Ajout d'un rappel pour jouer au Niveau du Jour."
            ],
            "upgrade": [
                "Les contrées dont on demande le classement ne sont plus sensibles aux accents.",
            ]
        }
    },
    {
        "version": "1.0.0",
        "changes": {
            "feature": [
                "Ajout d'une page d'aide avec `!help`",
                "Passage en ligne !"
            ]
        }
    },
    {
        "version": "0.2.0",
        "changes": {
            "feature": [
                "Ajout de la commande `!rank`, pour avoir le classement des joueurs d'une contrée"
            ]
        }
    },
    {
        "version": "0.1.1",
        "changes": {
            "feature": [
                "Ajout de la commande `!fridge`, pour analyser le frigo de quelqu'un"
            ]
        }
    },
    {
        "version": "0.1.0",
        "changes": {
            "feature": [
                "Ajout de la commande `!result`"
            ]
        }
    }
]

export default changes;