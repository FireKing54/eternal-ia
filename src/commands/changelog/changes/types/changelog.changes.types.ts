interface Change {
    "version": string,
    "changes": {
        "feature"?: Array<string>
        "upgrade"?: Array<string>,
        "fix"?: Array<string>,
    },
    "updatedAt"?: Date
}