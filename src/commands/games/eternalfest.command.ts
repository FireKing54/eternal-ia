import Games from "../../cache/Games";
import { APIEmbedField, ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction, EmbedBuilder } from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import { Game } from "blessed-api/dist/eternalfest/v1/types/eternalfest.game.types";
import { ETERNALFEST_GAMES_URL } from "../../Const";
import BlessedAPI from "blessed-api";
import { eternalfestGameCache } from "../../cache/eternalfest.cache";
import { MusicsService } from "./musics/musics.service";
import { PaginationService } from "../../modules/pagination/pagination.service";

@Discord()
export class EternalfestCommand {

    private musicsService: MusicsService;

    constructor() {
        this.musicsService = new MusicsService();
    }

    @Slash({
        "name": "ef",
        "description": "Donne des informations générales sur la contrée demandée"
    })
    public async perform(
        @SlashOption({
            "name": "land",
            "description": "Contrée à afficher",
            "required": true,
            "type": ApplicationCommandOptionType.String,
            "autocomplete": async (inter: AutocompleteInteraction) => {
                const input = inter.options.getFocused();
                await inter.respond(await Games.autocomplete(input));
            },
        })
        landID: string,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        // Get the game
        const game = await eternalfestGameCache.getGameByID(landID);

        // Get all real musics
        const musics = await this.musicsService.getAllRealMusics(game);

        // Format all musics in the markdown link format
        const formattedMusics = await this.musicsService.formatMusicsToMarkdownLink(musics);

        // Get the thumbnail
        const { thumbnail } = game;

        const pages = formattedMusics.map((_, index) => {
            return {
                "embeds": [this.buildResult(game, formattedMusics, index)],
                "files": [{
                    "name": "image.png",
                    "attachment": thumbnail
                }]
            }
        })

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": pages
            }
        });

        return interaction.editReply(pagination)
    }

    private buildResult(land: Game, musics: Array<APIEmbedField>, page: number) {
        const build = land.channels.active.build;
    
        return new EmbedBuilder()
            .setTitle(build.display_name)
            .setDescription(build.description)
            .setAuthor({
                "name": land.owner.display_name
            })
            .setURL(`${ETERNALFEST_GAMES_URL}/${land.id}`)
            .setThumbnail("attachment://image.png")
            .setColor([50, 200, 200])
            .addFields([
                {
                    "name": "Création",
                    "value": new Date(land.created_at).toLocaleDateString("fr-FR"),
                    "inline": true
                },
                {
                    "name": "Publication",
                    "value": new Date(land.channels.active.publication_date).toLocaleDateString("fr-FR"),
                    "inline": true
                },
                {
                    "name": "Mise à jour",
                    "value": new Date(land.channels.active.sort_update_date).toLocaleDateString("fr-FR"),
                    "inline": true
                },
                {
                    "name": "Catégorie",
                    "value": BlessedAPI.Utils.String.capitalize(build.category),
                    "inline": false
                },
                {
                    "name": "Langue",
                    "value": build.main_locale.toUpperCase(),
                    "inline": true
                },
                {
                    "name": "Publique",
                    "value": land.channels.active.is_enabled ? "Oui" : "Non",
                    "inline": true
                },
                musics[page]
            ])
    }
}