import { CommandInteraction } from "discord.js";
import { Discord, Slash, SlashGroup } from "discordx";
import { ItemsService } from "./items/items.service";
import { IMAGE_SIZE } from "./items/items.command";
import { H4AGamesService } from "./games/games.hamm4all.service";
import { FamiliesService } from "./families/families.service";
import { QuestService } from "./quests/quests.service";
import H4AFiles from "../core/api/hamm4all/files-api";
import { Item } from "@hamm4all/shared";
import { ScreenService } from "./screen/screen.service";

@Discord()
@SlashGroup({
    name: "random",
    description: "Commandes qui donnent des éléments aléatoires"
})
@SlashGroup("random")
export class RandomCommand {

    @Slash({
        name: "item",
        description: "Donne un objet aléatoire",
        dmPermission: true
    })
    async item(interaction: CommandInteraction) {
        await interaction.deferReply();
        let item: Item;
        let itemImage: Buffer | undefined;

        do {
            item = await ItemsService.getRandomItem();
            if (item.picture_id) {
                itemImage = await H4AFiles.getResizedFile(item.picture_id, IMAGE_SIZE, IMAGE_SIZE);
            }
        } while (!itemImage);

        const embed = await ItemsService.buildDisplay(item, item.name.fr);

        interaction.editReply({
            embeds: [embed],
            files: [{name: "image.png", attachment: itemImage}]
        })
    }

    @Slash({
        name: "level",
        description: "Donne un niveau aléatoire",
        dmPermission: true
    })
    async level(interaction: CommandInteraction) {
        await interaction.deferReply();

        let game;
        let level;

        // Potential fix for the bug where the level is undefined. If not fixed, needs to modify blessed-api instead
        while((!game) || (!level)) {
            game = await H4AGamesService.getRandomGame();
            try {
                level = await H4AGamesService.getRandomLevel(game.id);
            } catch(e) {
                level = undefined;
                continue;
            }
        }

        const replyInteraction = await new ScreenService(game.id, level.name).perform()
        return interaction.editReply(replyInteraction)
    }

    @Slash({
        name: "family",
        description: "Donne une famille aléatoire",
        dmPermission: true
    })
    async family(interaction: CommandInteraction) {
        await interaction.deferReply();

        const family = await FamiliesService.getRandomFamily();
        const image = await FamiliesService.buildFamilyImage(family);

        const embed = await FamiliesService.buildDisplay(family.name.fr);

        return interaction.editReply({
            embeds: [embed],
            files: [{name: "image.png", attachment: image}]
        })
    }

    @Slash({
        name: "quest",
        description: "Donne une quête aléatoire",
        dmPermission: true
    })
    async quest(interaction: CommandInteraction) {
        await interaction.deferReply();

        const quest = await QuestService.getRandomQuest();
        const image = await QuestService.buildQuestImage(quest);

        const embed = await QuestService.buildDisplay(quest.name.fr);

        return interaction.editReply({
            embeds: [embed],
            files: [{name: "image.png", attachment: image}]
        })
    }

}