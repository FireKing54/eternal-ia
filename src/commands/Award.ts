import { RoleModel } from "../database/models/Role";
import { UserModel } from "../database/models/User";
import { CommandInteraction, GuildMember, EmbedBuilder, Role, ApplicationCommandOptionType, Guild, userMention, roleMention } from "discord.js";
import { Discord, Guard, Guild as GuildGuard, Slash, SlashOption } from "discordx";
import { ENV } from "../Environment";
import BotError from "../errors/BotError";
import { NeedsPermission } from "../permissions/Permissions";

interface ReturnEmbed {
    "removedRoles": Array<Role>,
    "addedRole"?: Role,
    "titles": number,
    "metaTitle"?: Role,
    "rank": number
}

@Discord()
@Guard(NeedsPermission("MODERATOR"))
@GuildGuard(ENV.MAIN_GUILD)
export class Award {

    @Slash({
        "name": "award",
        "description": "Attribuer un rôle à un utilisateur",
        "dmPermission": false,
    })
    async award(
        @SlashOption({
            "name": "user",
            "description": "Utilisateur à qui attribuer le rôle",
            "required": true,
            "type": ApplicationCommandOptionType.User
        })
        user: GuildMember,
        @SlashOption({
            "name": "role",
            "description": "Rôle à attribuer",
            "required": true,
            "type": ApplicationCommandOptionType.Role
        })
        role: Role,
        interaction: CommandInteraction
    ) {
        const handled = await handleAward(interaction.guild!, user, role)
        const embed = buildAwardEmbed(user, handled)

        interaction.reply({
            "content": userMention(user.id),
            embeds: [embed]
        });
    }

}

export async function handleAward(guild: Guild, taggedUser: GuildMember, discordRole: Role): Promise<ReturnEmbed> {

    const returnEmbed: ReturnEmbed = {
        "removedRoles": [],
        "titles": 0,
        "metaTitle": discordRole,
        "rank": Infinity
    }

    const role = await RoleModel.findByDiscordRole(discordRole);

    if (!role) {
        throw new BotError(`Le rôle ${roleMention(discordRole.id)} donné ne compte pas pour les méta titres.`);
    }

    const { removedRoles, addedRole } = await role.awardRole(taggedUser);

    const nbRoles = await UserModel.countDiscordRoles(taggedUser);
    const {titles, metaTitle} = getNewMetatitle(taggedUser, nbRoles)
    returnEmbed.titles = titles;
    returnEmbed.metaTitle = metaTitle

    const rankings = await RoleModel.getDiscordServerRankings(guild)
    const rank = rankings.find(elem => elem.username === taggedUser.displayName)?.rank || Infinity;

    return {
        titles,
        metaTitle,
        rank,
        removedRoles,
        addedRole
    };
}

export function getCurrentMetatitle(taggedUser: GuildMember) {
    const metaTitlesRegex = /.+\(\d+\+\)$/g;
    const metaTitles = taggedUser.guild.roles.cache.filter(r => r.name.match(metaTitlesRegex) !== null);
    if (! metaTitles) {
        throw new BotError(`Aucun méta-titre n'a été trouvé sur le serveur. Rappel : les méta titres doivent correspondre à l'expression régulière ${metaTitlesRegex}`);
    }

    for (const metaTitle of metaTitles.values()) {
        if (taggedUser.roles.cache.find(r => r.id === metaTitle.id)) {
            return metaTitle;
        }
    }
    return undefined;
}

export function getNewMetatitle(taggedUser: GuildMember, nbRoles: number): {titles: number, metaTitle?: Role} {
    const metaTitlesRegex = /.+\(\d+\+\)$/g;
    const metaTitles = taggedUser.guild.roles.cache.filter(r => r.name.match(metaTitlesRegex) !== null);
    
    if (! metaTitles) {
        throw new BotError(`Aucun méta-titre n'a été trouvé sur le serveur. Rappel : les méta titres doivent correspondre à l'expression régulière ${metaTitlesRegex}`);
    }

    const metaList: Array<{meta: Role, threshold: number}> = [];

    for (const metaTitle of metaTitles.values()) {
        if (taggedUser.roles.cache.find(r => r.id === metaTitle.id)) {
            taggedUser.roles.remove(metaTitle);
        }
        const beginning = metaTitle.name.lastIndexOf("(");
        const end = metaTitle.name.lastIndexOf("+)");
        const threshold = parseInt(metaTitle.name.slice(beginning + 1, end));
        metaList.push({meta: metaTitle, threshold: threshold});
    }

    metaList.sort((a: any, b: any) => b.threshold - a.threshold);

    let metaTitleGiven = undefined
    for(const meta of metaList) {
        if (meta.threshold <= nbRoles) {
            taggedUser.roles.add(meta.meta);
            metaTitleGiven = meta.meta;
            taggedUser.roles.add(meta.meta);
            break;
        }
    }

    return {
        "titles": nbRoles,
        "metaTitle": metaTitleGiven
    };
}

export function buildAwardEmbed(user: GuildMember, embedReturn: ReturnEmbed): EmbedBuilder {

    const {addedRole, removedRoles, titles, metaTitle} = embedReturn;

    let titlesString = "n'as **aucun titre**";
    if (titles === 1) titlesString = "as **1 titre**"
    else if (titles > 1) titlesString = `as **${titles} titres**`

    const removed = `On t'a retiré le(s) rôle(s) ${removedRoles.map(x => roleMention(x.id)).join(",")}`;

    const embed = new EmbedBuilder()
        .setAuthor({
            "name": user.displayName
        })
        .setImage(user.avatarURL() ?? user.user.avatarURL() ?? user.user.defaultAvatarURL)
    
    if (removedRoles.length > 0) {
        embed.addFields([
            {
                "name": "\u200b",
                "value": removed
            }
        ]);
    }

    if (addedRole) {
        embed.addFields([
            {
                "name": "\u200b",
                "value": `Tu as reçu le rôle ${roleMention(addedRole.id)}`
            }
        ])
    }

    if (!metaTitle) {
        embed.addFields([
            {
                "name": "\u200b",
                "value": `Tu n'as aucun titre`
            }
        ])
        return embed
    }

    embed.addFields([
        {
            "name": "\u200b",
            "value": `Tu ${titlesString}, et tu es ${roleMention(metaTitle.id)}`
        },
        {
            "name": "\u200b",
            "value": `Tu es classé **n°${embedReturn.rank}** du serveur`
        }
    ])

    return embed
}