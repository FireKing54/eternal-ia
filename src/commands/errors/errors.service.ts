import { StoredError } from "./types/stored-error.types";
import { v4 } from "uuid"

class ErrorsService {

    private readonly errors: Map<string, StoredError>;

    constructor() {
        this.errors = new Map();
    }

    public registerError(error: StoredError): string {
        const id = v4();
        this.errors.set(id, error);
        return id;
    }

    public getError(name: string) {
        return this.errors.get(name);
    }
}

export default new ErrorsService();