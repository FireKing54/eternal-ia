import { Game, Level } from "@hamm4all/shared";
import H4AFiles from "../../core/api/hamm4all/files-api";
import H4AGames from "../../core/api/hamm4all/games-api";
import H4ALevels from "../../core/api/hamm4all/levels-api";
import { EmbedBuilder, InteractionReplyOptions } from "discord.js";

export class ScreenService {
    private level!: Level;
    private game!: Game;

    constructor(
        private readonly gameID: number,
        private readonly levelName: string
    ) {}

    async perform(): Promise<InteractionReplyOptions> {
        this.game = await H4AGames.getGame(this.gameID);
        this.level = await H4ALevels.getLevel(this.gameID, this.levelName);

        return {
            "embeds": [await this.getResultEmbed()]
        }
    }

    async getResultEmbed() {
        const levelLastUpdateDate = new Date(this.level.updated_at);

        const embed = new EmbedBuilder()
            .setTitle(`Niveau ${this.levelName}`)
            .setDescription(`Modifié le ${levelLastUpdateDate.toLocaleDateString("fr-FR")} par ${this.level.updated_by.name}`)
            .addFields([
                {
                    "name": "Jeu",
                    "value": `[${this.game.name}](${H4AGames.getGameURL(this.gameID)})`
                },
                {
                    "name": "Niveau",
                    "value": `[Niveau ${this.level.name}](${H4ALevels.getLevelURL(this.gameID, this.levelName)})`
                }
            ])
            .setTimestamp(levelLastUpdateDate)
        
        if (this.level.mainScreenshotID) {
            embed.setImage(H4AFiles.getURL(this.level.mainScreenshotID))
        }

        return embed;
    }

    getLevelURL() {
        return `https://hamm4all.net/games/${this.gameID}/levels/${this.levelName}`
    }
}