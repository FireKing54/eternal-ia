import { Family, OldFamilyItem } from "@hamm4all/shared";
import { ItemsService } from "../../commands/items/items.service";
import H4AFamilies from "../../core/api/hamm4all/families-api";
import H4AFiles from "../../core/api/hamm4all/files-api";
import H4AItems from "../../core/api/hamm4all/items-api";
import { EmbedBuilder } from "discord.js";

export class FamiliesService {

    static async getFamily(familyName: string) {
        const family = await H4AFamilies.getFamilyByName(familyName);
        if (! family) {
            throw new Error("Family not found");
        }
        
        return family;
    }

    static async buildFamilyImage(family: Family): Promise<Buffer> {
        const enhancedItems: Array<OldFamilyItem & {image?: Buffer}> = []
        
        for(const item of family.items) {
            const familyItem = await H4AItems.getItemByGameIDAndInGameID(31, item.id);
            if (! familyItem) {
                throw new Error("Item not found");
            }

            if (!item.img || !familyItem.picture_id) {
                enhancedItems.push(item);
                continue;
            }

            const itemImage = await H4AFiles.getResizedFile(familyItem.picture_id, 50, 50);

            enhancedItems.push({
                ...item,
                "image": itemImage
            })
        }

        // Build the image
        return ItemsService.getGroupItemsImage(enhancedItems.map((item) => {
            return {
                "img": item.image,
            }
        }), false);
    }

    static async getRandomFamily() {
        const families = await H4AFamilies.getFamilies();
        const randomFamily = families[Math.floor(Math.random() * families.length)];
        return randomFamily;
    }

    static async buildDisplay(askedName: string) {
        return new EmbedBuilder()
            .setTitle(askedName)
            .setColor([250, 0, 250])
            .setImage('attachment://image.png')
        }

}