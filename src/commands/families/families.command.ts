import { Discord, Slash, SlashOption } from "discordx";
import { FamiliesService } from "./families.service";
import { ApplicationCommandOptionType, AutocompleteInteraction, CommandInteraction } from "discord.js";
import H4AFamilies from "../../core/api/hamm4all/families-api";

@Discord()
export class FamiliesCommand {
    
        @Slash({
            "name": "family",
            "description": "Donne des informations sur la famille recherchée"
        })
        async familyPreProcess(
            @SlashOption({
                "name": "name",
                "description": "Famille recherchée",
                "required": true,
                "type": ApplicationCommandOptionType.String,
                "autocomplete": async (inter: AutocompleteInteraction) => {
                    const input = inter.options.getFocused();
                    await inter.respond(await H4AFamilies.autoCompleteFamilies(input));  
                }
            })
            familyName: string,
            interaction: CommandInteraction
        ) {
            await interaction.deferReply()

            // Remove the language prefix
            familyName = familyName.slice(5);

            const family = await FamiliesService.getFamily(familyName);

            const familyImage = await FamiliesService.buildFamilyImage(family);
            const embed = await FamiliesService.buildDisplay(familyName);

            interaction.editReply({
                embeds: [embed],
                files: [{name: "image.png", attachment: familyImage}]
            })
        }
}