import { ENV } from "../../Environment";
import { CommandInteraction } from "discord.js";
import { Discord, Guild, Slash } from "discordx";
import { AdminService } from "./admin.service";
import { buildTable } from "../../modules/Display";
import { Grade, getPermissionName } from "../../permissions/Permissions";
import { PaginationService } from "../../modules/pagination/pagination.service";

@Discord()
export class AdminCommand {

    @Slash({
        "name": "admin",
        "description": "Liste les personnes qui ont des permissions sur le bot"
    })
    @Guild(ENV.MAIN_GUILD)
    async handle(interaction: CommandInteraction) {
        await interaction.deferReply();

        const moderatorsOrMore = await AdminService.getAdmins();

        const tables = buildTable({
            "title": "Modérateurs et administrateurs",
            "headers": ["Utilisateur", "Grade"],
            "data": moderatorsOrMore.map(user => {
                const guildMember = interaction.guild?.members.cache.get(user.id);
                return [
                    guildMember?.displayName ?? guildMember?.user.username ?? user.id,
                    getPermissionName(user.role as Grade)
                ]
            })
        })

        const pagination = await PaginationService.addPagination({
            "messageId": interaction.id,
            "pagination": {
                "currentPageIndex": 0,
                "pages": tables.map((table) => {
                    return {
                        "content": table,
                    }
                })
            }
        });

        return interaction.editReply(pagination);
    }
}