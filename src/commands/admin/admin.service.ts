import { UserModel } from "../../database/models/User";
import { MoreThanOrEqual } from "typeorm";

export class AdminService {

    static getAdmins(): Promise<UserModel[]> {
        return UserModel.find({
            "where": {
                "role": MoreThanOrEqual(2) // 2 is the grade of VIPs.
            },
            "order": {
                "role": "DESC"
            }
        });
    }

}