
import { ApplicationCommandOptionType, CommandInteraction, EmbedBuilder } from "discord.js";
import { Discord, Slash, SlashChoice, SlashOption } from "discordx";
import { BotException } from "../errors/bot.exception";

type FactLevel = "beginner" | "regular" | "advanced";

const factLevelInfo: {[key in FactLevel]: {color: [number, number, number], name: string, description: string}} = {
    "beginner": {
        "color": [0, 255, 127],
        "name": "Débutant",
        "description": "Un fait que tous les nouveaux joueurs devraient apprendre"
    },
    "regular": {
        "color": [255, 255, 127],
        "name": "Intermédiaire",
        "description": "Un fait que les joueurs actifs connaissent"
    },
    "advanced": {
        "color": [255, 127, 0],
        "name": "Avancé",
        "description": "Un fait généralement connu que par les vétérans du jeu"
    }
}

@Discord()
export class HammerfactCommand {

    @Slash({
        "name": "hammerfact",
        "description": "Renvoie un fait, une histoire ou une anecdote aléatoire sur le jeu"
    })
    async hammerfact(
        @SlashChoice(...Object.entries(factLevelInfo).map(([name, value]) => ({name: value.name, value: name})))
        @SlashOption({
            "name": "level",
            "description": "Niveau du fait",
            "required": false,
            "type": ApplicationCommandOptionType.String,
        })
        factLevel: string | undefined,
        @SlashOption({
            "name": "number",
            "description": "Numéro du fait",
            "required": false,
            "type": ApplicationCommandOptionType.Integer,
            "minValue": 0
        })
        num: number | undefined,
        interaction: CommandInteraction
    ) {
        await interaction.deferReply();

        let drawableFacts = [...facts];

        if (factLevel) {
            drawableFacts = drawableFacts.filter(f => f.level === factLevel);
        }

        const len = drawableFacts.length;
        const index = num ? num - 1 : Math.floor(Math.random() * len);

        if (index >= len) {
            throw new BotException("Le numéro du fait est trop grand", `Essaye de donner un numéro de fait entre 0 et ${len - 1}.`);
        }

        const {level, fact} = drawableFacts[index];
        const {color, name, description} = factLevelInfo[level]

        const embed = new EmbedBuilder()
            .setColor(color)
            .setTitle(`Hammerfact (n°${index + 1}/${len})`)
            .setDescription(`Fait niveau **${name}**`)
            .addFields([
                {
                    "name": "\u200b",
                    "value": fact
                }
            ])
            .setFooter({
                "text": description
            })

        interaction.editReply({
            "embeds": [embed]
        });


    }
}

interface Fact {
    level: FactLevel,
    fact: string
}

const facts: Array<Fact> = [
    {
        "fact": "Quand il y a plusieurs fraises dans un niveau, la fraise qui a la balle est **toujours la même**. De plus, celle-ci est **la première de l'ordre**",
        "level": "advanced"
    },
    {
        "fact": "En 2006, aux débuts du jeu, il était possible de **glitcher au niveau 103** pour passer au travers d'un mur de plateforme. On avait alors accès à quelques niveaux supplémentaires, connus sous le nom de `Temple des Epreuves`",
        "level": "regular"
    },
    {
        "fact": "Il est possible de jouer au temple des épreuves sur **Hammerfest Deluxe Edition** : il suffit de prendre la **porte alternative** au **niveau 102**, et glitcher au niveau 103 à travers la plateforme",
        "level": "regular"
    },
    {
        "fact": "[Vacerne](http://www.hammerfest.fr/user.html/78596) est le seul joueur a avoir sur sa fiche le niveau **115** comme niveau maximum. Il l'a accompli quand le *temple des épreuves* était encore accessible, en prenant un parapluie vert au **niveau 112**",
        "level": "advanced"
    },
    {
        "fact": "[Eternoël](https://eternalfest.net/games/d27fe54a-afe3-451f-b85e-efac6e895f6f) fut la première contrée publique d'Eternalfest. Elle est sortie le jour de Noël 2018.",
        "level": "beginner"
    },
    {
        "fact": "Le [Temple Oublié](https://eternalfest.net/games/9f3bdd58-59ab-40f2-ad78-2c81776aca08) est la plus ancienne grosse contrée créée par quelqu'un n'appartenant pas à la Motion Twin. Il a été créé en **2009** par **Gary520**",
        "level": "regular"
    },
    {
        "fact": "La Canne de Bobble était au départ un **coeff 5**. Elle a ensuite été placée en coeff 7 (entre coeff 4 et coeff 5), une catégorie créée spécialement pour elle, pour baisser la difficulté de la quête **Goût Raffiné**",
        "level": "advanced"
    },
    {
        "fact": "La plus grosse somme de points théoriquement obtenable par big bang et ordre dans le jeu officiel s'élève à 844000 points. Cela peut se faire en multi-coopératif (et plus difficilement en solo) au niveau [54.12.1D.9.3.14](https://hamm4all.net/lands/31/levels/54.12.1D.9.3.14). Les premiers à l'avoir fait en vidéo sont **Zryx** et **fardur23**",
        "level": "regular"
    },
    {
        "fact": "Très rarement, faire CRISTAL, ou prendre une perle au moment exact où une framboise se téléporte envoie ses pépins de façon uniforme dans toutes les directions.",
        "level": "advanced"
    },
    {
        "fact": "La bouée canard donne **250 points pour George** (la boule de feu) si elle est déjà présente au moment de prendre l'objet",
        "level": "advanced"
    },
    {
        "fact": "Aux tous débuts de jeu, les **pierres du changement** de la **grosse pé-perle** valaient **19 200 points**. Leur valeur ayant par la suite été jugée trop élevée (car des joueurs moyens pouvaient alors accéder au panthéon uniquement grâce aux points procurés par la perle), elle a été diminuée à **10 000 points**",
        "level": "advanced"
    },
    {
        "fact": "Prendre des baskets IcePump quand on joue en Tornade + Cauchemar **ralentit** légèrement Igor. En effet, les baskets fixent la vitesse d'Igor à 1.5, alors que Tornade + Cauchemar la fixe à 1.6",
        "level": "regular"
    },
    {
        "fact": "Il existe un secret dans l'apprentissage officiel : Poser une bombe directement à droite contre le mur en entrant dans le niveau, ouvre un raccourci vers la deuxième partie de l'apprentissage",
        "level": "beginner"
    },
    {
        "fact": "Casser le seau avec une bombe dans le niveau 0 envoie directement au niveau 10. Cela est aussi valable dans le **Temple Oublié**",
        "level": "beginner"
    },
    {
        "fact": "Dans le code du jeu, la pastèque est appelée `Whale`, soit `Baleine` en anglais",
        "level": "advanced"
    },
    {
        "fact": "Le casque de Volleyfest est un objet qui ne peut être obtenu dans l'aventure, mais qui constitue le 353ème objet. Il est donc impossible de compléter un frigo à 100%",
        "level": "regular"
    },
    {
        "fact": "Au [niveau 86.1](https://hamm4all.net/lands/31/levels/86.1) se trouve une porte qui peut être ouverte grâce à la **Clé d'Inversion**. Mais cette clé n'a jamais été implémentée dans le jeu, la dimension 86.1 n'existe donc pas",
        "level": "regular"
    },
    {
        "fact": "Il est possible de sortir du **niveau 103** en mod **Explosifs Instables**. On peut aller jusqu'au **niveau 106**, où on est définitivement coincé",
        "level": "regular"
    },
    {
        "fact": "Le **Joyau d'Ankhel** donne **une vie supplémentaire** quand il est ramassé",
        "level": "regular"
    },
    {
        "fact": "En mars 2017, un bug interne sur le site officiel a figé le classement. Il n'était donc plus possible de passer au panthéon. **BlessedRacc** est donc resté 1er du premier étage sans passer au panthéon pendant plus de **3 ans**",
        "level": "regular"
    },
    {
        "fact": "Depuis le bug du classement, **Erneraude** est passé premier de l'étage 4, avec plus de 10 millions de points. Ce score est longtemps resté le plus élevé de tous les étages confondus, avant que **Pierre839** lui passe devant dans l'étage 1",
        "level": "advanced"
    },
    {
        "fact": "Depuis mars 2017, prendre la **Neige-ô-glycérine** pour la première fois **bloque la complétion des autres quêtes**. Ainsi, quelqu'un qui prend la NôG pendant cette période alors qu'il a des quêtes non terminées ne pourra **jamais les compléter**, même s'il réunit tous les objets pour. Egalement, le bug de la NôG **fige le score maximum d'un joueur**. De cette façon, un joueur qui aurait amélioré son meilleur score ne verrait pas son nouveau score sauvegardé sur sa fiche publique",
        "level": "advanced"
    },
    {
        "fact": "Casser la carapace d'un litchi **casse** l'ordre, quoi qu'il arrive. Il existe cependant des moyens de faire des ordres avec des litchis, notamment au [niveau 61](https://hamm4all.net/lands/31/levels/61), où un **surfing** du citron peut étourdir le litchi en le poussant dans le vide dans enlever sa carapace.",
        "level": "regular"
    },
    {
        "fact": "Faire CRISTAL dans l'ordre permet un **bonus de 150 000 points**. C'est quelque chose à prendre en compte en partie à points, pour les plus expérimentés d'entre nous",
        "level": "beginner"
    },
    {
        "fact": "Plus on a de **vies**, plus la **boule de feu arrive vite**. Cette limite n'est pas capée (sauf dans certaines contrées Eternalfest). La vitesse d'apparition de la boule de feu est au minimum quand on possède 3 vies, ou moins",
        "level": "regular"
    },
    {
        "fact": "Il y a 4 objets dits \interdits\". Ces objets étaient bannis du forum Hammerfest car étant buggés, ils pouvaient être utilisés sur le forum même si on ne les avait pas débloqués. Ces objets sont indisponibles en jeu",
        "level": "regular"
    },
    {
        "fact": "Tous les objets de la famille `Bâtons de Joie` (donc tous les pads) sont notés **coeffs 6** dans le code du jeu. Mais comme leur famille n'est jamais débloquée, ils ne peuvent pas apparaître naturellement en jeu, et sont donc équivalents à des coeffs 0",
        "level": "advanced"
    },
    {
        "fact": "Hammerfest est grandement inspiré du jeu **Bubble Bobble**. On retrouve beaucoup de référence à ce jeu disséminés aux quatre coins du puits.",
        "level": "regular"
    },
    {
        "fact": "En empruntant un chemin très spécifique, il est possible de prendre un nombre infini de fois l'objet qui apparaît au **niveau 33.?**. Le joueur **Globox** en a d'ailleurs profité pour débloquer son **Laxatif aux Amandes** en une seule partie",
        "level": "advanced"
    },
    {
        "fact": "La canne de Bobble du [niveau 33.0.2](https://hamm4all.net/lands/31/levels/33.0.2) est en réalité **double** : si on parvient à la prendre une fois, on peut la prendre une **deuxième** car elle réapparaît une fois",
        "level": "regular",
    },
    {
        "fact": "**Hammerfest** est une ville dans la vie réelle. Elle se situe au nord de la Norvège, et elle est connue pour être la ville la plus **septentrionale** du monde",
        "level": "regular"
    },
    {
        "fact": "Le **ig'or** au niveau 51.8 n'apparaît que si **on lag en entrant dans le niveau**. Cela est dû au fait que cet Ig'or apparaît si la frame 1 est la première frame du niveau. Cela implique donc lagger suffisamment en entrant dans le niveau pour passer la frame 0",
        "level": "regular"
    }
];
