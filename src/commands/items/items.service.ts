import { ItemInGroup } from "./types/items-group.types";
import { ItemGroupBuilder } from "./items-group.builder";
import { EmbedBuilder } from "discord.js";
import H4AItems from "../../core/api/hamm4all/items-api";
import H4AFiles from "../../core/api/hamm4all/files-api";
import { Item } from "@hamm4all/shared";

export class ItemsService {

    static async getImage(item: Item, size: number): Promise<Buffer | undefined> {
        if (!item.picture_id) {
            return undefined;
        }
        const resizedImage = await H4AFiles.getResizedFile(item.picture_id, size, size);
        return resizedImage;
    }

    static async getGroupItemsImage(items: Array<ItemInGroup>, displayAmount: boolean): Promise<Buffer> {
        return new ItemGroupBuilder()
            .setDisplayAmount(displayAmount)
            .setFont("Arial")
            .setFontColor("white")
            .setMissingItemColor("yellow")
            .setPadding(6)
            .setSizeOfItem(50)
            .setSizeOfText(22)
            .setMaxItemsPerRow(10)
            .build(items);
    }

    static async getRandomItem() {
        const items = await H4AItems.getItems();
        const randomItem = items[Math.floor(Math.random() * items.length)];
        return randomItem;
    }

    static async buildDisplay(item: Item, askedName: string) {
        return new EmbedBuilder()
            .setTitle(askedName)
            .setColor([250, 0, 250])
            .setDescription(item.effect ? ItemsService.clearMarkdownTags(item.effect) : "Aucun effet")
            .addFields([
                {
                    "name": "Coefficient",
                    "value": `${item.coefficient}`,
                    "inline": true
                },
                {
                    "name": "Valeur",
                    "value": item.value ? `${item.value}` : "0",
                    "inline": true
                },
                {
                    "name": "Jeu",
                    "value": item.game.name,
                    "inline": true
                }
            ])
            .setImage('attachment://image.png')
    }

    /**
     * A function that clears the HTML tags of a string
     * @param effet The string to clear
     * @returns 
     */
    static clearHTMLTags(str: string): string {
        return str.replace(/(<([^>]+)>)/gi, "");
    }

    /**
     * Clears Markdown tags from a string : double stars, double underscores, links and images
     * @param str 
     * @returns 
     */
    static clearMarkdownTags(str: string): string {
        return str.replace(/\*\*/g, "")
        .replace(/__/g, "")
        .replace(/!?\[.*?\]\(.*?\)/g, "")
    }
}