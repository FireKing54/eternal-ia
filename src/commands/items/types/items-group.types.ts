export interface ItemInGroup {
    img?: Buffer;
    amount?: number;
}

export type ItemGroupColor = string | CanvasGradient | CanvasPattern;