import PermissionError from "../errors/PermissionError";
import { ApplicationCommandPermissions, ApplicationCommandPermissionType, CommandInteraction, Guild } from "discord.js";
import { ApplicationCommandMixin, ArgsOf, GuardFunction, SimpleCommandMessage } from "discordx";
import { MoreThanOrEqual } from "typeorm";
import { UserModel } from "../database/models/User";

export type UserPermission = "DEFAULT" | "VIP" | "MODERATOR" | "ADMINISTRATOR" | "OWNER"

export type Grade = 1 | 2 | 3 | 4 | 5;

export enum UserGrade {
    DEFAULT = 1,
    VIP = 2,
    MODERATOR = 3,
    ADMINISTRATOR = 4,
    OWNER = 5
}

const PERMISSION_NAMES = ["Utilisateur", "VIP", "Modérateur", "Administrateur", "Créateur"]

export function getPermissionName(grade: Grade): string {
    return PERMISSION_NAMES[grade - 1];
}

interface PermissionEnum {
    "DEFAULT": PermissionEnumValue,
    "VIP": PermissionEnumValue,
    "MODERATOR": PermissionEnumValue,
    "ADMINISTRATOR": PermissionEnumValue,
    "OWNER": PermissionEnumValue
}

interface PermissionEnumValue {
    "name": string,
    "value": Grade
}

export const PERMISSIONS: PermissionEnum = {
    "DEFAULT": {
        "name": "Utilisateur",
        "value": 1
    },
    "VIP": {
        "name": "VIP",
        "value": 2
    },
    "MODERATOR": {
        "name": "Modérateur",
        "value": 3
    },
    "ADMINISTRATOR": {
        "name": "Administrateur",
        "value": 4
    },
    "OWNER": {
        "name": "Créateur",
        "value": 5
    },
}

export function toGrade(roleName: UserPermission) {
    return PERMISSIONS[roleName].value;
}

function UserRole(roleName: UserPermission) {

    let role: number = 0;

    switch(roleName) {
        case "DEFAULT":
            role = 1;
            break;
        case "VIP":
            role = 2;
            break;
        case "MODERATOR":
            role = 3;
            break;
        case "ADMINISTRATOR":
            role = 4;
            break;
        case "OWNER":
            role = 5;
            break;
    }

    return async (_guild: Guild, _cmd: ApplicationCommandMixin | SimpleCommandMessage): Promise<ApplicationCommandPermissions[]> => {

        const users = await UserModel.find({
            "where": {
                "role": MoreThanOrEqual(role)
            }
        });

        return users.map(user => ({
            "id": user.id,
            "permission": true,
            "type": ApplicationCommandPermissionType.User
        }))
    }
}

function NeedsPermission(grade: UserPermission) {
    const guard: GuardFunction<
        ArgsOf<"messageCreate"> | CommandInteraction
    > = async (arg, client, next) => {
        const argObj = arg instanceof Array ? arg[0] : arg;
        if (argObj instanceof CommandInteraction) {
            const user = await UserModel.findOne({
                "where": {
                    "id": argObj.user.id
                }
            }) ?? {role: 1}

            if (user.role >= toGrade(grade)) {
                await next();
            }
            else {
                throw new PermissionError(user.role as Grade, toGrade(grade))
            }
        }
        else {
            await next();
        }
    }   

    return guard;
}

export async function hasPermission(userID: string, grade: UserPermission): Promise<boolean> {
    const user = await UserModel.findOne({
        "where": {
            "id": userID
        }
    });

    if (user) {
        return user.role >= toGrade(grade);
    }
    else {
        return false;
    }
}

export async function getGrade(userID: string): Promise<Grade> {
    const grade = await UserModel.findOne({
        "where": {
            "id": userID
        }
    });

    return (grade?.role as Grade) ?? 1;
}

export default PERMISSIONS
export {
    UserRole,
    NeedsPermission,
}