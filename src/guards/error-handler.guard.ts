import { logger } from "../Log";
import { ButtonInteraction, ChannelSelectMenuInteraction, CommandInteraction, ContextMenuCommandInteraction, MentionableSelectMenuInteraction, ModalSubmitInteraction, RoleSelectMenuInteraction, StringSelectMenuInteraction, UserSelectMenuInteraction, Message, MessageComponentInteraction, InteractionReplyOptions, ChatInputCommandInteraction } from "discord.js";
import { GuardFunction, ArgsOf } from "discordx";
import { EmbedService } from "../embeds/embed.service";
import BotError from "../errors/BotError";
import { BotException } from "../errors/bot.exception";

export const ErrorHandler: GuardFunction<
    | ArgsOf<"messageCreate">
    | ButtonInteraction
    | ChannelSelectMenuInteraction
    | CommandInteraction
    | ContextMenuCommandInteraction
    | MentionableSelectMenuInteraction
    | ModalSubmitInteraction
    | RoleSelectMenuInteraction
    | StringSelectMenuInteraction
    | UserSelectMenuInteraction
> = async (arg, client, next, guardData) => {
    const interaction = arg instanceof Array ? arg[0] : arg;
    const user = interaction instanceof Message ? interaction.author : interaction.user

    try {
        await next().catch((error) => {throw error});
        logger.info(`Interaction \`${interaction.id}\` from \`${user.username}\` executed successfully!`)
    }
    catch (error: any) {
        if (!interaction) {
            return;
        }

        logger.error(`Interaction \`${interaction.id}\` from \`${user.username}\` returned an error!`)

        if (! (
            interaction instanceof MessageComponentInteraction ||
            interaction instanceof ButtonInteraction ||
            interaction instanceof CommandInteraction ||
            interaction instanceof Message
        )) {
            return;
        }

        // Error building
        let fixMessage = undefined
        if (error instanceof BotError) {
            fixMessage = error.debugInfo
        }
        else if (error instanceof BotException) {
            fixMessage = error.fix
        }
        
        const errorMessage = error.message ?? error

        logger.error(error?.stack ?? errorMessage ?? error)

        const response: InteractionReplyOptions = {
            "embeds": [
                EmbedService.buildException({
                    "message": errorMessage,
                    "fix": fixMessage
                })
            ],
            "ephemeral": true
        }

        // If there is a "editReply" method, it means that the interaction is editable
        if (
            (interaction instanceof MessageComponentInteraction ||
            interaction instanceof ButtonInteraction ||
            interaction instanceof ChatInputCommandInteraction) &&
            interaction.deferred
        ) {
            return interaction.editReply(response)
        }
        
        try {
            return (interaction as any).reply(response);
        }
        catch(e) {
            return
        }
    }
};