import BotError from "./BotError";

class UnknownError extends BotError {
    constructor() {
        super("Erreur Inconnue. Cette erreur ne devrait pas se produire.")
    }
}

export default UnknownError