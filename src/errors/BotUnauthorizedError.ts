import BotError from "./BotError";

class BotUnauthorizedError extends BotError {
    constructor() {
        super("Je n'ai pas l'autorisation d'éxecuter cette commande. Regarde dans les permissions du serveur pour me donner les droits.")
    }
}

export default BotUnauthorizedError