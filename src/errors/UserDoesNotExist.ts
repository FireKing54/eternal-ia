import BotError from "./BotError";

export default class UserDoesNotExist extends BotError {
    constructor(name: string) {
        super(`L'utilisateur qui a le nom ${name} n'existe pas}`)
    }
}