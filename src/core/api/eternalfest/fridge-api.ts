import axios from "axios"
import { ENV } from "../../../Environment"

export interface EternalfestFridge {
    [key: number]: number
}

export default class EFFridge {
    static getFridge(gameID: string, userID: string) {
        const request = axios.request<EternalfestFridge>({
            "baseURL": "https://eternalfest.net/api/v1",
            "url": `/users/${userID}/profiles/${gameID}/items`,
            "headers": {
                "Cookie": `ef_sid=${ENV.ETERNALFEST_AUTH_TOKEN}`
            }
        })
        .then((response) => response.data)

        return request
    }
}