import { APIEndpointGET } from "@hamm4all/shared";
import NodeCache from "node-cache";
import callH4AAPI from "./hamm4all-api";

const H4A_CACHE = new NodeCache()

export function getIfInCache<Result>(key: string) {
    return H4A_CACHE.get<Result>(key)
}

interface ApplyAPICachingParams<
    Params extends {},
    Query extends {},
    Response extends {}
> {
    endpoint: APIEndpointGET<Params, Query, Response>;
    params: Params;
    query: Query;
    timeToLive?: number;
    isFile?: boolean;
}

export async function applyAPICaching<
    Params extends {},
    Query extends {},
    Response extends {}
>(params: Readonly<ApplyAPICachingParams<Params, Query, Response>>): Promise<Response>{
    const key = JSON.stringify(params.endpoint.getQueryKeys(params.params, params.query))
    const cached = getIfInCache<Response | undefined>(key)

    if (cached) {
        return cached
    }

    const response = await params.endpoint.call((props) => callH4AAPI<Response>({...props, isFile: params.isFile}), params.params, params.query)
    H4A_CACHE.set<Response>(key, response, params.timeToLive ?? 0)

    return response
}

export default H4A_CACHE;