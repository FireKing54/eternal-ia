import { H4A_API } from "@hamm4all/shared";
import { applyAPICaching } from "../hamm4all-cache";
import BlessedAPI from "blessed-api";
import { H4A_BASE_URL } from "../hamm4all-api";
import path from "path";

export default class H4AFiles {
    static async getFile(id: string) {
        return applyAPICaching({
            "endpoint": H4A_API.v1.files.getRaw,
            "params": {id},
            "query": {},
            "timeToLive": 60 * 60 * 24 * 30 * 12,
            "isFile": true,
        })
    }

    /**
     * This probably won't be used anymore when family and quest items are in Item format
     */
    static async getAsset(assetPath: string) {
        return fetch(path.join(H4A_BASE_URL, assetPath))
            .then(res => res.blob())
    }

    static async getResizedFile(id: string, width: number, height: number) {
        const rawFile = await this.getFile(id);
        const file = BlessedAPI.Utils.Image.resize(rawFile, {"width": width, "height": height})
        return file;
    }

    static getURL(id: string) {
        return H4A_BASE_URL + H4A_API.v1.files.getRaw.getParsedUri({"id": id})
    }
}