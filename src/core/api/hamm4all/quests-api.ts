import { H4A_API, Quest } from "@hamm4all/shared";
import { ApplicationCommandOptionChoiceData } from "discord.js";
import { normalMatch } from "../../../match/matchers";
import { applyAPICaching } from "../hamm4all-cache";

export default class H4AQuests {
    static async getQuests() {
        return applyAPICaching({
            "endpoint": H4A_API.v1.quests.list,
            "params": {},
            "query": {},
            "timeToLive": 60 * 60 * 24 * 30
        })
    }

    static async getQuestsByCondition(condition: (quest: Quest) => boolean) {
        const quests = await this.getQuests()
        return quests.filter(condition);
    }

    static async getQuestByName(name: string) {
        const quests = await this.getQuests();
        return quests.find(quest => quest.name.fr === name || quest.name.en === name || quest.name.es === name);
    }

    static async autoCompleteQuests(input: string) {
        const quests = await this.getQuests();
        const mapped: Array<ApplicationCommandOptionChoiceData> = quests
            .flatMap(quest => [`[FR] ${quest.name.fr}`, `[EN] ${quest.name.en}`, `[ES] ${quest.name.es}`])
            .filter(quest => normalMatch(quest, input))
            .map(quest => ({
                "name": quest,
                "value": quest
            }))
        return mapped.slice(0, 24);
    }

}