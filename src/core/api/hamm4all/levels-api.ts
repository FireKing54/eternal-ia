import { H4A_API, Level } from "@hamm4all/shared";
import { applyAPICaching } from "../hamm4all-cache";
import { ApplicationCommandOptionChoiceData } from "discord.js";
import { normalMatch } from "../../../match/matchers";
import BotError from "../../../errors/BotError";


export default class H4ALevels {
    static async getLevels(gameID: number) {
        return applyAPICaching({
            "endpoint": H4A_API.v1.games.levels,
            "params": { "gameID": gameID },
            "query": {},
            "timeToLive": 60 * 60
        })
    }

    static async getLevel(gameID: number, levelName: string) {
        const levels = await H4ALevels.getLevels(gameID)
        const level = levels.find(l => l.name === levelName);
        return new Promise<Level>((resolve, reject) => {
            if (level) {
                resolve(level)
            } else {
                reject(new BotError("Level not found"))
            }
        })
    }

    static async autoCompleteLevels(level: string, gameID: number) {
        const game = await H4ALevels.getLevels(gameID)

        if (game.length === 0) return [];

        const mapped: Array<ApplicationCommandOptionChoiceData> = game
            .filter(l => normalMatch(l.name, level))
            .map(l => ({"name": l.name, "value": l.name}))

        return mapped.slice(0, 24);
    }

    static getLevelURL(gameID: number, levelName: string) {
        return `https://hamm4all.net/games/${gameID}/levels/${levelName}`
    }
}