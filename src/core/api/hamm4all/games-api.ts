import { H4A_API } from "@hamm4all/shared";
import { applyAPICaching } from "../hamm4all-cache";
import { ApplicationCommandOptionChoiceData } from "discord.js";
import { normalMatch } from "../../../match/matchers";

export default class H4AGames {
    static getGames() {
        return applyAPICaching({
            "endpoint": H4A_API.v1.games.list,
            "params": {},
            "query": {},
            "timeToLive": 60 * 60
        })
    }

    static getGame(gameID: number) {
        return applyAPICaching({
            "endpoint": H4A_API.v1.games.get,
            "params": { "gameID": gameID },
            "query": {},
            "timeToLive": 60 * 60
        })
    }

    static async autoCompleteGames(input: string) {
        const games = await H4AGames.getGames()

        const filtered: Array<ApplicationCommandOptionChoiceData> = 
            games
                .filter(game => normalMatch(game.name, input))
                .map(game => ({
                    "name": game.name,
                    "value": game.id.toString()
                }))

        return filtered.slice(0, 24);
    }

    static getGameURL(gameID: number) {
        return `https://hamm4all.net/games/${gameID}`
    }
}