import "dotenv/config";
import { z } from "zod";

const validator = z.object({
    "APP_ENV": z.union([z.literal("DEV"), z.literal("PRODUCTION")]),
    "BOT_TOKEN": z.string().min(1),
    "MAIN_GUILD": z.string().min(1).regex(/\d+/g),
    "NDJ_REFRESH_CRON": z.string().min(1).refine(x => /(((\d+,)+\d+|(\d+(\/|-)\d+)|\d+|\*) ?){5,7}/g.test(x)),

    "DB_USER": z.string().min(1).optional().default("postgres"),
    "DB_HOST": z.string().min(1).optional().default("localhost"),
    "DB_NAME": z.string().min(1).optional().default("eternal-ia"),
    "DB_PASSWORD": z.string().min(1).optional().default("postgres"),
    "DB_PORT": z.string().min(1).optional().default("5432").transform(x => parseInt(x, 10)).refine(x => ! isNaN(x)),

    "ETERNALFEST_AUTH_TOKEN": z.string().uuid("Le token Eternalfest d'Authentification n'est pas valide"),

    "QUIZ_CHANNEL": z.string().min(1).regex(/\d+/g),
    "ROLE_CLAIM_CHANNEL": z.string().min(1).regex(/\d+/g)
});

const ENV = validator.parse(process.env);

export {
    ENV
}