import {createLogger, format, transports} from "winston"
import { ENV } from "./Environment"

const loggerPrintFormat = format.printf(p => `[${p["timestamp"]}] ${p.level}: ${p.message}`);
const profilerPrintFormat = format.printf(p => `[${p["timestamp"]}] Command "${p.message}", ${p["durationMs"]}ms`)

const logger = createLogger({
    level: "info",
    format: format.combine(
        format.errors({ stack: true }),
        format.colorize(),
        format.timestamp({
            "format": "YYYY-MM-DD HH:mm:ss.SSS"
        }),
        loggerPrintFormat
    ),
    transports: [
        new transports.Console()
    ],
});

const profiler = createLogger({
    level: "debug",
    format: format.combine(
        format.colorize(),
        format.timestamp({
            "format": "YYYY-MM-DD HH:mm:ss.SSS"
        }),
        profilerPrintFormat, 
    ),
    transports: [
        new transports.File({
            "filename": "./tmp/profiler.log"
        })
    ],
});

if (ENV.APP_ENV === "DEV") {
    logger.level = "silly"
}

export {
    logger,
    profiler,
}