import { GuildMember } from "discord.js";
import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";
import { Max, Min } from "class-validator";
import { RoleModel } from "./Role";


@Entity({
    "name": "users"
})
export class UserModel extends BaseEntity {

    @PrimaryColumn()
    declare id: string;

    @Column({ "type": "integer", "nullable": false, "default": 1 })
    @Min(1)
    @Max(5)
    declare role: number;

    /**
     * Counts the number of roles available for metatitles the user has
     * @param discordUser the user to get the roles from
     */
    static async countDiscordRoles(discordUser: GuildMember) {
        const roles = await RoleModel.find();
        const count = roles.reduce((acc, role) => {
            if (discordUser.roles.cache.find(r => r.id === role.id)) {
                return acc + 1;
            }
            return acc;
        }, 0);
        return count;
    }
}