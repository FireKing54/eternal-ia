import BotUnauthorizedError from "../../errors/BotUnauthorizedError";
import { Guild, GuildMember, Role } from "discord.js";
import { logger } from "../../Log";
import { RoleGroup } from "./RoleGroup";
import { UserModel } from "./User";
import { getCurrentMetatitle } from "../../commands/Award";
import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";
import { Min } from "class-validator";

interface RolesAttributes {
    id: string,
    coeff: number;
}

interface AwardRoleReturnType {
    "removedRoles": Array<Role>,
    "addedRole"?: Role,
}

interface RankingType {
    "rank": number,
    "username": string,
    "metaTitleName": string,
    "nbRoles": number
}

@Entity({
    "name": "roles"
})
export class RoleModel extends BaseEntity {
    
    @PrimaryColumn()
    declare id: string;

    @Column({"nullable": false, "type": "integer", "default": 1})
    @Min(0)
    declare coeff: number;

    static async findByDiscordRole(discordRole: Role) {
        return await this.findOne({where: {id: discordRole.id}});
    }

    static async getGroup(discordRoleID: string) {
        const groups = await RoleGroup.find({
            "relations": {
                "roles": true
            }
        });
        return groups.find(g => g.roles?.find(item => item.idrole === discordRoleID))
    }

    static async getDiscordServerRankings(guild: Guild): Promise<Array<RankingType>> {

        await guild.members.fetch()
    
        let data: Array<RankingType> = [];

        for(const user of guild.members.cache.values()) {
            const nbRoles = await UserModel.countDiscordRoles(user)
            const metaTitle = getCurrentMetatitle(user);

            data.push({
                rank: 0,
                username: user.displayName,
                metaTitleName: metaTitle?.name || "Aucun",
                nbRoles
            })
        }
    
        // Sort the data by the number of roles
        data = [...data].sort((a, b) => {
            return b.nbRoles - a.nbRoles
        })
        
        // Add the rank to the data
        data.forEach((e, index) => {
            e.rank = index + 1;
            return e;
        })
    
        return data;
    }

    /**
     * 
     * @param guild usually `message.guild` will do
     * @returns 
     */
    static async getMetaTitles(guild: Guild) {
        const metaTitlesRegex = /.+\(\d+\+\)$/g;
        const metaTitles = guild?.roles.cache.filter(role => role.name.match(metaTitlesRegex) !== null);

        const metaList: Array<{metaTitle: Role, threshold: number}> = []

        for (const metaTitle of metaTitles.values()) {
            const beginning = metaTitle.name.lastIndexOf("(");
            const end = metaTitle.name.lastIndexOf("+)");
            const threshold = parseInt(metaTitle.name.slice(beginning + 1, end));
            metaList.push({metaTitle, threshold});
        }

        metaList.sort((a, b) => b.threshold - a.threshold);

        return metaList
    }

    

    /**
     * Awards the current role to the user
     * @param discordUser the user to award the role to
     */
    async awardRole(discordUser: GuildMember): Promise<AwardRoleReturnType> {

        const recap: AwardRoleReturnType = {
            "removedRoles": []
        };

        try {
            const awardedDiscordRole = discordUser.roles.cache.find(r => r.id === this.id);

            if (awardedDiscordRole) { // If user already has the desired role
                await discordUser.roles.remove(this.id); // Remove the role
                recap.removedRoles.push(awardedDiscordRole);
            } else {

                // Get the group where the searched role is
                const group = await RoleGroup.getAllRolesInSameGroup(this.id);

                // If the title is in a group of roles
                if (group) {
                    /**
                     * All the roles that the user has that are part of the group
                     */
                    const rolesOfGroupItem = group.filter(title => discordUser.roles.cache.has(title.idrole))

                    // Convert the array of roles to an array of role
                    const rolesOfGroup = rolesOfGroupItem.map(item => discordUser.roles.cache.find(r => r.id === item.idrole)!);

                    // If the user has at least 1 role of the group
                    if (rolesOfGroup.length > 0) {
                        // Remove all the roles that the user has that are part of the group
                        await discordUser.roles.remove(rolesOfGroup);
                        recap.removedRoles.push(...rolesOfGroup);
                    }
                }

                // Add the role to the user
                await discordUser.roles.add(this.id);
                recap.addedRole = discordUser.roles.cache.find(r => r.id === this.id)!;
            }
        } catch(e: any) {
            logger.error(e)
            throw new BotUnauthorizedError();
        }

        return recap;
    }
}

export type {
    RolesAttributes
}

