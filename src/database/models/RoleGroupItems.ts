import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn } from "typeorm";
import { RoleModel } from "./Role";
import { RoleGroup } from "./RoleGroup";

@Entity({
    "name": "rolegroupitems"
})
export class RoleGroupItem extends BaseEntity {

    @PrimaryColumn()
    declare idgroup: number;

    @ManyToOne(() => RoleGroup, (roleGroup) => roleGroup.roles)
    @JoinColumn({ "name": "idgroup" })
    declare group?: RoleGroup;

    @PrimaryColumn({ "unique": true })
    declare idrole: string;

    @OneToOne(() => RoleModel, (role) => role.id)
    @JoinColumn({ "name": "idrole" })
    declare role?: RoleModel

    @Column({"type": "integer", "nullable": false})
    declare rank: number;
}

