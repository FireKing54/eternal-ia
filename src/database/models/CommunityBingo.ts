import { BaseEntity, Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { CommunityBingoBox } from "./CommunityBingoBox";
import { Min } from "class-validator";

@Entity({
    "name": "communitybingos"
})
export class CommunityBingo extends BaseEntity {

    @PrimaryGeneratedColumn("increment")
    declare id: number;

    @OneToMany(() => CommunityBingoBox, bingoBox => bingoBox.bingo)
    declare grid: Array<CommunityBingoBox>

    @Min(1)
    @Column({ "nullable": false, "type": "real" })
    declare difficulty: number;
 
    @Min(0)
    @Column({ "nullable": false, "type": "integer" })
    declare size: number;

    @CreateDateColumn()
    declare created_at: Date

    @Column({ "nullable": true, "type": "timestamp" })
    declare ended_at?: Date;
}