import { MessageEditOptions } from "discord.js";


export type PaginationOptions = PaginationOptionsWithPages | PaginationOptionsWithResolver;

interface PaginationOptionsWithPages {
    currentPageIndex: number;
    pages: Array<PaginationItem>;
    pageResolver?: undefined
}

interface PaginationOptionsWithResolver {
    currentPageIndex: number;
    pages?: undefined;
    pageResolver: ((index: number) => Promise<PaginationItem> | PaginationItem);
    maxPage: number
}

export interface PaginationItem extends MessageEditOptions {

}
