import { PaginationOptions } from "../interfaces/pagination.interface";

export interface CreatePaginationDto {
    messageId: string;
    pagination: PaginationOptions;
    ttl?: number;
}