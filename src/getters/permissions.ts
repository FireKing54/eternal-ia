import { UserModel } from "../database/models/User";
import { User as DiscordUser } from "discord.js";

type Grade = 0 | 1 | 2 | 3 | 4 | 5;

/**
 * Function that tells if a Discord user has the required permission.
 * @param user The Discord user to check the permission for.
 * @param grade The permission grade to check for.
 */
async function hasPermission(user: DiscordUser, grade: Grade): Promise<boolean> {

    if (grade <= 1) return true;

    const dbuser = await UserModel.findOne({
        "where": {
            "id": user.id
        }
    })

    if (! dbuser) {
        return false;
    }

    return dbuser.role >= grade;
}

export {
    hasPermission
}